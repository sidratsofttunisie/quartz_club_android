package com.handmark.pulltorefresh.library;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

public class PullToRefreshLinearLayout extends PullToRefreshBase<LinearLayout> {
    public PullToRefreshLinearLayout(Context paramContext) {
        super(paramContext);
    }

    public PullToRefreshLinearLayout(Context paramContext, AttributeSet paramAttributeSet) {
        super(paramContext, paramAttributeSet);
    }

    public PullToRefreshLinearLayout(Context paramContext, PullToRefreshBase.Mode paramMode) {
        super(paramContext, paramMode);
    }

    protected LinearLayout createRefreshableView(Context paramContext, AttributeSet paramAttributeSet) {
        LinearLayout lin;

        if (VERSION.SDK_INT >= VERSION_CODES.GINGERBREAD) {
            lin = new InternalLinearLayoutSDK9(paramContext, paramAttributeSet);
        } else {
            lin = new LinearLayout(paramContext, paramAttributeSet);
        }

        lin.setId(R.id.linearlayout);
        return lin;

        // for (Object localObject = new InternalLinearLayoutSDK9(paramContext, paramAttributeSet);; localObject = new
        // LinearLayout(
        // paramContext, paramAttributeSet))
        // {
        // ((LinearLayout) localObject).setId(R.id.linearlayout);
        // return localObject;
        // }
    }

    public PullToRefreshBase.Orientation getPullToRefreshScrollDirection() {
        return PullToRefreshBase.Orientation.HORIZONTAL;
    }

    protected boolean isReadyForPullEnd() {
        View localView = ((LinearLayout) this.mRefreshableView).getChildAt(0);
        if (localView != null)
            return ((LinearLayout) this.mRefreshableView).getScrollX() >= localView.getWidth() - getWidth();
        return false;
    }

    protected boolean isReadyForPullStart() {
        return ((LinearLayout) this.mRefreshableView).getScrollY() == 0;
    }

    @TargetApi(9)
    final class InternalLinearLayoutSDK9 extends LinearLayout {
        public InternalLinearLayoutSDK9(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        protected boolean overScrollBy(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5,
                                       int paramInt6, int paramInt7, int paramInt8, boolean paramBoolean) {
            boolean bool = super.overScrollBy(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6,
                    paramInt7, paramInt8, paramBoolean);
            OverscrollHelper.overScrollBy(PullToRefreshLinearLayout.this, paramInt1, paramInt7, paramInt2, paramInt8,
                    paramBoolean);
            return bool;
        }
    }
}
