/**
 * Automatically generated file. DO NOT MODIFY
 */
package mobi.app4mob.quartzclub.activity;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "mobi.app4mob.quartzclub.activity";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 4;
  public static final String VERSION_NAME = "1.2";
}
