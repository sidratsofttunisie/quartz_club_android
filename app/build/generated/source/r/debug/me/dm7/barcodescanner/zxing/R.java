/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package me.dm7.barcodescanner.zxing;

public final class R {
    public static final class attr {
        public static final int borderAlpha = 0x7f0100e8;
        public static final int borderColor = 0x7f0100e1;
        public static final int borderLength = 0x7f0100e4;
        public static final int borderWidth = 0x7f0100e3;
        public static final int cornerRadius = 0x7f0100e6;
        public static final int finderOffset = 0x7f0100e9;
        public static final int laserColor = 0x7f0100e0;
        public static final int laserEnabled = 0x7f0100df;
        public static final int maskColor = 0x7f0100e2;
        public static final int roundedCorner = 0x7f0100e5;
        public static final int shouldScaleToFill = 0x7f0100de;
        public static final int squaredFinder = 0x7f0100e7;
    }
    public static final class color {
        public static final int viewfinder_border = 0x7f0e01b4;
        public static final int viewfinder_laser = 0x7f0e01b5;
        public static final int viewfinder_mask = 0x7f0e01b6;
    }
    public static final class integer {
        public static final int viewfinder_border_length = 0x7f0b0000;
        public static final int viewfinder_border_width = 0x7f0b0001;
    }
    public static final class styleable {
        public static final int[] BarcodeScannerView = { 0x7f0100de, 0x7f0100df, 0x7f0100e0, 0x7f0100e1, 0x7f0100e2, 0x7f0100e3, 0x7f0100e4, 0x7f0100e5, 0x7f0100e6, 0x7f0100e7, 0x7f0100e8, 0x7f0100e9 };
        public static final int BarcodeScannerView_shouldScaleToFill = 0;
        public static final int BarcodeScannerView_laserEnabled = 1;
        public static final int BarcodeScannerView_laserColor = 2;
        public static final int BarcodeScannerView_borderColor = 3;
        public static final int BarcodeScannerView_maskColor = 4;
        public static final int BarcodeScannerView_borderWidth = 5;
        public static final int BarcodeScannerView_borderLength = 6;
        public static final int BarcodeScannerView_roundedCorner = 7;
        public static final int BarcodeScannerView_cornerRadius = 8;
        public static final int BarcodeScannerView_squaredFinder = 9;
        public static final int BarcodeScannerView_borderAlpha = 10;
        public static final int BarcodeScannerView_finderOffset = 11;
    }
}
