package ExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mobi.app4mob.quartzclub.activity.AddCommandActivity;
import mobi.app4mob.quartzclub.activity.R;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.CommandProduct;
import mobi.app4mob.quartzclub.utils.BitmapManager;

import android.content.Context;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private ArrayList<CommandProduct> _command_product; // header titles


    public ExpandableListAdapter(Context context, ArrayList<CommandProduct> command_product) {
        this._context = context;
        this._command_product = command_product;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._command_product.get(groupPosition).getListe_code().get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.cell_list_package, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.code);

        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._command_product.get(groupPosition).getListe_code().size();
    }

    @Override
    public CommandProduct getGroup(int groupPosition) {
        return this._command_product.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._command_product.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        // TODO
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.cell_list_command_product, null);
        }


        ((TextView) convertView.findViewById(R.id.qt_product)).setText("X" + _command_product.get(groupPosition).getListe_code().size());

		/*Shader myShader = new LinearGradient( 
		    0, 0, 0, 100, 
		    0xFFFFFFFF, Color.BLACK, 
		    Shader.TileMode.CLAMP );*/


        Shader txtShad = AddCommandActivity.txtShad;

        ((TextView) convertView.findViewById(R.id.qt_product)).getPaint().setShader(txtShad);

        ((TextView) convertView.findViewById(R.id.adresse)).setVisibility(View.VISIBLE);
        ((TextView) convertView.findViewById(R.id.adresse)).setText(_command_product.get(groupPosition).getProduct().getCode());

        ((TextView) convertView.findViewById(R.id.name)).setVisibility(View.VISIBLE);
        ((TextView) convertView.findViewById(R.id.name)).setText(_command_product.get(groupPosition).getProduct().getName());

        if (!Functions.isStringEmpty(_command_product.get(groupPosition).getProduct().getImage())) {
            //int width = (int) _context.getResources().getDimension(R.dimen.width_img);
            //int height = (int) _context.getResources().getDimension(R.dimen.height_img);
            int width = 50;
            int height = 58;
            String url = Constants.DOMAIN + Constants.UPLOADS_PRODUCT_FOLDER + "/" + _command_product.get(groupPosition).getProduct().getImage();
            ImageView img = (ImageView) convertView.findViewById(R.id.product_image);
            BitmapManager.INSTANCE.loadBitmap(url, img, width, height);
        }


        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}