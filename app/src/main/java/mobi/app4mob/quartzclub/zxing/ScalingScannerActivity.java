package mobi.app4mob.quartzclub.zxing;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.Result;

import mobi.app4mob.quartzclub.activity.AddCommandActivity;
import mobi.app4mob.quartzclub.activity.AddMagasinActivity;
import mobi.app4mob.quartzclub.activity.R;
import mobi.app4mob.quartzclub.activity.ScanProductActivity;

import static com.dm.zbar.android.scanner.ZBarConstants.SCAN_RESULT;

public class ScalingScannerActivity extends BaseScannerActivity implements ZXingScannerView.ResultHandler {
    private static final String FLASH_STATE = "FLASH_STATE";

    private ZXingScannerView mScannerView;
    private boolean mFlash;
    public TextView nbProducts ;
    boolean isActive=false;

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_scaling_scanner);
        ViewGroup contentFrame = (ViewGroup) findViewById(R.id.content_frame);
        TextView action_bar_title = (TextView) findViewById(R.id.txt_action_bar_title);
        action_bar_title.setText(R.string.scan_code_product);
        mScannerView = new ZXingScannerView(this);
        contentFrame.addView(mScannerView);
        Bundle extras = getIntent().getExtras();
        int type_profil = extras.getInt("type_profil");
        isActive=true;
        int nbProduitScanner = extras.getInt("nbProduitScanner",0);
        boolean scanProduct = extras.getBoolean("scanProduct",false);
        boolean productExist = extras.getBoolean("productExist",false);
        if (type_profil == 2) {
            ((Toolbar) findViewById(R.id.my_toolbar)).setBackgroundColor(getResources().getColor(R.color.color_rose));
            ((Button) findViewById(R.id.button2)).setBackgroundColor(getResources().getColor(R.color.color_rose));
        }else {
            ((Toolbar) findViewById(R.id.my_toolbar)).setBackgroundColor(getResources().getColor(R.color.color_blue));
            ((Button) findViewById(R.id.button2)).setBackgroundColor(getResources().getColor(R.color.color_blue));
            ((Button) findViewById(R.id.buttonterminer)).setBackgroundColor(getResources().getColor(R.color.color_blue));
            ((TextView) findViewById(R.id.nbrProductScanned)).setVisibility(View.VISIBLE);
            //  ((TextView) findViewById(R.id.buttonterminer)).setVisibility(View.VISIBLE);
            if (scanProduct){
                ((TextView) findViewById(R.id.nbrProductScanned)).setText(nbProduitScanner+"");

                ((Button) findViewById(R.id.buttonterminer)).setVisibility(View.VISIBLE);
            }



        }

    }


    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
        mScannerView.setFlash(mFlash);
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(FLASH_STATE, mFlash);
    }

    @Override
    public void handleResult(Result rawResult) {
        if (rawResult.getText().toString().length() > 0) {


            if (!TextUtils.isEmpty(rawResult.getText())) {
                Intent dataIntent = new Intent();
                dataIntent.putExtra(AddMagasinActivity.SCAN_RESULT, rawResult.getText().toString());
                setResult(Activity.RESULT_OK, dataIntent);
                // close this activity
                finish();
            } else {

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Erreur lecture codebarre")
                        .setCancelable(true)
                        .setNegativeButton("Fermer", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                final AlertDialog alert = builder.create();
                alert.show();
            }


            // Note:
            // * Wait 2 seconds to resume the preview.
            // * On older devices continuously stopping and resuming camera preview can result in freezing the app.
            // * I don't know why this is the case but I don't have the time to figure out.
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //alert.dismiss();
                    mScannerView.resumeCameraPreview(ScalingScannerActivity.this);
                }
            }, 1000);
        }

    }
    public void toggleFlash(View v) {
        mFlash = !mFlash;
        mScannerView.setFlash(mFlash);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("goToList", true);
        editor.putBoolean("openScanner", false);
        finish();
    }

    public void goToListProducts(View view) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("goToList", true);
        editor.putBoolean("openScanner", false);
        editor.commit();
        //startActivity(new Intent(ScalingScannerActivity.this, AddCommandActivity.class));
        finish();
    }
}