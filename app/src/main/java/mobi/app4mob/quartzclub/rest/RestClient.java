package mobi.app4mob.quartzclub.rest;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import mobi.app4mob.quartzclub.constants.Constants;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ahmed on 2018/07/20.
 */

public class RestClient {
    public static final String BASE_URL = Constants.DOMAIN+"api.php/";
    private static Retrofit retrofit = null;
    public static ApiInterface service;
    public static ApiInterface getClient() {


        OkHttpClient client = new OkHttpClient();
        //client.setReadTimeout(240, TimeUnit.SECONDS);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.excludeFieldsWithoutExposeAnnotation();
        Gson gson = gsonBuilder.create();


        Retrofit retrofit = new Retrofit.Builder()

                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        service = retrofit.create(ApiInterface.class);


        //  client.interceptors().add(new LoggingInterceptor());


        return service;


    }
}
