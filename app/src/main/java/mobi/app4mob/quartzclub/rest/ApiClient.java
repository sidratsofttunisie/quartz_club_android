package mobi.app4mob.quartzclub.rest;

/**
 * Created by Yosri Mekni on 16/05/2017.
 */

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


import mobi.app4mob.quartzclub.constants.Constants;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {

    public static final String BASE_URL = Constants.DOMAIN+"api.php/";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {

        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(new GsonBuilder() .excludeFieldsWithoutExposeAnnotation() .create()))
                    .build();
        }
        return retrofit;
    }

   
}
