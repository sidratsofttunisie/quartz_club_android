package mobi.app4mob.quartzclub.rest;

/**
 * Created by Yosri Mekni on 16/05/2017.
 */

import com.google.gson.JsonObject;

import java.util.ArrayList;



import mobi.app4mob.quartzclub.model.History;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;



public interface ApiInterface {


    @GET("history_point.json")
    Call<History> getHistory(@Query("user_id") String user_id);

    @FormUrlEncoded
    @POST("requestgift")
    Call<JsonObject> requestGift(@Field("user_id") int user_id,@Field("gift_id") int gift_id);

}
