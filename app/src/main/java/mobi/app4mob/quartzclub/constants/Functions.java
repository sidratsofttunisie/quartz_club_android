package mobi.app4mob.quartzclub.constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import mobi.app4mob.quartzclub.activity.MenuActivity;
import mobi.app4mob.quartzclub.model.Produit;

public class Functions {


    public static AlertDialog showAlert(String message,String title,Context context) {
        return   new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(true)
                .show();
    }

    public static void LOGI(String tag, String message) {
        if (Constants.ENABLE_LOGS)
            Log.i(tag, message);
    }

    public static void LOGE(String tag, String message) {
        if (Constants.ENABLE_LOGS)
            Log.e(tag, message);
    }

    public static int setIntAttribute(String name, JSONObject json,
                                      int defaultValue) {
        int attr = 0;
        attr = json.optInt(name, defaultValue);
        return attr;
    }

    public static boolean setBoolAttribute(String name, JSONObject json,
                                           boolean defaultValue) {
        boolean attr = false;
        attr = json.optBoolean(name, defaultValue);
        return attr;
    }

    public static Produit getProduitById(int id, ArrayList<Produit> liste_produits) {
        for (Produit produit : liste_produits) {
            if (produit.getId() == id) {
                return produit;
            }
        }
        return null;
    }

    public static double setDoubleAttribute(String name, JSONObject json,
                                            int defaultValue) {
        double attr = 0;
        attr = json.optDouble(name, defaultValue);
        return attr;
    }

    public static String setAttribute(String name, JSONObject json,
                                      String defaultValue) {
        String attr = null;
        attr = json.optString(name, defaultValue);
        return attr;
    }

    public static ArrayList<String> parseListOfStrings(JSONObject json,
                                                       String name) {
        ArrayList<String> listStrings = null;

        try {
            JSONArray jArray = json.optJSONArray(name);
            if (jArray != null && jArray.length() > 0) {
                listStrings = new ArrayList<String>();
                for (int i = 0; i < jArray.length(); i++) {
                    listStrings.add(jArray.getString(i));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listStrings;
    }

    public static boolean isStringEmpty(String str) {
        return TextUtils.isEmpty(str) || str.equals("null");
    }

    public static boolean isArrayEmpty(ArrayList<?> array) {
        return (array == null || (array != null && array.size() == 0));
    }


    public static int getWidhtScreen(Activity activity) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return (metrics.widthPixels);
    }

    public static int getHeightScreen(Activity activity) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return (metrics.heightPixels);
    }

    public static StateListDrawable setSelector(int pressed, int normal,
                                                Context context) {
        StateListDrawable states = new StateListDrawable();
        // states.addState(new int[] { android.R.attr.state_pressed },
        // context.getResources().getDrawable(pressed));
        // states.addState(new int[] { android.R.attr.state_focused },
        // context.getResources().getDrawable(pressed));
        // states.addState(new int[] { android.R.attr.state_selected },
        // context.getResources().getDrawable(pressed));

        states.addState(new int[]{android.R.attr.state_pressed,
                        android.R.attr.state_focused, android.R.attr.state_selected},
                context.getResources().getDrawable(pressed));
        states.addState(new int[]{}, context.getResources()
                .getDrawable(normal));

        return states;
    }

    public static String getFormatedDate(String date) {
        Date inputDate = new Date();
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            inputDate = fmt.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // return inputDate;
        fmt = new SimpleDateFormat("dd/MM/yyyy");
        return fmt.format(inputDate);
    }

    public static String getFormatedDateHours(String date) {
        Date inputDate = new Date();
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            inputDate = fmt.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // return inputDate;
        fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        return fmt.format(inputDate);
    }

    public static String getYoutubeVideoId(String youtubeUrl) {
        String video_id = "";
        if (youtubeUrl != null && youtubeUrl.trim().length() > 0
                && youtubeUrl.startsWith("http")) {

            String expression = "^.*((youtu.be"
                    + "\\/)"
                    + "|(v\\/)|(\\/u\\/w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*"; // var
            // regExp
            // =
            // /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
            CharSequence input = youtubeUrl;
            Pattern pattern = Pattern.compile(expression,
                    Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(input);
            if (matcher.matches()) {
                String groupIndex1 = matcher.group(7);
                if (groupIndex1 != null && groupIndex1.length() == 11)
                    video_id = groupIndex1;
            }
        }
        return video_id;
    }

//	public static ArrayList<String> getYoutubeIds(ArrayList<Video> data,
//			boolean isOnlyId) {
//		ArrayList<String> youtubeIds = null;
//
//		if (data != null && data.size() > 0) {
//			youtubeIds = new ArrayList<String>();
//			// youtubeMignature = new ArrayList<String>();
//			for (int i = 0; i < data.size(); i++) {
//				String ytb = isOnlyId ? data.get(i).getLinkVideo() : Functions
//						.getYoutubeVideoId(data.get(i).getLinkVideo());
//				youtubeIds.add(ytb);
//				// youtubeMignature.add(Constants.VIDEO_THUMBNAIL_URL + ytb +
//				// Constants.DEFAULT_THUMBNAIL);
//			}
//		}
//		return youtubeIds;
//	}


    //*****************************************

//	public static boolean testPrefDejaVue(int idVideo, Context context) {
//		
//		SharedPreferences prefs = context.getSharedPreferences(
//				Constants.SHARED_PREFS_VIDEOTHEQUE, 0);
//		
//		String videosVues = prefs.getString(Constants.SHARED_VUES, "");
//
//		String[] ids = videosVues.split(",");
//		boolean found = false;
//
//		for (int i = 0; i < ids.length && !found; i++) {
//			if (ids[i].equals("" + idVideo))
//			{
//				found = true;
//			}
//		}
//
//		//new MyToast(context, "testDejaVue = " + found, Toast.LENGTH_SHORT);
//
//		return found;
//	}
//
//	public static void addPrefVideoVue(int idVideo, Context context) {
//		
//		SharedPreferences prefs = context.getSharedPreferences(
//				Constants.SHARED_PREFS_VIDEOTHEQUE, 0);
//		
//		String videosVues = prefs.getString(Constants.SHARED_VUES, "");
//
//		SharedPreferences.Editor ed = prefs.edit();
//		ed.putString(Constants.SHARED_VUES, videosVues+","+idVideo);
//		ed.commit();
//
//	}


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static void setBgDrawableVsJB(View v, Drawable d) {
        v.setBackground(d);
    }

    @SuppressWarnings("deprecation")
    public static void setBgDrawable(View v, Drawable d) {
        v.setBackgroundDrawable(d);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static void setBgDrawableVsJB(View v, StateListDrawable d) {
        v.setBackground(d);
    }

    @SuppressWarnings("deprecation")
    public static void setBgDrawable(View v, StateListDrawable d) {
        v.setBackgroundDrawable(d);
    }


    // day/mounth/year
    public static String getYMD(String date) {
        String dateFormat = "";
        // 2012-12-17 hh:mm:ss
        // 17/12/2012
        try {
            dateFormat = date.substring(8, 10) + "/" + date.substring(5, 7) + "/" + date.substring(0, 4);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateFormat;
    }

    // hh:mm
    public static String getHM(String date) {
        // 2012-12-17 hh:mm:ss
        // HHhMM
        String d = "";
        try {
            d = date.substring(11, 16).replace(":", "h");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }


    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {

        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),

                bitmap.getHeight(), Config.ARGB_8888);

        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;

        final Paint paint = new Paint();

        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        final RectF rectF = new RectF(rect);

        final float roundPx = 12;

        paint.setAntiAlias(true);

        canvas.drawARGB(0, 0, 0, 0);

        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));

        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    /**
     * ****************************************
     */

    public static String getUIDDevice(Context context) {
        return Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
    }

    public static boolean isEquals(String str1, String str2) {
        return str1 == str2 || str1.equals(str2);
    }

    // ***** méthode récupérant la localisation actuelle *****
    public final static Location getMyLocation(Context _context) {
        Location location = null;
        try {
            LocationManager locationManager = (LocationManager) _context.getSystemService(Context.LOCATION_SERVICE);

            List<String> providers = locationManager.getProviders(true);

            for (int i = providers.size() - 1; i >= 0; i--)
                location = locationManager.getLastKnownLocation(providers.get(i));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return location;
    }

    /**
     * @return the last know best location
     */
    public final static Location getLastBestLocation(Context _context) {
        LocationManager mLocationManager = (LocationManager) _context.getSystemService(Context.LOCATION_SERVICE);
        Location locationGPS = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Location locationNet = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        long GPSLocationTime = 0;
        if (null != locationGPS) {
            GPSLocationTime = locationGPS.getTime();
        }

        long NetLocationTime = 0;

        if (null != locationNet) {
            NetLocationTime = locationNet.getTime();
        }

        if (0 < GPSLocationTime - NetLocationTime) {
            return locationGPS;
        } else {
            return locationNet;
        }
    }


}
