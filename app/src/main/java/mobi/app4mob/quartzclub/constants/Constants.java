package mobi.app4mob.quartzclub.constants;


public class Constants {


    // HTTP methods
    // HTTP Get
    public static final int HTTP_METHOD_GET = 0;
    // HTTP Post
    public static final int HTTP_METHOD_POST = 1;


    // HTTP connection parameters
    // Connection timeout
    public static final int CONNECTION_TIME_OUT = 30000; // in milliseconds
    // Socket timeout
    public static final int SOCKET_TIME_OUT = 30000; // in milliseconds
    // Web service user name
    public static final String WS_USER_NAME = "";
    // Web service password
    public static final String WS_PASSWORD = "";

    // HTTP errors
    public static final int HTTP_ERROR = 404;

    //pic2shop
	public static final String PIC2SHOP_PACKAGE = "com.visionsmarts.pic2shop";
	public static final String PIC2SHOP_ACTION = PIC2SHOP_PACKAGE + ".SCAN";
	public static final int PIC2SHOP_REQUEST_CODE_SCAN = 1405;
	public static final String PIC2SHOP_BARCODE = "BARCODE";
	public static interface Pro {

		public static final String PACKAGE = "com.visionsmarts.pic2shoppro";
		public static final String ACTION = PACKAGE + ".SCAN";

		// request Intent
		public static final String FORMATS = "formats";
		// response Intent
		public static final String FORMAT = "format";
	}
    /**
     * ********************************** WEB SRVICES ***************************
     */
    public static final String UPLOADS_PRODUCT_FOLDER = "uploads/product";
    public static final String UPLOADS_GIFT_FOLDER = "uploads/gifts";
    public static final String DOMAIN = "http://quartzclub.localbeta.ovh/";
    //public static final String DOMAIN = "http://196.203.223.107/";
    //public static final String DOMAIN = "http://total.localbeta.net/";
    public static final String LOGIN_WS_URL = DOMAIN + "api.php/login";
    public static final String FORGOT_PASSWORD_WS_URL = DOMAIN + "api.php/forgot-password";
    public static final String Gifts_WS_URL = DOMAIN + "api.php/gift.json";
    public static final String Promos_WS_URL = DOMAIN + "api.php/promotion.json";
    public static final String MAGASIN_WS_URL = DOMAIN + "api.php/magasin.json";
    public static final String ADD_MAGASIN_WS_URL = DOMAIN + "api.php/addmagasin";
    public static final String GET_PRODUCT_BY_CODE_WS_URL = DOMAIN + "api.php/productdetail.json";
    public static final String GET_ALL_PRODUC_WS_URL = DOMAIN + "api.php/product.json";
    public static final String UPDATE_USER = DOMAIN + "api.php/update_user";
    public static final String NEW_COMMAND_WS_URL = DOMAIN + "api.php/addcommande";
    public static final String ADD_TARGET_GIFT_WS_URL = DOMAIN + "api.php/targetgift";
    public static final String HISTORIQUE_WS_URL = DOMAIN + "api.php/historique.json";
    public static final String BONUSnPACK_WS_URL = DOMAIN + "api.php/bonus-produit";
    public static final String BONUS_ANNIV_WS_URL = DOMAIN + "api.php/bonus-aniversaisre";
    public static final String BONUS_PALIER_WS_URL = DOMAIN + "api.php/bonus-palier";
    public static final String BONUS_BIENVENU_WS_URL = DOMAIN + "api.php/bonus-bienvenu";
    public static final String MAGINFO_WS_URL = DOMAIN + "api.php/magasin_info";
    public static final String UPDATEPASS_WS_URL = DOMAIN + "api.php/updatePassword";
    public static final String MESSAGE_WS_URL = DOMAIN + "api.php/message.json";
    public static final String NOTIFICATION_WS_URL = DOMAIN + "api.php/notification";
    public static final String CATALOGUE_WS_URL = DOMAIN + "api.php/get-catalogue";
    public static final String UPDATE_TAG_MAG_WS_URL = DOMAIN + "api.php/magasin/update-catalogue";


    // Enable/disable logs
    public static final boolean ENABLE_LOGS = true;
    // Application name tag
    public static final String APP_NAME = "TOTAL";


    public static final String VIDE = "";


    // ****** Extras Intents ******
    public static final String EXTRA_FROM_THEME = "extra_from_theme";


    public static final class MSG {
        public static final int _MSG = 2000;
        public static final int ERROR = 2001;
        public static final int NETWORK_ERROR = 2002;
        public static final int FORGET_PASSWORD_ERROR = 2003;
        public static final int FORGET_PASSWORD = 2004;
        public static final int DECONNEXION = 2005;
        public static final int PHOTO = 2006;
        public static final int CONFIRM_PHOTO = 2007;
        public static final int SUCCESS = 2008;
        public static final int PAS_EVENT_IN_CARTE = 2009;
        public static final int AUCUNE_INFOS = 2010;
        public static final int NOT_CONNECTED = 2011;
        public static final int SUCCESS_ADD = 2012;
        public static final int ERROR_ADD = 2013;
        public static final int SUCCESS_DELETE_FAV = 2014;
        public static final int NO_UPDATE_DONE = 2015;
        public static final int VALIDATE_UPDATE = 2016;
        public static final int ERROR_EMAIL_EXIST = 2017;
        public static final int OLD_PASS_ERROR = 2018;
        public static final int VALIDATE_DELETE_FAV = 2019;
        public static final int VALIDATE_DELETE_PUB = 2020;
        public static final int SUCCESS_DELETE_PUB = 2021;
        public static final int FORGET_PASSWORD_ERROR_EMAIL = 2022;
        public static final int FORGET_PASSWORD_SUCCESS = 2023;
        public static final int CONFIRM_DELETE_PHOTO = 2024;
        public static final int CONFIRM_EXIT = 2025;
        // public static final int ERROR_CHOISIR_CAT = 2026;
        public static final int NO_MORE = 2027;
        public static final int NO_RESULT_RECHERCHE = 2028;
        public static final int SHARE = 2029;
        public static final int CONNECT = 2030;
        public static final int DISCONNECT = 2031;
    }

    public static final class Extra {
        public static final String SCAN_TAG_MAG = "scan_tag_mag";
        public static final String SCAN_CODE_PRODUCT = "scan_code_product";
        public static final String SCAN_CODE_FACTURE = "scan_code_facture";
        public static final String LIST_COMMAND_PRODUCT = "list_command_product";

    }


    /**
     * ************************************************
     */

    public static final long RETRY_TIME = 13000;
    public static final long START_TIME = 0;
    public static final String DO = "do";
    public static final String ID = "id";
    public static final String UPDATE = "update";


    /**
     * ********* gcm ***********
     */

    // Admob size
    //		public static final AdSize BANNER_SIZE = AdSize.BANNER;

    public static final class SETTINGS_GCM {
        public static final String SETTINGS = "SETTINGS";

        // gcm
        //content: {"device_id":"1245654", "token":"1s1s1s1s", "type":"1", "app_id":"1", "apikey":"notif4center"}
        public static final String GCM_UID_DEVICE = "device_id";
        public static final String GCM_TOKEN = "token";
        public static final String GCM_APIKEY = "apikey";
        //public static final String GCM_APP_FANS = "id";
        public static final String GCM_APP_ID = "app_id";
        public static final String GCM_TYPE = "type";

        // params
        public static final String ID = "i";
        public static final String MESSAGE = "m";
    }
}
