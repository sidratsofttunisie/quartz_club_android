package mobi.app4mob.quartzclub.model;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;

import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.utils.WebClient;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;


public class Magasin implements Serializable {
    private static final long serialVersionUID = -3L;
    private Marker marker = null;

    public static final String ID = "id";
    private int id;

    public static final String MAGASIN_NAME = "magasin_name";
    private String magasin_name;

    // nom du contact
    public static final String NAME = "first_name";
    private String name;

    public static final String LONGITUDE = "longitude";
    private Float longitude;

    public static final String LATITUDE = "latitude";
    private Float latitude;

    public static final String ADDRESS = "address";
    private String address;

    public static final String REGISTERCOMMERCE = "registre_commerce";
    private String registre_commerce;


    public static final String TEL = "tel";
    private String tel;

    public static final String FAX = "fax";
    private String fax;

    public static final String CIN = "cin";
    private String cin;

    public static final String TAG_MAGASIN = "ref_magasin";
    private String tag_magasin;

    public static final String TAG_CLIENT = "ref_client";
    private String tag_client;

    public static final String KEYWORD = "key";

    public Magasin() {
        super();

    }


    public Magasin(JSONObject json) {
        super();

        this.id = Functions.setIntAttribute(ID, json, 0);
        this.magasin_name = Functions.setAttribute(MAGASIN_NAME, json, "");
        this.registre_commerce = Functions.setAttribute(REGISTERCOMMERCE, json, "");
        this.name = Functions.setAttribute(NAME, json, "");
        this.tel = Functions.setAttribute(TEL, json, "");
        this.fax = Functions.setAttribute(FAX, json, "");
        this.cin = Functions.setAttribute(CIN, json, "");
        this.tag_client = Functions.setAttribute(TAG_CLIENT, json, "");
        this.tag_magasin = Functions.setAttribute(TAG_MAGASIN, json, "");
        this.address = Functions.setAttribute(ADDRESS, json, "");
        this.longitude = (float) Functions.setDoubleAttribute(LONGITUDE, json, 0);
        this.latitude = (float) Functions.setDoubleAttribute(LATITUDE, json, 0);


    }

    public String getRegistre_commerce() {
        return registre_commerce;
    }


    public void setRegistre_commerce(String registre_commerce) {
        this.registre_commerce = registre_commerce;
    }

    private final String KEY_PAGE = "page";
    private final String KEY_COMMERCIAL_ID = "commercial_id";


    public static final String LONG1 = "long1";
    public static final String LAT1 = "lat1";
    public static final String LONG2 = "long2";
    public static final String LAT2 = "lat2";
    public static final String LAT = "latitude";
    public static final String LONG = "longitude";

    public ArrayList<Magasin> getMagasinNearMe(String url, int commercial_id, double long1, double lat1, double long2, double lat2, double latitude, double longitude, String keyword, int page) {
        ArrayList<Magasin> data = null;
        String response = null;

        ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();

        parameters.add(new BasicNameValuePair(KEY_COMMERCIAL_ID, "" + commercial_id));

        if (page > 0) {
            parameters.add(new BasicNameValuePair(KEY_PAGE, "" + page));
        }

        if (long1 > 0 && lat1 > 0 && long2 > 0 && lat2 > 0) {
            parameters.add(new BasicNameValuePair(LONG1, "" + long1));
            parameters.add(new BasicNameValuePair(LAT1, "" + lat1));
            parameters.add(new BasicNameValuePair(LONG2, "" + long2));
            parameters.add(new BasicNameValuePair(LAT2, "" + lat2));
        }

        parameters.add(new BasicNameValuePair(LAT, "" + latitude));
        parameters.add(new BasicNameValuePair(LONG, "" + longitude));

        parameters.add(new BasicNameValuePair(KEYWORD, "" + keyword));

        WebClient webClient = new WebClient(url, parameters);
        webClient.execute(0);
        response = webClient.getStringResponse();

        data = Magasin.parseData(response);

        return data;
    }


    public ArrayList<Magasin> getMagasin(String url, int commercial_id, double longitude, double latitude, String keyword, int page) {
        ArrayList<Magasin> data = null;
        String response = null;

        ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();

        parameters.add(new BasicNameValuePair(KEY_COMMERCIAL_ID, "" + commercial_id));

        if (page > 0) {
            parameters.add(new BasicNameValuePair(KEY_PAGE, "" + page));
        }

        if (longitude > 0 && latitude > 0) {
            parameters.add(new BasicNameValuePair(LONGITUDE, "" + longitude));
            parameters.add(new BasicNameValuePair(LATITUDE, "" + latitude));
        }

        parameters.add(new BasicNameValuePair(KEYWORD, "" + keyword));

        WebClient webClient = new WebClient(url, parameters);
        webClient.execute(0);
        response = webClient.getStringResponse();
        data = Magasin.parseData(response);

        return data;
    }

    public static ArrayList<Magasin> parseData(String wsResponse) {
        ArrayList<Magasin> data = null;
        if (wsResponse != null && !wsResponse.equals(null) && wsResponse.length() > 0) {
            try {
                JSONArray obj = new JSONArray(wsResponse);
                data = new ArrayList<Magasin>();
                for (int i = 0; i < obj.length(); i++) {
                    JSONObject objI = obj.getJSONObject(i);
                    data.add(new Magasin(objI));
                }
                 Log.i("data ", data.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return data;
    }

    public LatLng getLatLng() {
        return new LatLng(latitude, longitude);
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    public Magasin set_marker(Marker marker) {
        this.marker = marker;
        return this;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMagasin_name() {
        return magasin_name;
    }

    public void setMagasin_name(String magasin_name) {
        this.magasin_name = magasin_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public String getAddress() {
        return address;
    }


    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    public String getTag_magasin() {
        return tag_magasin;
    }

    public void setTag_magasin(String tag_magasin) {
        this.tag_magasin = tag_magasin;
    }

    public String getTag_client() {
        return tag_client;
    }

    public void setTag_client(String tag_client) {
        this.tag_client = tag_client;
    }

}
