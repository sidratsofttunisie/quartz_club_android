package mobi.app4mob.quartzclub.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.constants.Functions;


public class Commande {

    public static final String MAGASIN_PTS = "magasin_points";
    private String magasin_points;

    public static final String COMMERCIAL_PTS = "commercial_points";
    private String commercial_points;

    public static final String CREATEDAT = "created_at";
    private String created_at;

    public static final String IMAGEPATH = "image_path";
    private String image_path;

    public static final String QTETOTAL = "qte_total";
    private String qte_total;

    public static final String PRODUITS = "products";
    private ArrayList<ProduitC> listProduct;


    public Commande(JSONObject json) {
        super();
        ArrayList<ProduitC> data = new ArrayList<ProduitC>();
        try {
            JSONArray obj = new JSONArray(Functions.setAttribute(PRODUITS, json,
                    ""));

            for (int i = 0; i < obj.length(); i++) {
                JSONObject objI = obj.getJSONObject(i);
                data.add(new ProduitC(objI));
            }
            // Log.i("data ", data.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.listProduct = data;
        this.commercial_points = Functions.setAttribute(COMMERCIAL_PTS, json, "");
        this.magasin_points = Functions.setAttribute(MAGASIN_PTS, json, "");
        this.qte_total = Functions.setAttribute(QTETOTAL, json, "");
        this.image_path = Functions.setAttribute(IMAGEPATH, json, "");
        this.created_at = Functions.setAttribute(CREATEDAT, json, "");

    }


    public ArrayList<ProduitC> getListProduct() {
        return listProduct;
    }

    public void setListProduct(ArrayList<ProduitC> listProduct) {
        this.listProduct = listProduct;
    }

    public String getMagasin_points() {
        return magasin_points;
    }

    public void setMagasin_points(String magasin_points) {
        this.magasin_points = magasin_points;
    }

    public String getCommercial_points() {
        return commercial_points;
    }

    public void setCommercial_points(String commercial_points) {
        this.commercial_points = commercial_points;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public String getQte_total() {
        return qte_total;
    }

    public void setQte_total(String qte_total) {
        this.qte_total = qte_total;
    }
}
