package mobi.app4mob.quartzclub.model;

import org.json.JSONException;
import org.json.JSONObject;

import mobi.app4mob.quartzclub.constants.Functions;

/**
 * Created by Yosri Mekni <dev4.yosri@gmail.com> on 08/02/2018.
 */

public class BonusPackage {


    private static final long serialVersionUID = 4441981177333667735L;

    public static final String PRODUCT_ID = "id";
    private int product_id;

    public static final String QT = "qt";
    private int qt;

    public static final String PRODUCT = "Product";
    private Produit product;


    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getQt() {
        return qt;
    }

    public void setQt(int qt) {
        this.qt = qt;
    }

    public Produit getProduct() {
        return product;
    }

    public void setProduct(Produit product) {
        this.product = product;
    }
}
