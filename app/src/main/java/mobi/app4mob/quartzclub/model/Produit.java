package mobi.app4mob.quartzclub.model;

import java.io.Serializable;
import java.util.ArrayList;

import mobi.app4mob.quartzclub.activity.MenuActivity;
import mobi.app4mob.quartzclub.constants.Functions;
import org.json.JSONObject;

public class Produit implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 7673100708833459808L;
    public static final String ID = "id";
    private int id;

    public static final String NAME = "name";
    private String name;

    public static final String CODE = "code";
    private String code;

    public static final String ORIGIN = "origin";
    private String origin;

    public static final String UNITE = "unite";
    private String unite;

    public static final String USE = "utilisation";
    private String use;

    public static final String GAMME = "gamme";
    private String gamme;

    public static final String POIDS = "poids";
    private String poids;

    public static final String IMAGE = "image";
    private String image;


    public static ArrayList<Produit> liste_produits = null;

    public Produit() {
        super();
    }

    public Produit(JSONObject json) {
        super();
        this.id = Functions.setIntAttribute(ID, json, -1);
        this.name = Functions.setAttribute(NAME, json, "");
        this.code = Functions.setAttribute(CODE, json, "");
        this.image = Functions.setAttribute(IMAGE, json, "");
        Functions.LOGI("**** produit: ***", "id: " + this.id + "code: " + this.code);

    }

    public Produit getProduitByCode(String code) {
        for (Produit produit : liste_produits) {
            if (produit.getCode().equals(code)) {
                return produit;
            }
        }
        return null;
    }

    public Produit getProduitById(int id) {
        for (Produit produit : liste_produits) {
            if (produit.getId() == id) {
                return produit;
            }
        }
        return null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getUnite() {
        return unite;
    }

    public void setUnite(String unite) {
        this.unite = unite;
    }

    public String getUse() {
        return use;
    }

    public void setUse(String use) {
        this.use = use;
    }

    public String getGamme() {
        return gamme;
    }

    public void setGamme(String gamme) {
        this.gamme = gamme;
    }

    public String getPoids() {
        return poids;
    }

    public void setPoids(String poids) {
        this.poids = poids;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
