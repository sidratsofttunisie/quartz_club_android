package mobi.app4mob.quartzclub.model;

import org.json.JSONObject;

import java.io.Serializable;

import mobi.app4mob.quartzclub.constants.Functions;

public class Message implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 7673100708833459808L;
    
    public static final String USERID = "user_id";
    private int user_id;
    
    public static final String COMMERCIALID = "commercial_id";
    private int commercial_id;
    
    public static final String HASSEEN = "has_seen";
    private boolean has_seen; 
    
    public static final String ISBRODCAST = "is_brodcast";
    private boolean is_brodcast;

    public static final String CREATEDAT = "created_at";
    private String created_at;

    public static final String MESSAGE = "message";
    private String message;


    public static final String SENDER = "sender";
    private String sender;

   
    public Message() {
        super();
    }

    public Message(JSONObject json) {
        super();
        this.user_id = Functions.setIntAttribute(USERID, json, 0);
        this.commercial_id = Functions.setIntAttribute(COMMERCIALID, json, 0);
        this.has_seen = Functions.setBoolAttribute(HASSEEN, json, true);
        this.is_brodcast = Functions.setBoolAttribute(ISBRODCAST, json, false);
        this.created_at = Functions.setAttribute(CREATEDAT, json, "");
        this.message = Functions.setAttribute(MESSAGE, json, "");
        this.sender = Functions.setAttribute(SENDER, json, "");

    }

	public Message(int user_id, int commercial_id, boolean has_seen,
                   boolean is_brodcast, String created_at, String message, String sender) {
		super();
		this.user_id = user_id;
		this.commercial_id = commercial_id;
		this.has_seen = has_seen;
		this.is_brodcast = is_brodcast;
		this.created_at = created_at;
		this.message = message;
		this.sender = sender;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getCommercial_id() {
		return commercial_id;
	}

	public void setCommercial_id(int commercial_id) {
		this.commercial_id = commercial_id;
	}

	public boolean isHas_seen() {
		return has_seen;
	}

	public void setHas_seen(boolean has_seen) {
		this.has_seen = has_seen;
	}

	public boolean isIs_brodcast() {
		return is_brodcast;
	}

	public void setIs_brodcast(boolean is_brodcast) {
		this.is_brodcast = is_brodcast;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}
    
    


    
    
}
