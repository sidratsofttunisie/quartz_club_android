package mobi.app4mob.quartzclub.model;

import org.json.JSONObject;

import java.io.Serializable;

import mobi.app4mob.quartzclub.constants.Functions;

/**
 * Created by Yosri Mekni <dev4.yosri@gmail.com> on 15/02/2018.
 */

public class MagInfo implements Serializable {

    public static final String ID = "id";
    private String id;

    public static final String POINT_JOUR = "point_jour";
    private int point_jour;

    public static final String MAGASIN_NAME = "magasin_name";
    private String magasin_name;

    public static final String POINT = "point";
    private String point;

    public static final String NEXT_GIFT_NAME = "next_gift_name";
    private String next_gift_name;

    public static final String NEXT_GIFT_POINT = "next_gift_point";
    private String next_gift_point;

    public static final String NEXT_GIFT_IMAGE = "next_gift_image";
    private String next_gift_image;


    public MagInfo() {
        super();

    }


    public MagInfo(JSONObject json) {
        super();

        this.id = Functions.setAttribute(ID, json, "");
        this.point_jour = Functions.setIntAttribute(POINT_JOUR, json, -1);
        this.point = Functions.setAttribute(POINT, json, "");
        this.magasin_name = Functions.setAttribute(MAGASIN_NAME, json, "");
        this.next_gift_name = Functions.setAttribute(NEXT_GIFT_NAME, json, "");
        this.next_gift_point = Functions.setAttribute(NEXT_GIFT_POINT, json, "");
        this.next_gift_image = Functions.setAttribute(NEXT_GIFT_IMAGE, json, "");
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getPoint_jour() {
        return point_jour;
    }

    public void setPoint_jour(int point_jour) {
        this.point_jour = point_jour;
    }

    public String getMagasin_name() {
        return magasin_name;
    }

    public void setMagasin_name(String magasin_name) {
        this.magasin_name = magasin_name;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getNext_gift_name() {
        return next_gift_name;
    }

    public void setNext_gift_name(String next_gift_name) {
        this.next_gift_name = next_gift_name;
    }

    public String getNext_gift_point() {
        return next_gift_point;
    }

    public void setNext_gift_point(String next_gift_point) {
        this.next_gift_point = next_gift_point;
    }

    public String getNext_gift_image() {
        return next_gift_image;
    }

    public void setNext_gift_image(String next_gift_image) {
        this.next_gift_image = next_gift_image;
    }
}

