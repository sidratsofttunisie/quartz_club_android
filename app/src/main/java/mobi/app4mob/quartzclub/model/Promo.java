package mobi.app4mob.quartzclub.model;

import java.io.Serializable;
import java.util.ArrayList;

import mobi.app4mob.quartzclub.constants.Functions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Promo implements Serializable {

    private static final long serialVersionUID = -3203313082353308422L;


    public static final String NAME = "name";
    private String name;

    public static final String COMPOINTS = "commercial_points";
    private String commercial_points;

    public static final String MAGPOINTS = "magasin_points";
    private String magasin_points;

    public static final String PACKAGE = "package";
    private ArrayList<PromoPackage> liste_promo = new ArrayList<PromoPackage>();

    public Promo(JSONObject json) throws JSONException {
        super();

        this.name = Functions.setAttribute(NAME, json, "");
        this.commercial_points = Functions.setAttribute(COMPOINTS, json, "");
        this.magasin_points = Functions.setAttribute(MAGPOINTS, json, "");

        JSONArray jsonArray = json.optJSONArray(PACKAGE);
        for (int i = 0; i < jsonArray.length(); i++) {
            PromoPackage promo_package = new PromoPackage();
            promo_package.setProduct_id(jsonArray.getJSONObject(i).getInt(PromoPackage.PRODUCT_ID));
            promo_package.setQt(jsonArray.getJSONObject(i).getInt(PromoPackage.QT));
            this.liste_promo.add(promo_package);
        }


    }


    public String getCommercial_points() {
        return commercial_points;
    }


    public void setCommercial_points(String commercial_points) {
        this.commercial_points = commercial_points;
    }


    public String getMagasin_points() {
        return magasin_points;
    }


    public void setMagasin_points(String magasin_points) {
        this.magasin_points = magasin_points;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<PromoPackage> getListe_promo() {
        return liste_promo;
    }

    public void setListe_promo(ArrayList<PromoPackage> liste_promo) {
        this.liste_promo = liste_promo;
    }


}
