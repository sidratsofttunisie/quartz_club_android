package mobi.app4mob.quartzclub.model;

import java.util.ArrayList;


public class Command {
    public static final String NUM_FACTURE = "num_facture";
    private String num_facture;

    public static final String DATE = "date";
    private String date;

    private ArrayList<CommandProduct> listProduct;
    private Magasin magasin;

    public static final String MAGASIN_ID = "magasin_id";
    private int magasin_id;


    public static final String COMMERCIAL_ID = "commercial_id";
    private int commercial_id;


    public Command(String num_facture, ArrayList<CommandProduct> listProduct,
                   int magasin_id, int commercial_id) {
        super();
        this.num_facture = num_facture;
        this.listProduct = listProduct;
        this.magasin_id = magasin_id;
        this.commercial_id = commercial_id;
    }

    public String getNum_facture() {
        return num_facture;
    }

    public void setNum_facture(String num_facture) {
        this.num_facture = num_facture;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<CommandProduct> getListProduct() {
        return listProduct;
    }

    public void setListProduct(ArrayList<CommandProduct> listProduct) {
        this.listProduct = listProduct;
    }

    public Magasin getMagasin() {
        return magasin;
    }

    public void setMagasin(Magasin magasin) {
        this.magasin = magasin;
    }

    public int getMagasin_id() {
        return magasin_id;
    }

    public void setMagasin_id(int magasin_id) {
        this.magasin_id = magasin_id;
    }

    public int getCommercial_id() {
        return commercial_id;
    }

    public void setCommercial_id(int commercial_id) {
        this.commercial_id = commercial_id;
    }


}
