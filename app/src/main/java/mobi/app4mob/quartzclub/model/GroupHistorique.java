package mobi.app4mob.quartzclub.model;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.constants.Functions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GroupHistorique {

	public static final String DATE = "date";
	private String date;

	public static final String COMMAND = "command";
	private ArrayList<Historique> historiques;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public ArrayList<Historique> getHistoriques() {
		return historiques;
	}

	public void setHistoriques(ArrayList<Historique> historiques) {
		this.historiques = historiques;
	}

	public GroupHistorique(JSONObject json) {
		super();
		ArrayList<Historique> data = new ArrayList<Historique>();
		try {
			JSONArray obj = new JSONArray(Functions.setAttribute(COMMAND, json,
					""));
			
			for (int i = 0; i < obj.length(); i++) {
				JSONObject objI = obj.getJSONObject(i);
				data.add(new Historique(objI));
			}
			// Log.i("data ", data.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		this.historiques = data;
		this.date = Functions.setAttribute(DATE, json, "");

	}

}
