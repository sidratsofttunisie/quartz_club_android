package mobi.app4mob.quartzclub.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.constants.Functions;

/**
 * Created by Yosri Mekni on 06/02/2018.
 */

public class ProduitC {

    public static final String ID = "product_id";
    private String id;

    public static final String QTE = "qt";
    private String qte;

    public static final String COMMANDID = "command_id";
    private String command_id;

    public static final String PRODUIT = "product";
    private Produit produit;


    public ProduitC() {
        super();
    }

    public ProduitC(JSONObject json) {
        super();
        this.qte= Functions.setAttribute(QTE, json, "");
        this.id= Functions.setAttribute(ID, json, "");
        this.command_id = Functions.setAttribute(COMMANDID, json, "");
        try {
            this.produit = new Produit(new JSONObject(Functions.setAttribute(PRODUIT, json, "")));
        } catch (JSONException e) {
            e.printStackTrace();
        }

//        Functions.LOGI("**** produit: ***", "qte: " + this.qte + "code: " + this.code);

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQte() {
        return qte;
    }

    public void setQte(String qte) {
        this.qte = qte;
    }

    public String getCommand_id() {
        return command_id;
    }

    public void setCommand_id(String command_id) {
        this.command_id = command_id;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }
}
