package mobi.app4mob.quartzclub.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import mobi.app4mob.quartzclub.constants.Functions;

/**
 * Created by Yosri Mekni on 05/02/2018.
 */

public class History {

    public static final String ID = "id";
    private String id;

    public static final String POINTS = "points";
    private String points;

    public static final String DATE = "date";
    private String date;

    public static final String PALIERPOINT = "palier_point";
    private String palier_point;

    public static final String NBRYEAR = "nbr_year";
    private String nbr_year;

    public static final String LABELPROMO = "label_promo";
    private String label_promo;

    public static final String COMMANDE = "commande";
    private Commande commande;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPalier_point() {
        return palier_point;
    }

    public void setPalier_point(String palier_point) {
        this.palier_point = palier_point;
    }

    public String getNbr_year() {
        return nbr_year;
    }

    public void setNbr_year(String nbr_year) {
        this.nbr_year = nbr_year;
    }

    public String getLabel_promo() {
        return label_promo;
    }

    public void setLabel_promo(String label_promo) {
        this.label_promo = label_promo;
    }

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    public History() {
        super();
    }

    public History(JSONObject json) {
        super();
        this.id = Functions.setAttribute(ID, json, "");
        this.points = Functions.setAttribute(POINTS, json, "");
        this.date = Functions.setAttribute(DATE, json, "");
        this.palier_point = Functions.setAttribute(PALIERPOINT, json, "");
        this.nbr_year = Functions.setAttribute(NBRYEAR, json, "");
        this.label_promo = Functions.setAttribute(LABELPROMO, json, "");
        JSONObject obj = null;
        try {
            obj = new JSONObject(Functions.setAttribute(COMMANDE, json, ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.commande = new Commande(obj);
    }

    public History(String id, String points, String date, String palier_point, String nbr_year, String label_promo, Commande commande) {
        super();
        this.id = id;
        this.points = points;
        this.date = date;
        this.palier_point = palier_point;
        this.nbr_year = nbr_year;
        this.label_promo = label_promo;
        this.commande = commande;
    }
}
