package mobi.app4mob.quartzclub.model;

import java.io.Serializable;

public class Photo implements Serializable{

    public static final String ID = "id";
    private String id;

    public static final String IMAGE = "image";
    private String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
