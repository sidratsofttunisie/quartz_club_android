package mobi.app4mob.quartzclub.model;

import org.json.JSONException;
import org.json.JSONObject;

import mobi.app4mob.quartzclub.constants.Functions;

/**
 * Created by Yosri Mekni <dev4.yosri@gmail.com> on 12/02/2018.
 */

public class BonusPalier {

    public static final String ID = "id";
    private String id;

    public static final String LEVEL = "level";
    private String level;

    public static final String COMPOINTS = "commercial_points";
    private String commercial_points;

    public static final String MAGPOINTS = "magasin_points";
    private String magasin_points;

    public BonusPalier(JSONObject json) throws JSONException {
        super();

        this.id = Functions.setAttribute(ID, json, "");
        this.level = Functions.setAttribute(LEVEL, json, "");
        this.magasin_points = Functions.setAttribute(MAGPOINTS, json, "");
        this.commercial_points = Functions.setAttribute(COMPOINTS, json, "");

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getCommercial_points() {
        return commercial_points;
    }

    public void setCommercial_points(String commercial_points) {
        this.commercial_points = commercial_points;
    }

    public String getMagasin_points() {
        return magasin_points;
    }

    public void setMagasin_points(String magasin_points) {
        this.magasin_points = magasin_points;
    }
}
