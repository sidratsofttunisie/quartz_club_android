package mobi.app4mob.quartzclub.model;

import java.io.Serializable;


public class PromoPackage implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4441981177333667735L;

    public static final String PRODUCT_ID = "product_id";
    private int product_id;

    public static final String QT = "qt";
    private int qt;

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getQt() {
        return qt;
    }

    public void setQt(int qt) {
        this.qt = qt;
    }


}
