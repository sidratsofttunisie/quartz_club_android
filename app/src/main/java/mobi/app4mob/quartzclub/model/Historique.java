package mobi.app4mob.quartzclub.model;

import java.io.Serializable;
import mobi.app4mob.quartzclub.constants.Functions;

import org.json.JSONObject;

public class Historique implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 7673100708833459808L;
    
    public static final String NBPOINTSMAGASIN = "magasin_points";
    private String nb_points_magasin;
    
    public static final String NBPOINTSCOMMERCIAL = "commercial_points";
    private String nb_points_commercial;
    
    public static final String ID = "id";
    private int id;

    public static final String CREATEDAT = "created_at";
    private String created_at;

    public static final String NB_BOUTEILLE = "nb_boutielle";
    private String nb_bouteille;

   
    public Historique() {
        super();
    }

    public Historique(JSONObject json) {
        super();
        this.id = Functions.setIntAttribute(ID, json, 0);
        this.nb_bouteille = Functions.setAttribute(NB_BOUTEILLE, json, "0");
        this.nb_points_magasin = Functions.setAttribute(NBPOINTSMAGASIN, json, "0");
        this.nb_points_commercial = Functions.setAttribute(NBPOINTSCOMMERCIAL, json, "0");
        this.created_at = Functions.setAttribute(CREATEDAT, json, "");

    }
    
    

	public Historique(int id, String nb_bouteille, String nb_points_magasin, String nb_points_commercial, String created_at) {
		super();
		this.id = id;
		this.nb_bouteille = nb_bouteille;
		this.nb_points_magasin = nb_points_magasin;
		this.nb_points_commercial = nb_points_commercial;
		this.created_at = created_at;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getNb_bouteille() {
		return nb_bouteille;
	}

	public void setNb_bouteille(String nb_bouteille) {
		this.nb_bouteille = nb_bouteille;
	}

	public String getNb_points_magasin() {
		return nb_points_magasin;
	}

	public void setNb_points_magasin(String nb_points_magasin) {
		this.nb_points_magasin = nb_points_magasin;
	}

	public String getNb_points_commercial() {
		return nb_points_commercial;
	}

	public void setNb_points_commercial(String nb_points_commercial) {
		this.nb_points_commercial = nb_points_commercial;
	}



    
    
}
