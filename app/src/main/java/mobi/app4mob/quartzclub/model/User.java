package mobi.app4mob.quartzclub.model;

import java.io.Serializable;

import org.json.JSONObject;


import mobi.app4mob.quartzclub.constants.Functions;

public class User implements Serializable {

    private static final long serialVersionUID = -2879625746004330926L;


    @Override
    public String toString() {
        return "User [first_name=" + first_name + "]";
    }


    public static final String ID = "id";
    private int id;

    public static final String FIRST_NAME = "first_name";
    private String first_name;

    public static final String LAST_NAME = "last_name";
    private String last_name;

    private String full_name;

    public static final String NBMESSAGE = "nb_message";
    private int nb_message;

    public static final String LASTMESSAGE = "last_message";
    private String last_message;

    public static final String EMAIL = "email";
    private String email;

    public static final String USERNAME = "username";
    private String username;

    public static final String PASSWORD = "password";
    private String password;

    public static final String POINT = "point";
    private int point;

    public static final String POINT_JOUR = "point_jour";
    private int point_jour;

    public static final String TARGET_GIFT = "target_gift";
    private Gift target_gift;

    public static final String PAYS_ID = "pays_id";
    private String pays_id;


    public User() {
        super();
    }

    public User(JSONObject json) {
        super();
        this.id = Functions.setIntAttribute(ID, json, 0);
        this.last_name = Functions.setAttribute(LAST_NAME, json, "");
        this.first_name = Functions.setAttribute(FIRST_NAME, json, "");
        this.pays_id = Functions.setAttribute(PAYS_ID, json, "");
        this.email = Functions.setAttribute(EMAIL, json, "");
        this.username = Functions.setAttribute(USERNAME, json, "");
        this.point = Functions.setIntAttribute(POINT, json, 0);
        this.point_jour = Functions.setIntAttribute(POINT_JOUR, json, 0);
        this.target_gift = new Gift(json.optJSONObject(TARGET_GIFT));
        this.nb_message = Functions.setIntAttribute(NBMESSAGE, json, 0);
        this.last_message = Functions.setAttribute(LASTMESSAGE, json, "");

    }


    public User(int id, String pays_id, String first_name, String last_name, String email,
                String username, String password, int point, Gift target_gift, int nb_message, String last_message,int point_jour) {
        super();
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.pays_id = pays_id;
        this.username = username;
        this.password = password;
        this.point = point;
        this.target_gift = target_gift;
        this.nb_message = nb_message;
        this.last_message = last_message;
        this.point_jour = point_jour;
    }

    public User(int id ,int nb_message,String last_message, String first_name, String last_name, String email,
                String username, String password, int point, Gift target_gift) {
        super();
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.username = username;
        this.password = password;
        this.point = point;
        this.target_gift = target_gift;
        this.nb_message = nb_message;
        this.last_message = last_message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFull_name() {
        return this.first_name + " " + this.last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public Gift getTarget_gift() {
        return target_gift;
    }

    public void setTarget_gift(Gift target_gift) {
        this.target_gift = target_gift;
    }

    public int getNb_message() {
        return nb_message;
    }

    public void setNb_message(int nb_message) {
        this.nb_message = nb_message;
    }

    public String getLast_message() {
        return last_message;
    }

    public void setLast_message(String last_message) {
        this.last_message = last_message;
    }

    public String getPays_id() {
        return pays_id;
    }

    public void setPays_id(String pays_id) {
        this.pays_id = pays_id;
    }

    public int getPoint_jour() {
        return point_jour;
    }

    public void setPoint_jour(int point_jour) {
        this.point_jour = point_jour;
    }
}
