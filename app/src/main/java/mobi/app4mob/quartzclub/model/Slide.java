package mobi.app4mob.quartzclub.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.constants.Functions;

/**
 * Created by yosri on 27/02/2018.
 */

public class Slide {

    public static final String ID = "id";
    private String id;

    public static final String TITRE = "titre";
    private String titre;

    public static final String PHOTOS = "Photos";
    private ArrayList<Photo> photos = new ArrayList<>();

    public Slide() {
        super();

    }


    public Slide(JSONObject json) throws JSONException {
        super();

        this.id = Functions.setAttribute(ID, json, "");
        this.titre = Functions.setAttribute(TITRE, json, "");
        JSONArray jsonArray = json.optJSONArray(PHOTOS);
        for (int i = 0; i < jsonArray.length(); i++) {
            Photo photo = new Photo();
            photo.setId(jsonArray.getJSONObject(i).getString(Photo.ID));
            photo.setImage(jsonArray.getJSONObject(i).getString(Photo.IMAGE));
            this.photos.add(photo);
        }

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public ArrayList<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<Photo> photos) {
        this.photos = photos;
    }
}

