package mobi.app4mob.quartzclub.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

import mobi.app4mob.quartzclub.constants.Functions;

/**
 * Created by Yosri Mekni <dev4.yosri@gmail.com> on 08/02/2018.
 */

public class Bonus implements Serializable {

    private static final long serialVersionUID = -3203313082353308422L;


    public static final String ID = "id";
    private String id;

    public static final String NAME = "name";
    private String name;

    public static final String COMPOINTS = "commercial_points";
    private String commercial_points;

    public static final String MAGPOINTS = "magasin_points";
    private String magasin_points;

    public static final String UNLIMITED = "is_unlimited";
    private boolean is_unlimited;

    public static final String ISPACKAGE = "is_package";
    private boolean is_package;

    public static final String STARTAT = "start_at";
    private String start_at;

    public static final String ENDAT = "end_at";
    private String end_at;

    public static final String PACKAGE = "package";
    private ArrayList<BonusPackage> liste_bonus_pack = new ArrayList<BonusPackage>();





    public Bonus(JSONObject json) throws JSONException {
        super();

        this.name = Functions.setAttribute(NAME, json, "");
        this.commercial_points = Functions.setAttribute(COMPOINTS, json, "");
        this.magasin_points = Functions.setAttribute(MAGPOINTS, json, "");
        this.start_at = Functions.setAttribute(STARTAT, json, "");
        this.end_at = Functions.setAttribute(ENDAT, json, "");
        this.id = Functions.setAttribute(ID, json, "");
        this.is_package = Functions.setBoolAttribute(ISPACKAGE, json, false);
        this.is_unlimited = Functions.setBoolAttribute(UNLIMITED, json, false);

        JSONArray jsonArray = json.optJSONArray(PACKAGE);
        for (int i = 0; i < jsonArray.length(); i++) {
            BonusPackage bonus_package = new BonusPackage();
            bonus_package.setProduct_id(jsonArray.getJSONObject(i).getInt(BonusPackage.PRODUCT_ID));
            bonus_package.setQt(jsonArray.getJSONObject(i).getInt(BonusPackage.QT));
            bonus_package.setProduct(new Produit(jsonArray.getJSONObject(i).getJSONObject(BonusPackage.PRODUCT)));
            this.liste_bonus_pack.add(bonus_package);
        }


    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCommercial_points() {
        return commercial_points;
    }

    public void setCommercial_points(String commercial_points) {
        this.commercial_points = commercial_points;
    }

    public String getMagasin_points() {
        return magasin_points;
    }

    public void setMagasin_points(String magasin_points) {
        this.magasin_points = magasin_points;
    }

    public boolean isIs_unlimited() {
        return is_unlimited;
    }

    public void setIs_unlimited(boolean is_unlimited) {
        this.is_unlimited = is_unlimited;
    }

    public boolean isIs_package() {
        return is_package;
    }

    public void setIs_package(boolean is_package) {
        this.is_package = is_package;
    }

    public String getStart_at() {
        return start_at;
    }

    public void setStart_at(String start_at) {
        this.start_at = start_at;
    }

    public String getEnd_at() {
        return end_at;
    }

    public void setEnd_at(String end_at) {
        this.end_at = end_at;
    }

    public ArrayList<BonusPackage> getListe_bonus_pack() {
        return liste_bonus_pack;
    }

    public void setListe_bonus_pack(ArrayList<BonusPackage> liste_bonus_pack) {
        this.liste_bonus_pack = liste_bonus_pack;
    }
}
