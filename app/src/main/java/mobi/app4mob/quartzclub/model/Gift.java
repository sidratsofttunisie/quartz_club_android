package mobi.app4mob.quartzclub.model;

import java.io.Serializable;

import mobi.app4mob.quartzclub.constants.Functions;

import org.json.JSONObject;

public class Gift implements Serializable {

	private static final long serialVersionUID = 2L;

	public static final String ID = "id";
	private int id;

	public static final String NAME = "title";
	private String name;

	public static final String COMMERCIALPOINTS = "commercial_points";
	private String commercial_points;

	public static final String MAGASINPOINTS = "magasin_points";
	private String magasin_points;

	public static final String IMAGE = "image";
	private String image;

	public static final String TARGETGIFT = "target_gift";
	private Boolean target_gift;

	public Gift(JSONObject json) {
		super();
		if (json != null && json.has(NAME)) {
			this.id = Functions.setIntAttribute(ID, json, 0);
			this.name = Functions.setAttribute(NAME, json, "");
			this.commercial_points = Functions.setAttribute(COMMERCIALPOINTS, json, "");
			this.magasin_points = Functions.setAttribute(MAGASINPOINTS, json, "");
			this.image = Functions.setAttribute(IMAGE, json, "");
			this.target_gift = Functions.setBoolAttribute(TARGETGIFT, json,
					false);
		}

	}

	public Gift(String name, String commercial_points, String magasin_points, String image) {
		super();
		this.name = name;
		this.commercial_points = commercial_points;
		this.magasin_points = magasin_points;
		this.image = image;
	}

	public Gift(int id, String name, String commercial_points, String magasin_points, String image) {
		super();
		this.id = id;
		this.name = name;
		this.commercial_points = commercial_points;
		this.magasin_points = magasin_points;
		this.image = image;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCommercial_points() {
		return commercial_points;
	}

	public void setCommercial_points(String commercial_points) {
		this.commercial_points = commercial_points;
	}

	public String getMagasin_points() {
		return magasin_points;
	}

	public void setMagasin_points(String magasin_points) {
		this.magasin_points = magasin_points;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
