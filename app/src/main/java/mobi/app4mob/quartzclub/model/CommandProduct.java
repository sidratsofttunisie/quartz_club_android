package mobi.app4mob.quartzclub.model;

import java.io.Serializable;
import java.util.ArrayList;


public class CommandProduct implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 5981850025057283365L;
    final Boolean DELETE_ME = true;
    private Produit product;
    private ArrayList<String> liste_code;

    public Produit getProduct() {
        return product;
    }

    public void setProduct(Produit product) {
        this.product = product;
    }

    public ArrayList<String> getListe_code() {
        return liste_code;
    }

    public void setListe_code(ArrayList<String> liste_code) {
        this.liste_code = liste_code;
    }

    public int getPosOfRef(String find_ref) {
        int i = 0;
        for (String ref : this.getListe_code()) {
            if (ref.equals(find_ref))
                return i;
            i++;
        }
        return -1;
    }

    //return  true si le produit contient 0 référence
    public Boolean deleteRef(String ref_to_delete) {

        int pos = this.getPosOfRef(ref_to_delete);
        if (pos >= 0) {

            this.getListe_code()
                    .remove(pos);
            if (this.getListe_code().size() == 0) {
                return DELETE_ME;
            }
        }
        return false;

    }


}
