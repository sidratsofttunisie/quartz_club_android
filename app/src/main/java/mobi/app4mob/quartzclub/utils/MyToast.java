package mobi.app4mob.quartzclub.utils;

import mobi.app4mob.quartzclub.activity.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class MyToast extends Toast {

    public MyToast(Context context, String Msg, int duration) {

        super(context);
        View v = LayoutInflater.from(context).inflate(R.layout.toast, null);
        this.setView(v);
        TextView msg = (TextView) v.findViewById(R.id.msgToast);
        msg.setText(Msg);
        this.setDuration(duration);
        this.show();

    }

    public MyToast(Context context, int Msg, int duration) {

        super(context);
        View v = LayoutInflater.from(context).inflate(R.layout.toast, null);
        this.setView(v);
        TextView msg = (TextView) v.findViewById(R.id.msgToast);
        String message = context.getString(Msg);
        msg.setText(message);
        this.setDuration(duration);
        this.show();

    }

}
