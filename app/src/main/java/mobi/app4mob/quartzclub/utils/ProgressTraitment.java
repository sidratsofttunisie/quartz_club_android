package mobi.app4mob.quartzclub.utils;

import mobi.app4mob.quartzclub.activity.R;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;


public abstract class ProgressTraitment {

    private Context context;
    private Dialog myProgressDialog;
    final Handler uiThreadCallback = new Handler();

    public ProgressTraitment(Context c) {
        super();
        this.context = c;
    }

    public abstract void onLoadStarted();

    public abstract void onLoadFinished();

    public void DoTraitemnt(String Message, String Title) {
        ChecKConnection c = new ChecKConnection(context);

        if (c.verifierconnexion()) {

            myProgressDialog = new Dialog(context, R.style.progress_dialog);
            myProgressDialog.setContentView(R.layout.progress_dialog);
            // TextView message=(TextView) myProgressDialog.findViewById(R.id.lblMessage);
            // message.setText(Message);
            myProgressDialog.setCancelable(true);
            if (!((Activity) context).isFinishing()) {
                myProgressDialog.show();
            }

            final Runnable runInUIThread = new Runnable() {
                @Override
                public void run() {
                    // afficher résultat
                    ProgressTraitment.this.onLoadFinished();

                }
            };
            new Thread() {
                @Override
                public void run() {
                    Looper.prepare();
                    // Do traiment ici
                    ProgressTraitment.this.onLoadStarted();
                    if (!((Activity) context).isFinishing()) {
                        myProgressDialog.dismiss();
                    }
                    uiThreadCallback.post(runInUIThread);
                }
            }.start();
        } else {
            new MyToast(context, context.getString(R.string.no_connection), Toast.LENGTH_SHORT);
        }

    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

}
