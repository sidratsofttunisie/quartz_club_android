package mobi.app4mob.quartzclub.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import android.util.Log;

import com.android.internal.http.multipart.MultipartEntity;
import com.android.internal.http.multipart.Part;

import mobi.app4mob.quartzclub.activity.AddMagasinActivity;
import mobi.app4mob.quartzclub.constants.Constants;
import nl.changer.polypicker.model.Image;


public class WebClient {
    private ArrayList<NameValuePair> params;

    private String url;

    private int responseCode;
    private String httpmessage;

    private InputStream response;

    HashSet<Image> mMediaImages;

    public HashSet<Image> getmMediaImages() {
        return mMediaImages;
    }

    public InputStream getResponse() {
        return response;
    }

    public String getHttpMessage() {
        return httpmessage;
    }

    public int getResponseCode() {
        return responseCode;
    }


    public String inputStreamToString(InputStream is) {
        String s = "";
        String line = "";

        // Wrap a BufferedReader around the InputStream
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

        // Read response until the end
        try {
            while ((line = rd.readLine()) != null) {
                s += line;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Return full string
        return s;
    }

    public String getStringResponse() {
        String resp = null;
        if (getResponse() != null) {
            resp = inputStreamToString(getResponse());
        } else {
            Log.i("INPUTSTREAM", "null input stream");
        }
        return resp;
    }


    public WebClient(String url, ArrayList<NameValuePair> params) {
        this.url = url;
        if (params == null) {
            this.params = new ArrayList<NameValuePair>();
        } else {
            this.params = params;
        }
    }

    public WebClient(String url, ArrayList<NameValuePair> params,HashSet<Image> mMediaImages) {
        this.url = url;
        if (params == null) {
            this.params = new ArrayList<NameValuePair>();
        } else {
            this.params = params;
        }

        if (mMediaImages == null) {
            this.mMediaImages = new HashSet<Image>();
        } else {
            this.mMediaImages = mMediaImages;
        }
    }

    public void execute(int method) {
        switch (method) {
            case Constants.HTTP_METHOD_GET: {
                // add parameters
                String combinedParams = "";
                combinedParams = getParams(params);

                HttpGet request = new HttpGet(url + combinedParams);
                if (Constants.ENABLE_LOGS == true) {
                    Log.i(Constants.APP_NAME, "WS_URL_GET = " + url + combinedParams);
                }

                executeRequest(request, url);
            }
            break;

            case Constants.HTTP_METHOD_POST: {
                HttpPost request = new HttpPost(url);
                if (mMediaImages!=null && mMediaImages.size()>0){
                MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
                entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
                for (Image img : mMediaImages) {
                    File ff = new File(img.mUri.toString());
                    FileBody fileBody = new FileBody(ff, img.mUri.toString().substring(img.mUri.toString().lastIndexOf("/") + 1,img.mUri.toString().length()), "image/jpeg", "UTF-8");
                    System.out.println("FILE PATH"+ff.getPath()+" / "+img.mUri.toString().substring(img.mUri.toString().lastIndexOf("/") + 1,img.mUri.toString().length()) );
                    entityBuilder.addPart("photos", fileBody);
                }
                // add more key/value pairs here as needed

                HttpEntity entity = entityBuilder.build();
                request.setEntity(entity);
                request.getParams().setParameter("Content-Type", "multipart/form-data;");
                }
                String combinedParams = "";
                if (!params.isEmpty()) {
                    try {
                        request.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));

                        combinedParams = getParams(params);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                executeRequest(request, url);
                if (Constants.ENABLE_LOGS == true) {
                    Log.i(Constants.APP_NAME, "WS_URL_POST = " + url + combinedParams);
                }

            }
            break;

            default:
                break;
        }
    }

    private String getParams(ArrayList<NameValuePair> param) {
        String combineParams = "";
        if (!param.isEmpty()) {
            combineParams += "?";
            for (NameValuePair p : param) {
                String paramString = null;
                try {
                    paramString = p.getName() + "=" + URLEncoder.encode(p.getValue(), "UTF-8");
                } catch (Exception e) {

                    e.printStackTrace();
                }
                if (combineParams.length() > 1) {
                    combineParams += "&" + paramString;
                } else {
                    combineParams += paramString;
                }
            }
        }

        return combineParams;
    }

    private void executeRequest(HttpUriRequest request, String url) {
        HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, Constants.CONNECTION_TIME_OUT);
        HttpConnectionParams.setSoTimeout(httpParams, Constants.SOCKET_TIME_OUT);
        // Client
        DefaultHttpClient client = new DefaultHttpClient(httpParams);
        client.getParams().setParameter("Content-Type", "multipart/form-data;");
        // Authorizing
        client.getCredentialsProvider().setCredentials(
                new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT, AuthScope.ANY_SCHEME),
                new UsernamePasswordCredentials(Constants.WS_USER_NAME, Constants.WS_PASSWORD));

        HttpResponse httpResponse;

        try {
            httpResponse = client.execute(request);
            responseCode = httpResponse.getStatusLine().getStatusCode();
            httpmessage = httpResponse.getStatusLine().getReasonPhrase();

            HttpEntity entity = httpResponse.getEntity();

            if (entity != null) {
                response = entity.getContent();
            }

        } catch (ClientProtocolException e) {
            client.getConnectionManager().shutdown();
            e.printStackTrace();
        } catch (IOException e) {
            client.getConnectionManager().shutdown();
            e.printStackTrace();
        }
    }


}