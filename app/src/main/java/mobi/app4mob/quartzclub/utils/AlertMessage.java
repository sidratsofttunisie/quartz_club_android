package mobi.app4mob.quartzclub.utils;

import kankan.wheel.widget.WheelView;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;


public abstract class AlertMessage {
    Context context;
    public static CustomDialog.Builder dialog;
    public static WheelView wheelProp;
    public static WheelView wheelMin;
    public static WheelView wheelMax;
    public static WheelView wheelDay;
    public static WheelView wheelMonth;
    public static WheelView wheelYear;

    private String sBefore;
    public static EditText editMsgJour;

    public static final String months[] = new String[]{"Janvier", "Février", "Mars", "Avril", "Mai", "Juin",
            "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"};
    // public static int minYear;
    public static int maxYear;

    public static EditText editLastPwd;
    public static EditText editNewPwd1;
    public static EditText editNewPwd2;

    public AlertMessage(Context ctx) {
        super();
        context = ctx;

    }

    public abstract void onOk();

    public abstract void onCancel();

    public void show1(String message, String title, boolean cancelable, String ok) {
        dialog = new CustomDialog.Builder(context);
        dialog.setTitle(title).setMessage(message).setPositiveButton(ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onOk();
            }
        }).setCancelable(cancelable);

        if (!((Activity) context).isFinishing()) {
            Dialog d = dialog.create();
            d.show();
        }

    }

    public void show2(String message, String title, boolean cancelable, String ok, String cancel) {
        dialog = new CustomDialog.Builder(context);
        dialog.setTitle(title).setMessage(message).setPositiveButton(ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onOk();
            }
        }).setNegativeButton(cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onCancel();
            }
        }).setCancelable(cancelable);

        if (!((Activity) context).isFinishing()) {
            Dialog d = dialog.create();
            d.show();
        }

    }


}
