package mobi.app4mob.quartzclub.utils;

import mobi.app4mob.quartzclub.model.Gift;
import mobi.app4mob.quartzclub.model.User;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SessionManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "AndroidHivePref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String WITH_LONG_SESSION = "WithLongSession";

    public static final String ID = "user_id";

    public static final String FIRST_NAME = "user_first_name";

    public static final String LAST_NAME = "user_last_name";

    public static final String EMAIL = "user_email";

    public static final String USERNAME = "user_name";

    public static final String PASSWORD = "user_password";

    public static final String POINT = "user_point";

    public static final String GIFT_ID = "gift_id";

    public static final String GIFT_NAME = "gift_title";

    public static final String GIFT_COMMERCIAL_POINTS = "gift_commercial_points";

    public static final String GIFT_MAGASIN_POINTS = "gift_magasin_points";

    public static final String GIFT_IMAGE = "gift_image";

    public static final String PAYS_ID = "pays_id";

    public static final String LAST_MESSAGE = "last_message";

    public static final String NB_MESSAGE = "nb_message";
    public static final String POINT_JOUR = "point_jour";


    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     *
     * @param user
     */
    public void createLoginSession(User user, Boolean with_long_session) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        editor.putBoolean(WITH_LONG_SESSION, with_long_session);

        // Storing data in pref
        editor.putInt(ID, user.getId());
        editor.putString(FIRST_NAME, user.getFirst_name());
        editor.putString(LAST_NAME, user.getLast_name());
        editor.putString(PAYS_ID, user.getPays_id());
        editor.putString(EMAIL, user.getEmail());
        editor.putString(PASSWORD, user.getPassword());
        editor.putInt(POINT, user.getPoint());
        editor.putInt(POINT_JOUR, user.getPoint_jour());
        editor.putInt(GIFT_ID, user.getTarget_gift().getId());
        editor.putString(GIFT_NAME, user.getTarget_gift().getName());
        editor.putInt(GIFT_COMMERCIAL_POINTS, Integer.parseInt(user.getTarget_gift().getCommercial_points()));
        editor.putInt(GIFT_MAGASIN_POINTS, Integer.parseInt(user.getTarget_gift().getMagasin_points()));
        editor.putString(GIFT_IMAGE, user.getTarget_gift().getImage());
        editor.putInt(NB_MESSAGE, user.getNb_message());
        editor.putString(LAST_MESSAGE, user.getLast_message());

        // commit changes
        editor.commit();
    }

    /**
     * Update login session
     *
     * @param user
     */
    public void updateSession(User user) {
        // Storing login value as TRUE
        System.out.println(">>>>>>>>>>>"+user.getNb_message());
        editor.putBoolean(IS_LOGIN, true);

        // Storing data in pref
        editor.putInt(ID, user.getId());
        editor.putString(FIRST_NAME, user.getFirst_name());
        editor.putString(LAST_NAME, user.getLast_name());
        editor.putString(PAYS_ID, user.getPays_id());
        editor.putString(EMAIL, user.getEmail());
        editor.putString(PASSWORD, user.getPassword());
        editor.putInt(POINT, user.getPoint());
        editor.putInt(POINT_JOUR, user.getPoint_jour());
        editor.putInt(GIFT_ID, user.getTarget_gift().getId());
        editor.putString(GIFT_NAME, user.getTarget_gift().getName());
        editor.putInt(GIFT_COMMERCIAL_POINTS, Integer.parseInt(user.getTarget_gift().getCommercial_points()));
        editor.putInt(GIFT_MAGASIN_POINTS, Integer.parseInt(user.getTarget_gift().getMagasin_points()));
        editor.putString(GIFT_IMAGE, user.getTarget_gift().getImage());
        editor.putInt(NB_MESSAGE, user.getNb_message());
        editor.putString(LAST_MESSAGE, user.getLast_message());

        // commit changes
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     */
    public Boolean checkLogin() {
        // Check login status
        return this.isLoggedIn();
    }


    /**
     * Get stored session data
     */
    public User getUserDetails() {
        User user;
        Gift gift;
        //intit gift
        gift = new Gift(pref.getInt(GIFT_ID, 0), pref.getString(GIFT_NAME, ""), String.valueOf(pref.getInt(GIFT_COMMERCIAL_POINTS, 0)), String.valueOf(pref.getInt(GIFT_MAGASIN_POINTS, 0)), pref.getString(GIFT_IMAGE, ""));

        //intit user
        user = new User(pref.getInt(ID, -1),pref.getString(PAYS_ID, ""), pref.getString(FIRST_NAME, ""), pref.getString(LAST_NAME, ""), pref.getString(EMAIL, ""), pref.getString(USERNAME, ""), pref.getString(PASSWORD, ""), pref.getInt(POINT, 0), gift,pref.getInt(NB_MESSAGE,0),pref.getString(LAST_MESSAGE,""),pref.getInt(POINT_JOUR,0));

        // return user
        return user;
    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
        /* 
        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
         
        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
         
        // Staring Login Activity
        _context.startActivity(i);*/
    }

    /**
     * Quick check for login
     * *
     */
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    /**
     * Quick check garder session
     * *
     */
    // Get Session State
    public boolean withLongSession() {
        return pref.getBoolean(WITH_LONG_SESSION, false);
    }
}
