package mobi.app4mob.quartzclub.adapter;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.activity.R;
import mobi.app4mob.quartzclub.model.Magasin;
import mobi.app4mob.quartzclub.utils.BitmapManager;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ListMagasinsAdapter extends BaseAdapter {

    private Context context;
    private static LayoutInflater inflater = null;
    private ArrayList<Magasin> arrayMagasins;

    public ListMagasinsAdapter(Context _context, ArrayList<Magasin> _arrayMagasins) {
        context = _context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        arrayMagasins = _arrayMagasins;

        BitmapManager.INSTANCE.setPlaceholder(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.img));

    }

    @Override
    public int getCount() {
        return this.arrayMagasins.size();
    }

    @Override
    public Object getItem(int position) {
        return this.arrayMagasins.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.arrayMagasins.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = inflater.inflate(R.layout.cell_listmagasin, parent, false);

        if (position < arrayMagasins.size()) {

            ((TextView) v.findViewById(R.id.adresse)).setVisibility(View.VISIBLE);
            ((TextView) v.findViewById(R.id.adresse)).setText(arrayMagasins.get(position).getAddress());

            ((TextView) v.findViewById(R.id.name)).setVisibility(View.VISIBLE);
            ((TextView) v.findViewById(R.id.name)).setText(arrayMagasins.get(position).getMagasin_name().toUpperCase());

        }

        return v;

    }

}
