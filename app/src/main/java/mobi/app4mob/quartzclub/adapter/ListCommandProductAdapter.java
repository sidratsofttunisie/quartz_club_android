package mobi.app4mob.quartzclub.adapter;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.activity.AddCommandActivity;
import mobi.app4mob.quartzclub.activity.R;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.CommandProduct;
import mobi.app4mob.quartzclub.utils.BitmapManager;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Shader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListCommandProductAdapter extends BaseAdapter {

    private Context context;
    private static LayoutInflater inflater = null;
    private ArrayList<CommandProduct> arrayCommandProducts;

    public ListCommandProductAdapter(Context _context, ArrayList<CommandProduct> _arrayCommandProducts) {
        context = _context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        arrayCommandProducts = _arrayCommandProducts;

        BitmapManager.INSTANCE.setPlaceholder(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.img_detail_default));

    }

    @Override
    public int getCount() {
        return this.arrayCommandProducts.size();
    }

    @Override
    public Object getItem(int position) {
        return this.arrayCommandProducts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.arrayCommandProducts.get(position).getProduct().getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = inflater.inflate(R.layout.cell_list_command_product, parent, false);

        if (position < arrayCommandProducts.size()) {


            ((TextView) v.findViewById(R.id.qt_product)).setText("X" + arrayCommandProducts.get(position).getListe_code().size());

			/*Shader myShader = new LinearGradient( 
			    0, 0, 0, 100, 
			    0xFFFFFFFF, Color.BLACK, 
			    Shader.TileMode.CLAMP );*/


            Shader txtShad = AddCommandActivity.txtShad;

            ((TextView) v.findViewById(R.id.qt_product)).getPaint().setShader(txtShad);

            ((TextView) v.findViewById(R.id.adresse)).setVisibility(View.VISIBLE);
            ((TextView) v.findViewById(R.id.adresse)).setText(arrayCommandProducts.get(position).getProduct().getCode());

            ((TextView) v.findViewById(R.id.name)).setVisibility(View.VISIBLE);
            ((TextView) v.findViewById(R.id.name)).setText(arrayCommandProducts.get(position).getProduct().getName());

            if (!Functions.isStringEmpty(arrayCommandProducts.get(position).getProduct().getImage())) {
                int width = (int) context.getResources().getDimension(R.dimen.width_img);
                int height = (int) context.getResources().getDimension(R.dimen.height_img);
                String url = Constants.DOMAIN + Constants.UPLOADS_PRODUCT_FOLDER + "/" + arrayCommandProducts.get(position).getProduct().getImage();
                ImageView img = (ImageView) v.findViewById(R.id.product_image);
                BitmapManager.INSTANCE.loadBitmap(url, img, width, height);
            }
        }

        return v;

    }

}
