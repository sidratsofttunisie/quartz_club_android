package mobi.app4mob.quartzclub.adapter;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.activity.ListBonusActivity;
import mobi.app4mob.quartzclub.activity.R;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.Gift;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.utils.BitmapManager;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ListGiftsAdapter extends BaseAdapter {

    private Context context;
    private static LayoutInflater inflater = null;
    private ArrayList<Gift> arrayGifts;
    private User user;
    private int type_profil;
    private Boolean cible = false;
    public ListGiftsAdapter(Context _context, ArrayList<Gift> _arrayGifts,
                            User user, int type_profil,Boolean cible) {
        context = _context;
        this.user = user;
        this.type_profil = type_profil;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        arrayGifts = _arrayGifts;

        BitmapManager.INSTANCE.setPlaceholder(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.img_detail_default));
        this.cible=cible;
    }

    @Override
    public int getCount() {
        return this.arrayGifts.size();
    }

    @Override
    public Object getItem(int position) {
        return this.arrayGifts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.arrayGifts.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = inflater.inflate(R.layout.cell_list_gift, parent, false);

        if (position < arrayGifts.size()) {

            ((TextView) v.findViewById(R.id.gift_name)).setText(arrayGifts.get(
                    position).getName());

            TextView gift_point_text_edit = (TextView) v
                    .findViewById(R.id.gift_points);

            ImageView blockedgiftimg = (ImageView) v.findViewById(R.id.img_blockedgift);
            LinearLayout blockedgift = (LinearLayout) v.findViewById(R.id.blockedgift);

//            gift_point_text_edit.getPaint().setShader(
//                    new LinearGradient(200, 100, 10, gift_point_text_edit
//                            .getHeight(), 0xFFEFDC6F, 0xFFB98827,
//                            Shader.TileMode.MIRROR));
            if (cible){
                gift_point_text_edit.setTextColor(context.getResources().getColor(R.color.green));
                blockedgift.setVisibility(View.GONE);
                blockedgiftimg.setVisibility(View.GONE);
                if (type_profil == ListBonusActivity.COMMERCIALBONUS)
                    gift_point_text_edit.setText(""
                            + arrayGifts.get(position).getCommercial_points());
                else
                    gift_point_text_edit.setText(""
                            + arrayGifts.get(position).getMagasin_points());

                if (!Functions.isStringEmpty(arrayGifts.get(position).getImage())) {
                    int width = (int) context.getResources().getDimension(
                            R.dimen.width_img_gift);
                    int height = (int) context.getResources().getDimension(
                            R.dimen.height_img_gift);
                    String url = Constants.DOMAIN + Constants.UPLOADS_GIFT_FOLDER
                            + "/" + arrayGifts.get(position).getImage();
                    ImageView img = (ImageView) v.findViewById(R.id.image);
                    BitmapManager.INSTANCE.loadBitmap(url, img, width, height);
                }
            }else{
                //color gold if is unblocked
                if (user.getPoint() >= Integer.parseInt(arrayGifts.get(position).getCommercial_points())) {
                    gift_point_text_edit.setTextColor(context.getResources().getColor(R.color.green));
                    blockedgift.setVisibility(View.GONE);
                    blockedgiftimg.setVisibility(View.GONE);

                }
                //color gray if blocked
                else {
                    // gift_point_text_edit.getPaint().setShader(
                    // new LinearGradient(200, 100, 10, gift_point_text_edit
                    // .getHeight(), 0xFFEFFFFF, 0xFF000000,
                    // Shader.TileMode.MIRROR));
                    blockedgift.setVisibility(View.VISIBLE);
                    blockedgiftimg.setVisibility(View.VISIBLE);
                    gift_point_text_edit.setTextColor(context.getResources().getColor(R.color.color_gray));
                }

                if (type_profil == ListBonusActivity.COMMERCIALBONUS)
                    gift_point_text_edit.setText(""
                            + arrayGifts.get(position).getCommercial_points());
                else
                    gift_point_text_edit.setText(""
                            + arrayGifts.get(position).getMagasin_points());

                if (!Functions.isStringEmpty(arrayGifts.get(position).getImage())) {
                    int width = (int) context.getResources().getDimension(
                            R.dimen.width_img_gift);
                    int height = (int) context.getResources().getDimension(
                            R.dimen.height_img_gift);
                    String url = Constants.DOMAIN + Constants.UPLOADS_GIFT_FOLDER
                            + "/" + arrayGifts.get(position).getImage();
                    ImageView img = (ImageView) v.findViewById(R.id.image);
                    BitmapManager.INSTANCE.loadBitmap(url, img, width, height);
                }
            }


        }

        return v;

    }

}
