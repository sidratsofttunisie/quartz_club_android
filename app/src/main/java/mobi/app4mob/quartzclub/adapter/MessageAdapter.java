package mobi.app4mob.quartzclub.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.activity.R;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.Message;
import mobi.app4mob.quartzclub.utils.BitmapManager;

public class MessageAdapter extends BaseAdapter {

	private Context context;
	private static LayoutInflater inflater = null;
	private ArrayList<Message> arrayMessages;


	public MessageAdapter(Context _context,
                          ArrayList<Message> _arrayMessages) {
		context = _context;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		arrayMessages = _arrayMessages;

		BitmapManager.INSTANCE.setPlaceholder(BitmapFactory.decodeResource(
				context.getResources(), R.drawable.img_detail_default));

	}

	@Override
	public int getCount() {
		return this.arrayMessages.size();
	}

	@Override
	public Object getItem(int position) {
		return this.arrayMessages.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = inflater.inflate(R.layout.cell_list_message, parent, false);

		if (position < arrayMessages.size()) {

			((TextView) v.findViewById(R.id.date)).setText(Functions
					.getFormatedDateHours(arrayMessages.get(position)
							.getCreated_at())
					+ "");

			((TextView) v.findViewById(R.id.sender))
			.setText(arrayMessages.get(position).getSender()
					+ "");
	
			((TextView) v.findViewById(R.id.message))
			.setText(arrayMessages.get(position).getMessage()
					+ "");
	

		}

		return v;

	}

}
