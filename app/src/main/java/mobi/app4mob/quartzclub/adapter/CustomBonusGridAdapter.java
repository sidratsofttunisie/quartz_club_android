package mobi.app4mob.quartzclub.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.activity.ListBonusActivity;
import mobi.app4mob.quartzclub.activity.MenuActivity;
import mobi.app4mob.quartzclub.activity.R;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.BonusPackage;
import mobi.app4mob.quartzclub.utils.BitmapManager;

/**
 * Created by Yosri Mekni <dev4.yosri@gmail.com> on 09/02/2018.
 */

public class CustomBonusGridAdapter extends BaseAdapter {


    private Context mContext;
    private final ArrayList<BonusPackage> bonus_package;
    private int type_profil;

    public CustomBonusGridAdapter(Context c, ArrayList<BonusPackage> bonus_package, int type_profil) {
        mContext = c;
        this.bonus_package = bonus_package;
        this.type_profil = type_profil;

        BitmapManager.INSTANCE.setPlaceholder(BitmapFactory.decodeResource(
                mContext.getResources(), R.drawable.img_detail_default));

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return bonus_package.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View grid;
        System.out.println(">>>>>>>>>>>>>>>CBGAdapter>>>>" + bonus_package.size());
        Produit produit = new Produit();
        produit.liste_produits = MenuActivity.liste_produits;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(mContext);


            grid = inflater.inflate(R.layout.grid_single, null);


            TextView product_name = (TextView) grid
                    .findViewById(R.id.product_name);


            product_name.setText(Functions.getProduitById(
                    bonus_package.get(position).getProduct().getId(), MenuActivity.liste_produits).getName());

            TextView product_qt = (TextView) grid
                    .findViewById(R.id.product_qt);
            product_qt.setText("X" + bonus_package.get(position).getQt());
            if (type_profil == ListBonusActivity.MAGASINBONUS) {
                ((LinearLayout) grid.findViewById(R.id.container)).setBackground(mContext.getResources().getDrawable(R.drawable.border_image_magasin));
                product_qt.setBackground(mContext.getResources().getDrawable(R.drawable.background_qt_promo_magasin));
            }

            TextView product_code = (TextView) grid
                    .findViewById(R.id.product_code);
            product_code.setText(produit.getProduitById(
                    bonus_package.get(position).getProduct().getId()).getCode());

            if (!Functions.isStringEmpty(produit.getProduitById(bonus_package.get(position).getProduct().getId()).getImage())) {
                int width = 80;
                int height = 100;
                String url = Constants.DOMAIN + Constants.UPLOADS_PRODUCT_FOLDER + "/" + produit.getProduitById(bonus_package.get(position).getProduct().getId()).getImage();
                ImageView img = (ImageView) grid.findViewById(R.id.product_image);
                BitmapManager.INSTANCE.loadBitmap(url, img, width, height);
            }

        } else {
            grid = convertView;
        }

        return grid;
    }


}
