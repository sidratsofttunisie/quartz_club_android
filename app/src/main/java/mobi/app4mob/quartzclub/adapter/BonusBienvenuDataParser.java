package mobi.app4mob.quartzclub.adapter;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.model.BonusAnniv;
import mobi.app4mob.quartzclub.utils.WebClient;

/**
 * Created by Yosri Mekni <dev4.yosri@gmail.com> on 08/02/2018.
 */

public class BonusBienvenuDataParser {


    public String[] getBonusBienvenu(String url) {
        String[] data = null;
        String response = null;

        ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();
        WebClient webClient = new WebClient(url, parameters);
        webClient.execute(0);
        response = webClient.getStringResponse();

        data = parseData(response);

        return data;
    }

    public static String[] parseData(String wsResponse) {
        String[] data = null;
        if (wsResponse != null && !wsResponse.equals(null) && wsResponse.length() > 0) {
            try {
                JSONObject obj = new JSONObject(wsResponse);
                data = new String[]{obj.getJSONObject("data").getString("commercial_points"),
                        obj.getJSONObject("data").getString("magasin_points")};
                Log.i("data ", data.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return data;
    }
}
