package mobi.app4mob.quartzclub.adapter;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.activity.R;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.PromoPackage;
import mobi.app4mob.quartzclub.utils.BitmapManager;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomGridAdapter extends BaseAdapter {
    private Context mContext;
    private final ArrayList<PromoPackage> promo_package;

    public CustomGridAdapter(Context c, ArrayList<PromoPackage> promo_package) {
        mContext = c;
        this.promo_package = promo_package;

        BitmapManager.INSTANCE.setPlaceholder(BitmapFactory.decodeResource(
                mContext.getResources(), R.drawable.img_detail_default));

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return promo_package.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View grid;
        Produit produit = new Produit();
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(mContext);
            grid = inflater.inflate(R.layout.grid_single, null);


            TextView product_name = (TextView) grid
                    .findViewById(R.id.product_name);

            product_name.setText(produit.getProduitById(
                    promo_package.get(position).getProduct_id()).getName());

            TextView product_qt = (TextView) grid
                    .findViewById(R.id.product_qt);
            product_qt.setText("X" + promo_package.get(position).getQt());

            TextView product_code = (TextView) grid
                    .findViewById(R.id.product_code);
            product_code.setText(produit.getProduitById(
                    promo_package.get(position).getProduct_id()).getCode());

            if (!Functions.isStringEmpty(produit.getProduitById(promo_package.get(position).getProduct_id()).getImage())) {
                int width = 80;
                int height = 100;
                String url = Constants.DOMAIN + Constants.UPLOADS_PRODUCT_FOLDER + "/" + produit.getProduitById(promo_package.get(position).getProduct_id()).getImage();
                ImageView img = (ImageView) grid.findViewById(R.id.product_image);
                BitmapManager.INSTANCE.loadBitmap(url, img, width, height);
            }

        } else {
            grid = convertView;
        }

        return grid;
    }

}