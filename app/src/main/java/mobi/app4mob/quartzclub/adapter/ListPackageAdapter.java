package mobi.app4mob.quartzclub.adapter;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.activity.R;
import mobi.app4mob.quartzclub.utils.BitmapManager;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ListPackageAdapter extends BaseAdapter {

    private Context context;
    private static LayoutInflater inflater = null;
    private ArrayList<String> codePackages;

    public ListPackageAdapter(Context _context, ArrayList<String> _codePackages) {
        context = _context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        codePackages = _codePackages;

        BitmapManager.INSTANCE.setPlaceholder(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.img_detail_default));

    }

    @Override
    public int getCount() {
        return this.codePackages.size();
    }

    @Override
    public Object getItem(int position) {
        return this.codePackages.get(position);
    }

    @Override
    public long getItemId(int position) {
        //return this.codePackages.get(position).toString();
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = inflater.inflate(R.layout.cell_list_package, parent, false);

        if (position < codePackages.size()) {


            ((TextView) v.findViewById(R.id.code)).setText("111");

			/*Shader myShader = new LinearGradient( 
			    0, 0, 0, 100, 
			    0xFFFFFFFF, Color.BLACK, 
			    Shader.TileMode.CLAMP );*/


        }

        return v;

    }

}
