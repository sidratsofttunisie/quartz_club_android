package mobi.app4mob.quartzclub.adapter;

import java.util.ArrayList;


import mobi.app4mob.quartzclub.activity.HistoriqueActivity;
import mobi.app4mob.quartzclub.activity.R;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.Gift;
import mobi.app4mob.quartzclub.model.Historique;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.utils.BitmapManager;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class HistoriqueAdapter extends BaseAdapter {

    private Context context;
    private static LayoutInflater inflater = null;
    private ArrayList<Historique> arrayHistoriques;
    private User user;
    private int type_history;

    public HistoriqueAdapter(Context _context,
                             ArrayList<Historique> _arrayHistoriques, User user, int type_history) {
        Log.i(" type history ", "type = " + type_history);

        context = _context;
        this.user = user;
        this.type_history = type_history;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        arrayHistoriques = _arrayHistoriques;

        BitmapManager.INSTANCE.setPlaceholder(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.img_detail_default));

    }

    @Override
    public int getCount() {
        return this.arrayHistoriques.size();
    }

    @Override
    public Object getItem(int position) {
        return this.arrayHistoriques.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.arrayHistoriques.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        if (type_history == HistoriqueActivity.MAGASINHISTORY) {
            v = inflater.inflate(R.layout.cell_list_history_magasin, parent,
                    false);
        } else {
            v = inflater.inflate(R.layout.cell_list_history, parent, false);
        }
        if (position < arrayHistoriques.size()) {

            ((TextView) v.findViewById(R.id.date)).setText(Functions
                    .getFormatedDateHours(arrayHistoriques.get(position)
                            .getCreated_at())
                    + "h");

            ((TextView) v.findViewById(R.id.bouteille))
                    .setText(arrayHistoriques.get(position).getNb_bouteille()
                            + "");

            if (type_history == HistoriqueActivity.COMMERCIALHISTORY) { // historique
                // commercial
                ((TextView) v.findViewById(R.id.point))
                        .setText(arrayHistoriques.get(position)
                                .getNb_points_commercial() + " points");
            }
            if (type_history == HistoriqueActivity.MAGASINHISTORY) { // historique
                // magasin
                ((TextView) v.findViewById(R.id.point))
                        .setText(arrayHistoriques.get(position)
                                .getNb_points_magasin() + " points");

            }

        }

        return v;

    }

}
