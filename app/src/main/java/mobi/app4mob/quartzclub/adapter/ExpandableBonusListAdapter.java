package mobi.app4mob.quartzclub.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.activity.HistoriqueActivity;
import mobi.app4mob.quartzclub.activity.ListBonusActivity;
import mobi.app4mob.quartzclub.activity.R;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.Bonus;
import mobi.app4mob.quartzclub.model.BonusPackage;
import mobi.app4mob.quartzclub.model.Commande;
import mobi.app4mob.quartzclub.model.ProduitC;

/**
 * Created by Yosri Mekni <dev4.yosri@gmail.com> on 09/02/2018.
 */

public class ExpandableBonusListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private ArrayList<Bonus> groupBonus; // header titles
    private int type_profil;

    public ExpandableBonusListAdapter(Context context,
                                      ArrayList<Bonus> groupBonus, int type_profil) {
        this._context = context;
        this.groupBonus = groupBonus;
        this.type_profil = type_profil;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.groupBonus.get(groupPosition).getListe_bonus_pack()
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        View v;
        LayoutInflater inflater = (LayoutInflater) _context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        v = inflater.inflate(R.layout.cell_list_child_bonus, parent,
                false);

        BonusPackage bonusPackage = (BonusPackage) groupBonus.get(groupPosition).getListe_bonus_pack().get(childPosition);

        ((TextView) v.findViewById(R.id.bonus_name)).setText(bonusPackage.getProduct().getName());

        ((TextView) v.findViewById(R.id.bonus_code)).setText(bonusPackage.getProduct().getCode());

        Picasso.with(_context)
                .load(Constants.DOMAIN + Constants.UPLOADS_PRODUCT_FOLDER + "/" + bonusPackage.getProduct().getImage())
                .placeholder(R.drawable.btn_ref_s2)
                .error(R.drawable.ic_sup)
                .into((ImageView) v.findViewById(R.id.bonus_image));

//        int nb_point = 0;
//
//        if (type_history == HistoriqueActivity.COMMERCIALHISTORY) { // historique commercial
//            nb_point = Integer.parseInt(historique.getNb_points_commercial());
//
//        }
//        if (type_history == HistoriqueActivity.MAGASINHISTORY) { // historique magasin
//
//            nb_point = Integer.parseInt(historique.getNb_points_magasin());
//
//        }
//        Log.i(">>>type_hisotry", ">>> type_history = " + type_history);
//        ((TextView) v.findViewById(R.id.point)).setText(nb_point + " points");

        return v;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.groupBonus.get(groupPosition).getListe_bonus_pack().size();
    }

    @Override
    public Bonus getGroup(int groupPosition) {
        return this.groupBonus.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.groupBonus.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        // TODO
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(
                    R.layout.cell_list_bonus, null);
        }

//        int sum_pt = 0;
//        int sum_bt = 0;
//        for (ProduitC bonusPackage : groupBonus.get(groupPosition)
//                .getListe_bonus_pack()) {
//
//            sum_bt += Integer.parseInt(historique.getNb_bouteille());
//
//            if (type_history == HistoriqueActivity.COMMERCIALHISTORY) {
//                sum_pt += Integer
//                        .parseInt(historique.getNb_points_commercial());
//                ((ImageView) convertView.findViewById(R.id.ic_bouteille)).setImageResource(R.drawable.gaz_bleu_petit);
//
//            } else {
//                sum_pt += Integer
//                        .parseInt(historique.getNb_points_magasin());
//                ((ImageView) convertView.findViewById(R.id.ic_bouteille)).setImageResource(R.drawable.gaz_rouge_petit);
//
//            }
//        }
        ((TextView) convertView.findViewById(R.id.name))
                .setText(groupBonus.get(groupPosition).getName());

        if (!groupBonus.get(groupPosition).isIs_unlimited()) {
            ((ImageView) convertView.findViewById(R.id.img_infini)).setVisibility(View.GONE);
            ((TextView) convertView.findViewById(R.id.perValue)).setVisibility(View.VISIBLE);
            ((TextView) convertView.findViewById(R.id.perValue)).setText(Functions.getFormatedDate(groupBonus.get(groupPosition).getStart_at()) + " --> " + Functions.getFormatedDate(groupBonus.get(groupPosition).getEnd_at()));
        } else {
            ((ImageView) convertView.findViewById(R.id.img_infini)).setVisibility(View.VISIBLE);
            ((TextView) convertView.findViewById(R.id.perValue)).setVisibility(View.GONE);
            if (type_profil == ListBonusActivity.COMMERCIALBONUS)
                ((ImageView) convertView.findViewById(R.id.img_infini)).setImageDrawable(_context.getResources().getDrawable(R.drawable.infini_b));
            else
                ((ImageView) convertView.findViewById(R.id.img_infini)).setImageDrawable(_context.getResources().getDrawable(R.drawable.infini_r));
        }


        if (type_profil == ListBonusActivity.COMMERCIALBONUS) {
            ((TextView) convertView.findViewById(R.id.points)).setText(groupBonus.get(groupPosition).getCommercial_points());
        } else {
            ((TextView) convertView.findViewById(R.id.points)).setText(groupBonus.get(groupPosition).getMagasin_points());
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


}
