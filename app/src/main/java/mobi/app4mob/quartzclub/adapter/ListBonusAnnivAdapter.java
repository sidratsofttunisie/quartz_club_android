package mobi.app4mob.quartzclub.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.activity.ListBonusActivity;
import mobi.app4mob.quartzclub.activity.MenuActivity;
import mobi.app4mob.quartzclub.activity.R;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.BonusAnniv;
import mobi.app4mob.quartzclub.model.Gift;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.utils.BitmapManager;

/**
 * Created by Yosri Mekni <dev4.yosri@gmail.com> on 12/02/2018.
 */

public class ListBonusAnnivAdapter extends BaseAdapter {

    private Context context;
    private static LayoutInflater inflater = null;
    private int type_profil;
    private ArrayList<BonusAnniv> arrayBonus;

    public ListBonusAnnivAdapter(Context _context, ArrayList<BonusAnniv> _arrayBonus,
                                 int type_profil) {
        this.context = _context;
        this.type_profil = type_profil;
        this.arrayBonus = _arrayBonus;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        BitmapManager.INSTANCE.setPlaceholder(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.img_detail_default));

    }

    @Override
    public int getCount() {
        return this.arrayBonus.size();
    }

    @Override
    public Object getItem(int position) {
        return this.arrayBonus.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(this.arrayBonus.get(position).getId());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = inflater.inflate(R.layout.cell_list_bonus_anniv, parent, false);

        if (position < arrayBonus.size()) {

            //points
            if (type_profil == 1)
                ((TextView) v.findViewById(R.id.level)).setText(arrayBonus.get(
                        position).getCommercial_points() + " ");
            else {
                ((TextView) v.findViewById(R.id.level)).setText(arrayBonus.get(position).getMagasin_points() + " ");
                ((TextView) v.findViewById(R.id.annee)).setTextColor(context.getResources().getColor(R.color.color_rose));
            }

            //annee
                ((TextView) v.findViewById(R.id.annee)).setText(arrayBonus.get(
                        position).getYear() + " " + context.getResources().getString(R.string.annees));


        }

        return v;

    }
}

