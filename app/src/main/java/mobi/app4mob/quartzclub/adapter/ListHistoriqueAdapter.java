package mobi.app4mob.quartzclub.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.activity.R;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.Gift;
import mobi.app4mob.quartzclub.model.Historique;
import mobi.app4mob.quartzclub.model.History;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.utils.BitmapManager;

/**
 * Created by Yosri Mekni on 05/02/2018.
 */

public class ListHistoriqueAdapter extends BaseAdapter {

    private Context context;
    private static LayoutInflater inflater = null;
    private ArrayList<History> arrayHistories;
    private User user;

    public ListHistoriqueAdapter(Context _context, ArrayList<History> _arrayHistories,
                                 User user) {
        context = _context;
        this.user = user;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        arrayHistories = _arrayHistories;

        BitmapManager.INSTANCE.setPlaceholder(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.img_detail_default));

    }

    @Override
    public int getCount() {
        return this.arrayHistories.size();
    }

    @Override
    public Object getItem(int position) {
        return this.arrayHistories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(this.arrayHistories.get(position).getId());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = inflater.inflate(R.layout.cell_list_history, parent, false);


        ((TextView) v.findViewById(R.id.date)).setText(Functions.getFormatedDateHours(arrayHistories.get(
                position).getDate()) + "h");

        ((TextView) v.findViewById(R.id.point)).setText(arrayHistories.get(
                position).getPoints() + " " + context.getResources().getString(R.string.points));


        return v;

    }

}
