package mobi.app4mob.quartzclub.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import mobi.app4mob.quartzclub.activity.HistoriqueActivity;
import mobi.app4mob.quartzclub.activity.R;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.Commande;
import mobi.app4mob.quartzclub.model.History;
import mobi.app4mob.quartzclub.model.Historique;
import mobi.app4mob.quartzclub.model.ProduitC;

import android.content.Context;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private ArrayList<Commande> groupHistoriques; // header titles
    private int type_history;

    public ExpandableListAdapter(Context context,
                                 ArrayList<Commande> groupHistoriques, int type_history) {
        this._context = context;
        this.groupHistoriques = groupHistoriques;
        this.type_history = type_history;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.groupHistoriques.get(groupPosition).getListProduct()
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        View v;
        LayoutInflater inflater = (LayoutInflater) _context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (type_history == HistoriqueActivity.MAGASINHISTORY) {
            v = inflater.inflate(R.layout.cell_list_history_magasin, parent,
                    false);
        } else {
            v = inflater.inflate(R.layout.cell_list_history, parent, false);
        }

        ProduitC produitC = (ProduitC) groupHistoriques.get(groupPosition).getListProduct().get(childPosition);

        ((TextView) v.findViewById(R.id.name)).setText(produitC.getProduit().getName());

        ((TextView) v.findViewById(R.id.qte)).setText(produitC.getQte()+" "+_context.getResources().getString(R.string.produits));

        Picasso.with(_context)
                .load(Constants.DOMAIN + groupHistoriques.get(groupPosition).getImage_path() + produitC.getProduit().getImage())
                .placeholder(R.drawable.goute_liste)
                .error(R.drawable.goute_liste)
                .into((ImageView) v.findViewById(R.id.image));

//        int nb_point = 0;
//
//        if (type_history == HistoriqueActivity.COMMERCIALHISTORY) { // historique commercial
//            nb_point = Integer.parseInt(historique.getNb_points_commercial());
//
//        }
//        if (type_history == HistoriqueActivity.MAGASINHISTORY) { // historique magasin
//
//            nb_point = Integer.parseInt(historique.getNb_points_magasin());
//
//        }
//        Log.i(">>>type_hisotry", ">>> type_history = " + type_history);
//        ((TextView) v.findViewById(R.id.point)).setText(nb_point + " points");

        return v;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.groupHistoriques.get(groupPosition).getListProduct().size();
    }

    @Override
    public Commande getGroup(int groupPosition) {
        return this.groupHistoriques.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.groupHistoriques.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        // TODO
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(
                    R.layout.cell_list_history_group, null);
        }

//        int sum_pt = 0;
//        int sum_bt = 0;
//        for (ProduitC produitC : groupHistoriques.get(groupPosition)
//                .getListProduct()) {
//
//            sum_bt += Integer.parseInt(historique.getNb_bouteille());
//
//            if (type_history == HistoriqueActivity.COMMERCIALHISTORY) {
//                sum_pt += Integer
//                        .parseInt(historique.getNb_points_commercial());
//                ((ImageView) convertView.findViewById(R.id.ic_bouteille)).setImageResource(R.drawable.gaz_bleu_petit);
//
//            } else {
//                sum_pt += Integer
//                        .parseInt(historique.getNb_points_magasin());
//                ((ImageView) convertView.findViewById(R.id.ic_bouteille)).setImageResource(R.drawable.gaz_rouge_petit);
//
//            }
//        }

        ((TextView) convertView.findViewById(R.id.date))
                .setText(Functions.getFormatedDateHours(groupHistoriques.get(groupPosition).getCreated_at()));

        ((TextView) convertView.findViewById(R.id.bouteille)).setText(groupHistoriques.get(groupPosition).getQte_total());
        if (type_history == HistoriqueActivity.COMMERCIALHISTORY)
            ((TextView) convertView.findViewById(R.id.point)).setText(groupHistoriques.get(groupPosition).getCommercial_points());
        else {
            ((RelativeLayout)convertView.findViewById(R.id.container)).setBackgroundColor(_context.getResources().getColor(R.color.color_rose));
            ((TextView) convertView.findViewById(R.id.point)).setText(groupHistoriques.get(groupPosition).getMagasin_points());
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}