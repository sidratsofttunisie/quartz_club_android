package mobi.app4mob.quartzclub.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.activity.R;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.Message;
import mobi.app4mob.quartzclub.model.Slide;
import mobi.app4mob.quartzclub.utils.BitmapManager;

public class CategsListAdapter extends BaseAdapter {

    private Context context;
    private static LayoutInflater inflater = null;
    private ArrayList<Slide> arrayCategs;


    public CategsListAdapter(Context _context,
                          ArrayList<Slide> _arrayCategs) {
        context = _context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        arrayCategs = _arrayCategs;

        BitmapManager.INSTANCE.setPlaceholder(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.img_detail_default));

    }

    @Override
    public int getCount() {
        return this.arrayCategs.size();
    }

    @Override
    public Object getItem(int position) {
        return this.arrayCategs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = inflater.inflate(R.layout.cell_list_catalogue, parent, false);

        if (position < arrayCategs.size()) {

            ((TextView) v.findViewById(R.id.categ_name)).setText(((Slide)getItem(position)).getTitre());


            if(((Slide)getItem(position)).getPhotos().size()>0){
                Picasso.with(context)
                        .load(Constants.DOMAIN + ((Slide)getItem(position)).getPhotos().get(0).getImage())
                        .placeholder(R.drawable.btn_ref_s2)
                        .error(R.drawable.ic_sup)
                        .into((ImageView) v.findViewById(R.id.categ_image));
            }

        }

        return v;

    }

}
