package mobi.app4mob.quartzclub.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.activity.ListBonusActivity;
import mobi.app4mob.quartzclub.activity.R;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.Bonus;
import mobi.app4mob.quartzclub.utils.BitmapManager;
import mobi.app4mob.quartzclub.view.ExpandableHeightGridView;

/**
 * Created by Yosri Mekni <dev4.yosri@gmail.com> on 08/02/2018.
 */

public class BonusPagerAdapter extends PagerAdapter {


    private ArrayList<Bonus> arrayBonuss;
    private Context context;
    private int type_profil;
    private LayoutInflater inflater;
    ExpandableHeightGridView grid;

    public BonusPagerAdapter(ArrayList<Bonus> arrayBonuss, Context context, int type_profil) {
        super();
        this.arrayBonuss = arrayBonuss;
        this.context = context;
        this.type_profil = type_profil;

        BitmapManager.INSTANCE.setPlaceholder(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.img_detail_default));
    }

    @Override
    public int getCount() {
        return arrayBonuss.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        TextView promoName;
        TextView promoPoints;
        ImageView periode_img;
        TextView periode_txt;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.viewpager_item_promo,
                container, false);

        final int _position = position;
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

		/*
         * txtName = (TextView) itemView.findViewById(R.id.txt_name);
		 * txtPrixBonus = (TextView) itemView.findViewById(R.id.txt_prix_promo);
		 * txtPrix = (TextView) itemView.findViewById(R.id.txt_prix);
		 *
		 * txtName.setText(arrayBonuss.get(position).getName());
		 * txtPrixBonus.setText
		 * (arrayBonuss.get(position).getCommercial_points()+" DT");
		 *
		 * txtPrix.setPaintFlags(txtPrix.getPaintFlags() |
		 * Paint.STRIKE_THRU_TEXT_FLAG);
		 * txtPrix.setText(arrayBonuss.get(position).getMagasin_points()+" DT");
		 */

        promoName = (TextView) itemView.findViewById(R.id.promo_name);
        LinearLayout containerLayout = (LinearLayout) itemView.findViewById(R.id.container);
        promoPoints = (TextView) itemView.findViewById(R.id.promo_points);
        periode_img = (ImageView) itemView.findViewById(R.id.img_infini);
        periode_txt = (TextView) itemView.findViewById(R.id.perValue);


        promoName.getPaint().setShader(
                new LinearGradient(200, 100, 10, promoName
                        .getHeight(), 0xFFEFDC6F, 0xFFB98827,
                        Shader.TileMode.MIRROR));


        promoName.setText(arrayBonuss.get(position).getName());
        if (type_profil == ListBonusActivity.COMMERCIALBONUS) {
            promoPoints.setText(arrayBonuss.get(position).getCommercial_points());
            containerLayout.setBackground(context.getResources().getDrawable(R.drawable.bg_produit_b));
        } else {
            promoPoints.setText(arrayBonuss.get(position).getMagasin_points());
            containerLayout.setBackground(context.getResources().getDrawable(R.drawable.bg_produit_r));
        }
        if (arrayBonuss.get(position).isIs_unlimited()) {
            periode_txt.setVisibility(View.GONE);
            periode_img.setVisibility(View.VISIBLE);
            if (type_profil == ListBonusActivity.COMMERCIALBONUS)
                periode_img.setImageDrawable(context.getResources().getDrawable(R.drawable.infini_b));
            else
                periode_img.setImageDrawable(context.getResources().getDrawable(R.drawable.infini_r));
        } else {
            periode_img.setVisibility(View.GONE);
            periode_txt.setVisibility(View.VISIBLE);
            periode_txt.setText(Functions.getFormatedDate(arrayBonuss.get(position).getStart_at()) + " --> " + Functions.getFormatedDate(arrayBonuss.get(position).getEnd_at()));
        }
        CustomBonusGridAdapter adapter = new CustomBonusGridAdapter(context, arrayBonuss
                .get(position).getListe_bonus_pack(), type_profil);

        grid = (ExpandableHeightGridView) itemView.findViewById(R.id.grid);
        grid.setExpanded(true);
        grid.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        ((ViewPager) container).addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((LinearLayout) object);

    }


}
