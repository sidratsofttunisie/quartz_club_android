package mobi.app4mob.quartzclub.adapter;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.activity.R;
import mobi.app4mob.quartzclub.model.Promo;
import mobi.app4mob.quartzclub.utils.BitmapManager;
import mobi.app4mob.quartzclub.view.ExpandableHeightGridView;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PromosPagerAdapter extends PagerAdapter {

    private ArrayList<Promo> arrayPromos;
    private Context context;
    private LayoutInflater inflater;
    ExpandableHeightGridView grid;

    public PromosPagerAdapter(ArrayList<Promo> arrayPromos, Context context) {
        super();
        this.arrayPromos = arrayPromos;
        this.context = context;

        BitmapManager.INSTANCE.setPlaceholder(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.img_detail_default));
    }

    @Override
    public int getCount() {
        return arrayPromos.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        TextView promoName;
        TextView promoPoints;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.viewpager_item_promo,
                container, false);

        final int _position = position;
        itemView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

		/*
         * txtName = (TextView) itemView.findViewById(R.id.txt_name);
		 * txtPrixPromo = (TextView) itemView.findViewById(R.id.txt_prix_promo);
		 * txtPrix = (TextView) itemView.findViewById(R.id.txt_prix);
		 * 
		 * txtName.setText(arrayPromos.get(position).getName());
		 * txtPrixPromo.setText
		 * (arrayPromos.get(position).getCommercial_points()+" DT");
		 * 
		 * txtPrix.setPaintFlags(txtPrix.getPaintFlags() |
		 * Paint.STRIKE_THRU_TEXT_FLAG);
		 * txtPrix.setText(arrayPromos.get(position).getMagasin_points()+" DT");
		 */


        promoName = (TextView) itemView.findViewById(R.id.promo_name);
        promoPoints = (TextView) itemView.findViewById(R.id.promo_points);


        promoName.getPaint().setShader(
                new LinearGradient(200, 100, 10, promoName
                        .getHeight(), 0xFFEFDC6F, 0xFFB98827,
                        Shader.TileMode.MIRROR));


        promoName.setText(arrayPromos.get(position).getName());
        promoPoints.setText(arrayPromos.get(position).getCommercial_points());

        CustomGridAdapter adapter = new CustomGridAdapter(context, arrayPromos
                .get(position).getListe_promo());

        grid = (ExpandableHeightGridView) itemView.findViewById(R.id.grid);
        grid.setExpanded(true);
        grid.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        ((ViewPager) container).addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((LinearLayout) object);

    }
}
