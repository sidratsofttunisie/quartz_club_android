package mobi.app4mob.quartzclub.activity;

import java.util.ArrayList;
import java.util.HashSet;

import net.sourceforge.zbar.Symbol;

import com.dm.zbar.android.scanner.ZBarConstants;
import com.dm.zbar.android.scanner.ZBarScannerActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.MagasinDataParser;
import mobi.app4mob.quartzclub.utils.AlertMessage;
import mobi.app4mob.quartzclub.utils.EmailValidator;
import mobi.app4mob.quartzclub.utils.GPSTracker;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;
import mobi.app4mob.quartzclub.utils.SessionManager;
import mobi.app4mob.quartzclub.zxing.ScalingScannerActivity;
import nl.changer.polypicker.Config;
import nl.changer.polypicker.ImagePickerActivity;
import nl.changer.polypicker.model.Image;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class AddMagasinActivity extends BaseActivity implements
        OnMapLongClickListener, OnMapClickListener, OnMarkerDragListener {
    public static User user;
    public static boolean is_added;
    Activity activity;
    private GoogleMap mMap;
    public static FragmentManager fragmentManager;
    public static int new_sold = 0;

    private Location myLocation;
    private double latitude = 0;
    private double longitude = 0;

    private EditText raison_social;
    private EditText register;
    private EditText nom;
    private EditText prenom;
    private EditText address;
    private EditText email;
    private EditText tel;
    // private EditText fax;
    private EditText cin;
    private EditText tag_client;
    private EditText tag_mag;
    private TextView action_bar_title;

    public static final String SCAN_RESULT = "result";
    private static final int ZBAR_SCANNER_REQUEST = 0;
    private static final int ZBAR_QR_SCANNER_REQUEST = 1;
    private Boolean scan_tag_client = true;

    private String msgErreur = "";
    private ArrayList<Produit> liste_produits;
    SessionManager session;
    GPSTracker gps;
    Uri[] uriss;

    HashSet<Uri> mMedia = new HashSet<Uri>();
    HashSet<Image> mMediaImages = new HashSet<Image>();


    private static final int INTENT_REQUEST_GET_IMAGES = 13;
    public static final int INTENT_REQUEST_GET_N_IMAGES = 14;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        super.mTopToolbar.setBackgroundColor(getResources().getColor(R.color.color_rose));
        Bundle bn = new Bundle();
        bn = getIntent().getExtras();
        user = (User) bn.getSerializable("user");
        liste_produits = (ArrayList<Produit>) bn.getSerializable("produit");

        activity = this;
        gps = new GPSTracker(activity);
        gps.checkGps();
        initLatLong();
        initViews();

        fragmentManager = getSupportFragmentManager();
        setUpMap();


    }

    @Override
    protected int getLayoutResourceId(){
        return R.layout.add_magasin;
    }


    private void setUpMap() {

        // Do a null check to confirm that we have not already instantiated the
        // map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) AddMagasinActivity.fragmentManager
                    .findFragmentById(R.id.map_add)).getMap();
            Boolean f  = true;
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                f = false;
                return;
            }
            if (f)
                mMap.setMyLocationEnabled(true);
            // mMap = ((NiceSupportMapFragment)
            // getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            // ((NiceSupportMapFragment)
            // getSupportFragmentManager().findFragmentById(R.id.map))
            // .setPreventParentScrolling(true);
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                Log.d("total2", "long=" + longitude);
                Log.d("total2", "lat=" + latitude);
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latitude, longitude))
                        .draggable(true)
                        .title("")
                        .icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.ic_map_pin_toaster)));

                mMap.getUiSettings().setScrollGesturesEnabled(true);
                mMap.getUiSettings().setZoomControlsEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                        latitude, longitude), 14));

                mMap.setOnMapClickListener(this);
                mMap.setOnMapLongClickListener(this);
                mMap.setOnMarkerDragListener(this);

            }
        }
    }

    private void UpdateMap() {

        // Do a null check to confirm that we have not already instantiated the
        // map.

        // Try to obtain the map from the SupportMapFragment.
        mMap = ((SupportMapFragment) AddMagasinActivity.fragmentManager
                .findFragmentById(R.id.map_add)).getMap();
        // mMap = ((NiceSupportMapFragment)
        // getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        // ((NiceSupportMapFragment)
        // getSupportFragmentManager().findFragmentById(R.id.map))
        // .setPreventParentScrolling(true);
        // Check if we were successful in obtaining the map.
        if (mMap != null) {
            mMap.clear();
            Log.d("total2", "long=" + longitude);
            Log.d("total2", "lat=" + latitude);
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latitude, longitude))
                    .draggable(true)
                    .title("")
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.ic_map_pin_toaster)));

            mMap.getUiSettings().setScrollGesturesEnabled(true);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                    latitude, longitude), 14));

            mMap.setOnMapClickListener(this);
            mMap.setOnMapLongClickListener(this);
            mMap.setOnMarkerDragListener(this);

        }

    }

    private void initViews() {

        Button btn_ref = (Button) findViewById(R.id.ab_refersh_pos);
        btn_ref.setVisibility(View.VISIBLE);
        action_bar_title = (TextView) findViewById(R.id.txt_action_bar_title);
        action_bar_title.setText(R.string.add_magasin_title);
        raison_social = (EditText) findViewById(R.id.add_raison_social);
        register = (EditText) findViewById(R.id.add_register_commerce);
        nom = (EditText) findViewById(R.id.add_nom);
        prenom = (EditText) findViewById(R.id.add_prenom);
        address = (EditText) findViewById(R.id.add_address);
        email = (EditText) findViewById(R.id.add_email);
        tel = (EditText) findViewById(R.id.add_tel);
        // fax = (EditText) findViewById(R.id.add_fax);
        cin = (EditText) findViewById(R.id.add_cin);
        tag_client = (EditText) findViewById(R.id.add_tag_client);
        tag_mag = (EditText) findViewById(R.id.add_tag_magasin);

        // allow the user to scroll the map vertically into scroller view
        /*final ScrollView mainScrollView = (ScrollView) findViewById(R.id.main_scrollview);
        ImageView transparentImageView = (ImageView) findViewById(R.id.transparent_image);
        transparentImageView.setOnTouchListener(new View.OnTouchListener() {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });*/
    }

    public void refreshPos(View v) {
        ProgressTraitment prog = new ProgressTraitment(this) {
            @Override
            public void onLoadStarted() {
                gps = new GPSTracker(activity);
                gps.checkGps();
                initLatLong();

            }

            @Override
            public void onLoadFinished() {
                UpdateMap();
            }
        };
        prog.DoTraitemnt("", "");

    }

    private void initLatLong() {
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        } else {
            longitude = 10.180744;
            latitude = 36.799152;
            Functions.LOGE("ERROR",
                    getString(R.string.pas_pu_localiser_emplacement));
            new MyToast(getApplicationContext(),
                    getString(R.string.pas_pu_localiser_emplacement),
                    Toast.LENGTH_SHORT);
        }
        Log.d("total2", "****long=" + longitude);
        Log.d("total2", "****lat=" + latitude);
    }

    public void addMagasinAction(View v) {

        if (verifyData()) {
            ProgressTraitment prog = new ProgressTraitment(this) {
                int flag = 0;

                @Override
                public void onLoadStarted() {
                    Long tsLong = System.currentTimeMillis();

                    String rais_soc = raison_social.getText().toString().trim();
                    String register_com = register.getText().toString().trim();
                    String nom_ = nom.getText().toString().trim();
                    String prenom_ = prenom.getText().toString().trim();
                    String address_ = address.getText().toString().trim();
                    String email_ = email.getText().toString().trim();
                    String tel_ = tel.getText().toString().trim();
                    // String fax_ = fax.getText().toString().trim();
                    String cin_ = cin.getText().toString().trim();
                    //String tag_client_ = tag_client.getText().toString().trim();
                    String tag_client_ = "";
                    String tag_mag_ = tag_mag.getText().toString().trim();
                    flag = MagasinDataParser.addMagasin(user.getId(), rais_soc,
                            register_com, nom_, prenom_, address_, email_,
                            tel_, cin_, tag_client_, tag_mag_, longitude,
                            latitude,mMediaImages);

                }

                @Override
                public void onLoadFinished() {
                    if (flag == 1) {
                        // new MyToast(getApplicationContext(), "success",
                        // MyToast.LENGTH_SHORT);
                        /*
                         * SharedPreferences.Editor ed = prefs.edit();
						 * ed.putString(User.EMAIL, user.getEmail());
						 * ed.commit();
						 */

                        AlertMessage dialogAuthentificationFailed = new AlertMessage(
                                activity) {
                            @Override
                            public void onOk() {
                                if (new_sold > 0)
                                    user.setPoint(new_sold);

                                // Update Session User(with new solde point)
                                session = new SessionManager(
                                        getApplicationContext());
                                session.updateSession(user);

                                Intent intent = new Intent(activity,
                                        ListMagasinActivity.class);
                                Bundle b = new Bundle();
                                b.putSerializable("user", user);
                                b.putSerializable("produit", liste_produits);
                                intent.putExtras(b);
                                activity.startActivity(intent);
                                finish();

                            }

                            @Override
                            public void onCancel() {
                                Functions.LOGI("*****", "on cancle");
                            }
                        };
                        dialogAuthentificationFailed.show1(
                                getString(R.string.alert_add_magasin_message),
                                getString(R.string.alert_add_magasin_title),
                                false, getString(R.string.valider));

                    }
                    // cin existe
                    if (flag == -1) {
                        AlertMessage dialogAuthentificationFailed = new AlertMessage(
                                activity) {
                            @Override
                            public void onOk() {
                                // TODO: convert cin to red border

                            }

                            @Override
                            public void onCancel() {
                                Functions.LOGI("*****", "on cancle");
                            }
                        };
                        dialogAuthentificationFailed.show1(
                                getString(R.string.error_cin_existe),
                                getString(R.string.erreur), false, "OK");
                    }
                    // ref client existe
                    if (flag == -2) {
                        AlertMessage dialogAuthentificationFailed = new AlertMessage(
                                activity) {
                            @Override
                            public void onOk() {
                                // TODO: convert cin to red border

                            }

                            @Override
                            public void onCancel() {
                                Functions.LOGI("*****", "on cancle");
                            }
                        };
                        dialogAuthentificationFailed.show1(
                                getString(R.string.error_client_existe),
                                getString(R.string.erreur), false, "OK");
                    }
                    // ref magasin existe
                    if (flag == -3) {
                        AlertMessage dialogAuthentificationFailed = new AlertMessage(
                                activity) {
                            @Override
                            public void onOk() {
                                // TODO: convert cin to red border

                            }

                            @Override
                            public void onCancel() {
                                Functions.LOGI("*****", "on cancle");
                            }
                        };
                        dialogAuthentificationFailed.show1(
                                getString(R.string.error_magasin_existe),
                                getString(R.string.erreur), false, "OK");
                    }
                    // ref registre du commerce existe
                    if (flag == -4) {
                        AlertMessage dialogAuthentificationFailed = new AlertMessage(
                                activity) {
                            @Override
                            public void onOk() {
                                // TODO: convert cin to red border

                            }

                            @Override
                            public void onCancel() {
                                Functions.LOGI("*****", "on cancle");
                            }
                        };
                        dialogAuthentificationFailed
                                .show1(getString(R.string.error_registre_du_commerce_existe),
                                        getString(R.string.erreur), false, "OK");
                    }
                    // nom magasin existe
                    if (flag == -5) {
                        AlertMessage dialogAuthentificationFailed = new AlertMessage(
                                activity) {
                            @Override
                            public void onOk() {
                                // TODO: convert cin to red border

                            }

                            @Override
                            public void onCancel() {
                                Functions.LOGI("*****", "on cancle");
                            }
                        };
                        dialogAuthentificationFailed
                                .show1(getString(R.string.error_nom_pv_existe),
                                        getString(R.string.erreur), false, "OK");
                    }
                    // Autre
                    if (flag == 0) {
                        new MyToast(getApplicationContext(),
                                R.string.error_add_magasin, Toast.LENGTH_SHORT);

                        Log.v("main activity",
                                getString(R.string.error_add_magasin));
                    }

                }
            };
            prog.DoTraitemnt("", "");
        } else {
            // Toast.makeText(this, errorMsg, Toast.LENGTH_SHORT).show();
            // showMessage(Constants.FAILED_DATA);

            AlertMessage dialogAuthentificationFailed = new AlertMessage(this) {
                @Override
                public void onOk() {

                }

                @Override
                public void onCancel() {

                }
            };
            dialogAuthentificationFailed.show1(msgErreur,
                    getString(R.string.alert_invalid_magasin_title), false,
                    getString(R.string.valider));
        }

    }

    public void getNImages(View v) {
        Intent intent = new Intent(getApplicationContext(), ImagePickerActivity.class);
        Config config = new Config.Builder()
                .setTabBackgroundColor(R.color.white)    // set tab background color. Default white.
                .setTabSelectionIndicatorColor(R.color.blue)
                .setCameraButtonColor(R.color.orange)
                // set photo selection limit. Default unlimited selection.
                .build();
        ImagePickerActivity.setConfig(config);
        startActivityForResult(intent, INTENT_REQUEST_GET_N_IMAGES);
    }

    private boolean verifyData() {
        boolean verify = true;
        msgErreur = "";

        //String tel_patern = "^((\\+|00)216)?([24579][0-9]{7})$";
        String fax_patern = "^((\\+|00)216)?(7[0-9]{7})$";
        //String cin_patern = "^[0-9]{8}$";
        String nom_patern = "^[a-zA-ZÀ-ÿ\\s]{3,}$";
        String prenom_patern = "^[a-zA-ZÀ-ÿ\\s]{3,}$";
        String email_patern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        String rais_soc = raison_social.getText().toString().trim();
        String register_com = register.getText().toString().trim();
        String nom_ = nom.getText().toString().trim();
        String prenom_ = prenom.getText().toString().trim();
        String address_ = address.getText().toString().trim();
        String email_ = email.getText().toString().trim();
        String tel_ = tel.getText().toString().trim();
        // String fax_ = fax.getText().toString().trim();
        String cin_ = cin.getText().toString().trim();
        String tag_client_ = tag_client.getText().toString().trim();
        String tag_mag_ = tag_mag.getText().toString().trim();
        EmailValidator emailValidator = new EmailValidator();
        if (rais_soc.length() < 3) {
            msgErreur += getString(R.string.error_raison_social_invalide);
            verify = false;
            raison_social.setBackgroundResource(R.drawable.invalid_champ);
        } else {
            raison_social.setBackgroundResource(R.drawable.bg_edit_text);
        }

        // Registre de commerce OPT
		/*
		 * if (register_com.length() < 4) { msgErreur +=
		 * getString(R.string.error_registre_du_commerce_invalide); verify =
		 * false; register.setBackgroundResource(R.drawable.invalid_champ); }
		 * else { register.setBackgroundResource(R.drawable.bg_edit_text); }
		 */

        if (!nom_.matches(nom_patern)) {
            msgErreur += getString(R.string.error_nom_invalide);
            verify = false;
            nom.setBackgroundResource(R.drawable.invalid_champ);
        } else {
            nom.setBackgroundResource(R.drawable.bg_edit_text);
        }

        if (!prenom_.matches(prenom_patern)) {
            msgErreur += getString(R.string.error_prenom_invalide);
            verify = false;
            prenom.setBackgroundResource(R.drawable.invalid_champ);
        } else {
            prenom.setBackgroundResource(R.drawable.bg_edit_text);
        }

        if (address_.length() < 5) {
            msgErreur += getString(R.string.error_adresse_invalide);
            verify = false;
            address.setBackgroundResource(R.drawable.invalid_champ);
        } else {
            address.setBackgroundResource(R.drawable.bg_edit_text);
        }

        if (!email_.equals("") && !emailValidator.validate(email_)) {
            msgErreur += getString(R.string.error_email_invalide);
            verify = false;
            email.setBackgroundResource(R.drawable.invalid_champ);
        } else {
            email.setBackgroundResource(R.drawable.bg_edit_text);
        }
        if (tel.length() < 8) {
            msgErreur += getString(R.string.error_tel_invalide);
            verify = false;
        }else{
            tel.setBackgroundResource(R.drawable.bg_edit_text);
        }
       /*if (!tel_.matches(tel_patern)) {
            msgErreur += getString(R.string.error_tel_invalide);
            verify = false;
            tel.setBackgroundResource(R.drawable.invalid_champ);
        } else {
            tel.setBackgroundResource(R.drawable.bg_edit_text);
        }/*

        // if (fax_.length() > 0) {
        // if (!fax_.matches(fax_patern)) {
        // msgErreur += getString(R.string.error_fax_invalide);
        // verify = false;
        // fax.setBackgroundResource(R.drawable.invalid_champ);
        // } else {
        // fax.setBackgroundResource(R.drawable.bg_edit_text);
        // }
        // }


        if (!cin_.matches(cin_patern)) {
            msgErreur += getString(R.string.error_cin_invalide);
            verify = false;
            cin.setBackgroundResource(R.drawable.invalid_champ);
        } else {
            cin.setBackgroundResource(R.drawable.bg_edit_text);
        }


		/*if (tag_client_.length() < 4) {
			msgErreur += getString(R.string.error_client_invalide);
			verify = false;
			tag_client.setBackgroundResource(R.drawable.invalid_champ);
		} else {
			tag_client.setBackgroundResource(R.drawable.bg_edit_text);
		}*/


        if (tag_mag_.length() < 4) {
            msgErreur += getString(R.string.error_magasin_invalide);
            verify = false;
            tag_mag.setBackgroundResource(R.drawable.invalid_champ);
        } else {
            tag_mag.setBackgroundResource(R.drawable.bg_edit_text);
        }

        return verify;
    }

    public void scanCodeClient(View v) {
        scan_tag_client = true;
        goScan();
        //launchPic2ShopScanner();
    }

    public void scanCodeMagasin(View v) {
        scan_tag_client = false;
        goScan();
    }

    public void launchPic2ShopScanner() {
        //Intent intent = new Intent(Constants.PIC2SHOP_ACTION);
        //startActivityForResult(intent, Constants.PIC2SHOP_REQUEST_CODE_SCAN);

        Intent intent = new Intent(Constants.Pro.ACTION);
        intent.putExtra(Constants.Pro.FORMATS, "EAN13");
        startActivityForResult(intent, Constants.PIC2SHOP_REQUEST_CODE_SCAN);
    }

    private void goScan() {
        if (isCameraAvailable()) {
            Intent intent = new Intent(AddMagasinActivity.this, ScalingScannerActivity.class);
            intent.putExtra("type_profil", 2);
            startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
        } else {

            Toast.makeText(this, getString(R.string.camera_indisponible),
                    Toast.LENGTH_SHORT).show();

        }
//		Intent i = new Intent(AddMagasinActivity.this, ScalingScannerActivity.class);
    }

    /*
     *
     * QR code /Barre code scanner methode
     */
    public void launchScanner() {
        if (isCameraAvailable()) {
            Intent intent = new Intent(this, ZBarScannerActivity.class);
            startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
        } else {

            Toast.makeText(this, getString(R.string.camera_indisponible),
                    Toast.LENGTH_SHORT).show();

        }
    }

    public void launchQRScanner() {
        if (isCameraAvailable()) {
            Intent intent = new Intent(this, ZBarScannerActivity.class);
            intent.putExtra(ZBarConstants.SCAN_MODES,
                    new int[]{Symbol.QRCODE});
            startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
        } else {
            Toast.makeText(this, getString(R.string.camera_indisponible),
                    Toast.LENGTH_SHORT).show();
        }
    }


    public boolean isCameraAvailable() {
        PackageManager pm = getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ZBAR_SCANNER_REQUEST:
            case ZBAR_QR_SCANNER_REQUEST:
                if (resultCode == RESULT_OK) {

                    if (scan_tag_client)
                        tag_client.setText(data.getStringExtra(SCAN_RESULT));
                    else
                        tag_mag.setText(data.getStringExtra(SCAN_RESULT));
                } else if (resultCode == RESULT_CANCELED && data != null) {
                    String error = data.getStringExtra(ZBarConstants.ERROR_INFO);
                    if (!TextUtils.isEmpty(error)) {
                        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case Constants.PIC2SHOP_REQUEST_CODE_SCAN:
                if (resultCode == RESULT_OK) {

                    String barcode = data
                            .getStringExtra(Constants.PIC2SHOP_BARCODE);
                    String resultText = barcode;
                    if (scan_tag_client)
                        tag_client.setText(resultText);
                    else
                        tag_mag.setText(resultText);

                } else if (resultCode == RESULT_CANCELED && data != null) {

                    String error = data.getStringExtra(ZBarConstants.ERROR_INFO);
                    if (!TextUtils.isEmpty(error)) {

                        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();

                    }
                }
                break;
            case INTENT_REQUEST_GET_IMAGES:
            case INTENT_REQUEST_GET_N_IMAGES:
                if (resultCode == Activity.RESULT_OK) {

                    Parcelable[] parcelableUris = data.getParcelableArrayExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);
                    int[] parcelableOrientations = data.getIntArrayExtra((ImagePickerActivity.EXTRA_IMAGE_ORIENTATIONS));
                    if (parcelableUris == null) {
                        return;
                    }

                    // Java doesn't allow array casting, this is a little hack
                    uriss = new Uri[parcelableUris.length];
                    int[] orientations = new int[parcelableUris.length];
                    System.arraycopy(parcelableUris, 0, uriss, 0, parcelableUris.length);
                    System.arraycopy(parcelableOrientations, 0, orientations, 0, parcelableOrientations.length);
                    if (uriss != null) {
                        for (Uri uri : uriss) {
                            Log.i("IMAGE SELECTION", " uri: " + uri);
                            mMedia.add(uri);
                        }
                        for (int i = 0; i < orientations.length; i++) {
                            mMediaImages.add(new Image(uriss[i], orientations[i]));

                        }
//                        if (uriss.length > 0)
//                            validatePhoto();
                    }

                }
        }
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        // TODO Auto-generated method stub
        LatLng dragPosition = marker.getPosition();
        longitude = dragPosition.longitude;
        latitude = dragPosition.latitude;
        Log.i("info", "on drag end :" + latitude + " dragLong :" + longitude);

    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onMapClick(LatLng point) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onMapLongClick(LatLng point) {

    }


}
