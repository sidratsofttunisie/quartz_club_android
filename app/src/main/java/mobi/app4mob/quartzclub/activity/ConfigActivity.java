package mobi.app4mob.quartzclub.activity;

import java.io.Serializable;
import java.util.ArrayList;

import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.utils.SessionManager;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ConfigActivity extends Activity {
    SessionManager session;
    private User user;
    private ArrayList<Produit> liste_produits;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config_layout);
        // Session Manager
        session = new SessionManager(getApplicationContext());

        activity = this;

        Bundle bn = new Bundle();
        bn = getIntent().getExtras();
        user = (User) bn.getSerializable("user");
        liste_produits = (ArrayList<Produit>) bn.getSerializable("produit");
    }

    public void logOut(View v) {
        session.logoutUser();
        Intent intent = new Intent(activity,
                LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle b = new Bundle();
        b.putSerializable("produit",
                liste_produits);
        intent.putExtras(b);
        activity.startActivity(intent);
        finish();
    }
}
