package mobi.app4mob.quartzclub.activity;

import java.io.Serializable;
import java.util.ArrayList;

import net.sourceforge.zbar.Symbol;

import com.dm.zbar.android.scanner.ZBarConstants;
import com.dm.zbar.android.scanner.ZBarScannerActivity;

import mobi.app4mob.quartzclub.constants.Constants.Extra;
import mobi.app4mob.quartzclub.model.Magasin;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.zxing.ScalingScannerActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static mobi.app4mob.quartzclub.activity.AddMagasinActivity.SCAN_RESULT;

public class ScanCarteMagasinActivity extends Activity {
    private User user;
    private Magasin magasin;

    Activity activity;
    private ArrayList<Produit> liste_produits;

    private EditText edit_tag_mag;
    private String tag_mag;

    private static final int ZBAR_SCANNER_REQUEST = 0;
    private static final int ZBAR_QR_SCANNER_REQUEST = 1;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scan_carte_magasin);

        Bundle bn = new Bundle();
        bn = getIntent().getExtras();
        user = (User) bn.getSerializable("user");
        magasin = (Magasin) bn.getSerializable("magasin");
        liste_produits = (ArrayList<Produit>) bn.getSerializable("produit");

        activity = this;

        initViews();

    }

    private void initViews() {
        TextView action_bar_title = (TextView) findViewById(R.id.txt_action_bar_title);
        action_bar_title.setText(R.string.scan_tag_magasin_title);


        edit_tag_mag = (EditText) findViewById(R.id.edit_tag_magasin);

    }

    public void scanCodeMagasin(View v) {
        goScan();
    }


    private void goScan() {
        if (isCameraAvailable()) {
            Intent intent = new Intent(this, ScalingScannerActivity.class);
            intent.putExtra("type_profil",1);
            startActivityForResult(intent, ZBAR_QR_SCANNER_REQUEST);
        } else {
            Toast.makeText(this, getString(R.string.camera_indisponible),
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void verifyTag(View v) {
        String tag_mag = edit_tag_mag.getText().toString().trim();
        Log.i(">>>",">>>>scanned="+tag_mag+">>>magasin tag"+magasin.getTag_magasin());
        if (tag_mag.equals(magasin.getTag_magasin())) {

            Intent intent;
            Bundle b = new Bundle();
            // si il existe une liste de produit dans la command => go to
            // command
            if (AddCommandActivity.list_command_product.size() > 0) {
                intent = new Intent(ScanCarteMagasinActivity.this,
                        AddCommandActivity.class);
                AddCommandActivity.my_first_time = false;
            }
            // si la liste de command est vide ouvrir directement le cam
            else {
                intent = new Intent(ScanCarteMagasinActivity.this,
                        ScanProductActivity.class);
                ScanProductActivity.my_first_time = true;
                b.putBoolean("openTheCam", true);
            }

            b.putSerializable("magasin", magasin);
            b.putSerializable("user", user);
            b.putSerializable("produit", liste_produits);
            b.putString("from_activity", Extra.SCAN_TAG_MAG);

            intent.putExtras(b);

            startActivity(intent);
        } else {
            new MyToast(getApplicationContext(), R.string.invalid_tag_magasin,
                    Toast.LENGTH_SHORT);
        }
    }

    /*
     *
     * QR code /Barre code scanner methode
     */
    public void launchScanner() {
        if (isCameraAvailable()) {
            Intent intent = new Intent(this, ZBarScannerActivity.class);
            startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
        } else {
            Toast.makeText(this, getString(R.string.camera_indisponible),
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void launchQRScanner() {
        if (isCameraAvailable()) {
            Intent intent = new Intent(this, ZBarScannerActivity.class);
            intent.putExtra(ZBarConstants.SCAN_MODES,
                    new int[]{Symbol.QRCODE});
            startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
        } else {
            Toast.makeText(this, getString(R.string.camera_indisponible),
                    Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isCameraAvailable() {
        PackageManager pm = getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ZBAR_SCANNER_REQUEST:
            case ZBAR_QR_SCANNER_REQUEST:
                if (resultCode == RESULT_OK) {

                    edit_tag_mag.setText(data
                            .getStringExtra(SCAN_RESULT));
                } else if (resultCode == RESULT_CANCELED && data != null) {
                    String error = data.getStringExtra(ZBarConstants.ERROR_INFO);
                    if (!TextUtils.isEmpty(error)) {
                        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }



    @Override
    public void onBackPressed() {
        Intent intent = new Intent(activity, DetailMagasinActivity.class);
        Bundle b = new Bundle();

        b.putSerializable("magasin", magasin);
        b.putSerializable("user", user);
        b.putSerializable("produit", liste_produits);

        intent.putExtras(b);
        // for one instence
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

}
