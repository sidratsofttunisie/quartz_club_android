package mobi.app4mob.quartzclub.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.adapter.MessageAdapter;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.model.Message;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.MessageDataParser;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;
import mobi.app4mob.quartzclub.utils.SessionManager;

public class MessageActivity extends BaseActivity implements
        OnRefreshListener<ListView> {
	Activity activity;
	private PullToRefreshListView mListView;
	private int position_selected;
	private final int NB_PRODUCT_IN_PAGE = 30;
	private ArrayList<Message> arrayHistoriques;
	private MessageAdapter mAdapter;
	private int page = 1;
	public User user;
	int type_history;
	int magasin_id = 0;
	private SessionManager session;

	public static final int COMMERCIALHISTORY = 1;
	public static final int MAGASINHISTORY = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activity = this;

//		Bundle bn = new Bundle();
//		bn = getIntent().getExtras();

		session = new SessionManager(activity);
		user = session.getUserDetails();
		
		initViews();

	}

	@Override
	protected int getLayoutResourceId() {
		return R.layout.list_gift;
	}

	@SuppressWarnings("deprecation")
	private void initViews() {
		mListView = (PullToRefreshListView) findViewById(R.id.my_list_view);
		mListView
				.setPullLabel(getString(R.string.my_pull_to_refresh_pull_label));
		mListView
				.setReleaseLabel(getString(R.string.my_pull_to_refresh_release_label));
		mListView
				.setRefreshingLabel(getString(R.string.my_pull_to_refresh_refreshing_label));

		TextView action_bar_title = (TextView) findViewById(R.id.txt_action_bar_title);
		action_bar_title.setText("Message");

		loadGift();

	}

	private void loadGift() {
        ProgressTraitment progress = new ProgressTraitment(this) {

            ArrayList<Message> tempHistoriques;

            @Override
            public void onLoadStarted() {

            	
            	String url = Constants.MESSAGE_WS_URL+"?commercial_id="+user.getId();
				//String url = "http://totalgazclub.tn/api.php/message.json?commercial_id=19";
            	tempHistoriques = new MessageDataParser()
                        .getHistorique(url);

            }

            @Override
            public void onLoadFinished() {
                mListView.onRefreshComplete();

                if (tempHistoriques != null && tempHistoriques.size() > 0) {


                    if (tempHistoriques.size() < NB_PRODUCT_IN_PAGE) {
                        mListView
                                .setOnRefreshListener((OnRefreshListener<ListView>) null);
                        mListView.setMode(Mode.DISABLED);
                    } else {
                        mListView.setOnRefreshListener(MessageActivity.this);
                        mListView.setMode(Mode.PULL_FROM_END);
                    }

                    if (page == 1) {
                    	user.setNb_message(0);
                		user.setLast_message(tempHistoriques.get(0).getCreated_at());
                		session.updateSession(user);
                        arrayHistoriques = tempHistoriques;

                        mAdapter = new MessageAdapter(MessageActivity.this,
                        		tempHistoriques);
                        mListView.setAdapter(mAdapter);

                    } else {
                        for (int i = 0; i < tempHistoriques.size(); i++) {
                            arrayHistoriques.add(tempHistoriques.get(i));
                        }

                        mAdapter.notifyDataSetChanged();

                    }

                } else if (tempHistoriques != null && tempHistoriques.size() == 0) {
                    if (page == 1) {
                        arrayHistoriques = tempHistoriques;
                    }

                    mListView
                            .setOnRefreshListener((OnRefreshListener<ListView>) null);
                    mListView.setMode(Mode.DISABLED);

                    if (arrayHistoriques != null && arrayHistoriques.size() == 0) {
                        mListView.setAdapter(null);
                        new MyToast(MessageActivity.this,
                                getString(R.string.no_notif),
                                Toast.LENGTH_SHORT);
                    }

                } else {

                    if (page > 1) {
                        page--;
                    }

                    new MyToast(MessageActivity.this,
                            getString(R.string.error_loading),
                            Toast.LENGTH_SHORT);

                }

            }

        };

        progress.DoTraitemnt("", "");
    }

	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		page++;
		loadGift();

	}

}
