package mobi.app4mob.quartzclub.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.MagasinInfoParser;
import mobi.app4mob.quartzclub.parser.ProductDataParser;
import mobi.app4mob.quartzclub.parser.UpdatePassParser;
import mobi.app4mob.quartzclub.parser.UserDataParser;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;
import mobi.app4mob.quartzclub.utils.SessionManager;

public class EditProfileActivity extends BaseActivity {


    public User user = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    private void initView() {
        user = session.getUserDetails();
        TextView action_bar_title = (TextView) findViewById(R.id.txt_action_bar_title);
        action_bar_title.setTextColor(getResources().getColor(R.color.color_white));
        action_bar_title.setText(R.string.edit_pass);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_edit_profile;
    }

    public void UpdatePass(View v) {

        if (((EditText) findViewById(R.id.old_pass)).getText().toString().trim().length()>0 && ((EditText) findViewById(R.id.new_pass)).getText().toString().trim().length()>0) {
            ProgressTraitment progress = new ProgressTraitment(this) {
                String result = null;

                @Override
                public void onLoadStarted() {

                    result = new UpdatePassParser().updatePass(Constants.UPDATEPASS_WS_URL, user.getId(), ((EditText) findViewById(R.id.old_pass)).getText().toString(), ((EditText) findViewById(R.id.new_pass)).getText().toString());

                }

                @Override
                public void onLoadFinished() {

                    if (result != null) {

                        if (result.equals("SUCCESS")) {
                            new MyToast(EditProfileActivity.this, getString(R.string.pass_update_succes),
                                    Toast.LENGTH_SHORT);

                            Intent intent = new Intent(EditProfileActivity.this, MenuActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);


                        } else if (result.equals("ERROR PASSWORD")) {
                            new MyToast(EditProfileActivity.this, getString(R.string.not_identical_pass),
                                    Toast.LENGTH_SHORT);
                        } else {
                            new MyToast(EditProfileActivity.this, getString(R.string.error_loading),
                                    Toast.LENGTH_SHORT);
                        }

                    } else {
                        new MyToast(EditProfileActivity.this, getString(R.string.error_loading),
                                Toast.LENGTH_SHORT);
                    }

                }

            };

            progress.DoTraitemnt("", "");
        }else{
            new MyToast(EditProfileActivity.this, getString(R.string.failed_data),
                    Toast.LENGTH_SHORT);
        }
    }


}
