package mobi.app4mob.quartzclub.activity;

import static android.content.Intent.ACTION_VIEW;

import java.util.ArrayList;


import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.model.Gift;
import mobi.app4mob.quartzclub.model.History;
import mobi.app4mob.quartzclub.model.MagInfo;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.GiftDataParser;
import mobi.app4mob.quartzclub.parser.ProductDataParser;
import mobi.app4mob.quartzclub.parser.UserDataParser;
import mobi.app4mob.quartzclub.utils.AlertMessage;
import mobi.app4mob.quartzclub.utils.ChecKConnection;
import mobi.app4mob.quartzclub.utils.GPSTracker;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;
import mobi.app4mob.quartzclub.utils.SessionManager;
import mobi.app4mob.quartzclub.view.CircleDisplay;
import mobi.app4mob.quartzclub.view.CircleDisplay.SelectionListener;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class HomeActivity extends BaseActivity implements SelectionListener,
        SwipeRefreshLayout.OnRefreshListener {
    public static User user = null;
    public static Produit produit;
    public static ArrayList<Produit> liste_produits = null;
    private TextView nb_points;
    private TextView nb_points_jour;
    private TextView user_name;
    private TextView gift_name;
    private TextView nb_point_manquant;
    private Button btn_historique;
    private Button btn_cadx_cible;
    Activity activity;
    Context ctx;
    private CircleDisplay mCircleDisplay;
    public static Boolean this_activity_created = true;
    GPSTracker gps;
    private SwipeRefreshLayout swipeLayout;
    public Boolean target_is_changed = false;
    private ImageView gift_image;
    private int type_profil = -1;
    private MagInfo magInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this_activity_created = true;
        // Session Manager
        session = new SessionManager(getApplicationContext());

        Bundle bn = new Bundle();
        bn = getIntent().getExtras();
        user = MenuActivity.user;
        type_profil = bn.getInt("type_profil");
        if (type_profil == 1) {
            magInfo = (MagInfo) bn.getSerializable("maginfo");
            System.out.println("Maginfo>>>>>>>>> "+magInfo.getNext_gift_image());
            ((LinearLayout) findViewById(R.id.header)).setBackgroundColor(getResources().getColor(R.color.color_rose));
        }
//		liste_produits = (ArrayList<Produit>) bn.getSerializable("produit");
        activity = this;
        ctx = this;
        gps = new GPSTracker(activity);
        gps.checkGps();

        initViews();

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.home;
    }

    private void initViews() {
        super.mTopToolbar.setBackgroundColor(getResources().getColor(R.color.toolbar));

        Log.d("ee", "init from refresh");
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        nb_points = (TextView) findViewById(R.id.nbPoints);
        nb_points_jour = (TextView) findViewById(R.id.nbPointsJour);
        gift_name = (TextView) findViewById(R.id.giftName);
        gift_image = (ImageView) findViewById(R.id.cadeau_image);
        nb_point_manquant = (TextView) findViewById(R.id.nbPointsManquant);
        user_name = (TextView) findViewById(R.id.user_name);
        btn_historique = (Button) findViewById(R.id.btn_historique);
        btn_cadx_cible = (Button) findViewById(R.id.choose_cadx_cible);

        if (type_profil == 1) {
            btn_historique.setText(getResources().getText(R.string.mesPoints_historique_magasin));
            btn_historique.setBackgroundResource(R.drawable.btn_magasin);
            btn_historique.setTextAppearance(getApplicationContext(), R.style.StyleButtonBtnMagasin);
            btn_cadx_cible.setVisibility(View.INVISIBLE);
            Picasso.with(this)
                    .load(Constants.DOMAIN + magInfo.getNext_gift_image())
                    .placeholder(R.drawable.btn_ref_s2)
                    .error(R.drawable.ic_sup)
                    .into(gift_image);
        }else
            Picasso.with(this)
                    .load(Constants.DOMAIN + Constants.UPLOADS_GIFT_FOLDER +"/"+ user.getTarget_gift().getImage())
                    .placeholder(R.drawable.btn_ref_s2)
                    .error(R.drawable.ic_sup)
                    .into(gift_image);

        System.out.println("usergiftimg>>>>>>>>> "+user.getTarget_gift().getImage());

        int numPoints = MenuActivity.user.getPoint_jour();
        String strPointsFormat = getResources().getString(R.string.mesPoints_points_du_jour);
        String strPointsMsg = String.format(strPointsFormat, numPoints);

        SpannableString str = new SpannableString(strPointsMsg);
        int index = str.toString().indexOf("@");
        Drawable d = getResources().getDrawable(R.drawable.goute_liste);
        d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
        ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BOTTOM);
        str.setSpan(span, index, index + 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        nb_points_jour.setText(str);


        mCircleDisplay = (CircleDisplay) findViewById(R.id.circleDisplay);
        RelativeLayout mCircleDisplayLayout = (RelativeLayout) findViewById(R.id.circleDisplayLayout);

        mCircleDisplay.setAnimDuration(2000);
        mCircleDisplay.setValueWidthPercent(15f);
        mCircleDisplay.setFormatDigits(1);
        mCircleDisplay.setDimAlpha(0);
        mCircleDisplay.setSelectionListener(this);
        mCircleDisplay.setTouchEnabled(false);
        mCircleDisplay.setUnit("%");
        mCircleDisplay.setStepSize(0.5f);
        User userrr = MenuActivity.user;
        user_name.setText(MenuActivity.user.getFirst_name() + " "
                + MenuActivity.user.getLast_name());
        nb_points.setText("" + MenuActivity.user.getPoint());
        gift_name.setText(MenuActivity.user.getTarget_gift().getName());

        if (type_profil == 1) {
            user_name.setText(magInfo.getMagasin_name());
            nb_points.setText("" + magInfo.getPoint());
            gift_name.setText(magInfo.getNext_gift_name());
            if (Integer.parseInt(magInfo.getPoint()) < Integer.parseInt(magInfo.getNext_gift_point())) {
                nb_point_manquant.setText(Integer.parseInt(magInfo.getNext_gift_point())
                        - Integer.parseInt(magInfo.getPoint()) + "");

                float percentage_value = (Integer.parseInt(magInfo.getPoint()) * 100)
                        / Integer.parseInt(magInfo.getNext_gift_point());
                System.out.println(">>>>>>>>POURCENTAGE>>>>>>>"+percentage_value);

                mCircleDisplay.showValue(percentage_value, 100f, true);

                LinearLayout nbPointsManquantLayout = (LinearLayout) findViewById(R.id.nbPointsManquantLayout);
                TextView linkToWebSite = (TextView) findViewById(R.id.linkToWebSite);
                // mCircleDisplayLayout.setVisibility(View.GONE);
                nbPointsManquantLayout.setVisibility(View.VISIBLE);
                linkToWebSite.setVisibility(View.GONE);
                linkToWebSite.setMovementMethod(LinkMovementMethod
                        .getInstance());
            }
            // si le cadeau cible est ateint
            else {
                LinearLayout nbPointsManquantLayout = (LinearLayout) findViewById(R.id.nbPointsManquantLayout);
                TextView linkToWebSite = (TextView) findViewById(R.id.linkToWebSite);
                // mCircleDisplayLayout.setVisibility(View.GONE);
                nbPointsManquantLayout.setVisibility(View.GONE);
                linkToWebSite.setVisibility(View.VISIBLE);
                linkToWebSite.setMovementMethod(LinkMovementMethod.getInstance());
                // gift_name.setText("Liste des cadeaux");
                mCircleDisplay.showValue(100, 100f, true);
            }
            ((Button) findViewById(R.id.button_cadeau)).setVisibility(View.INVISIBLE);
        }else {

            if (MenuActivity.user.getTarget_gift() != null) {
                if (MenuActivity.user.getPoint() < Integer.parseInt(MenuActivity.user.getTarget_gift().getCommercial_points())) {
                    nb_point_manquant.setText(Integer.parseInt(MenuActivity.user.getTarget_gift().getCommercial_points())
                            - MenuActivity.user.getPoint() + "");

                    float percentage_value = (MenuActivity.user.getPoint() * 100)
                            / Integer.parseInt(MenuActivity.user.getTarget_gift().getCommercial_points());

                    mCircleDisplay.showValue(percentage_value, 100f, true);

                    LinearLayout nbPointsManquantLayout = (LinearLayout) findViewById(R.id.nbPointsManquantLayout);
                    TextView linkToWebSite = (TextView) findViewById(R.id.linkToWebSite);
                    // mCircleDisplayLayout.setVisibility(View.GONE);
                    nbPointsManquantLayout.setVisibility(View.VISIBLE);
                    linkToWebSite.setVisibility(View.GONE);
                    linkToWebSite.setMovementMethod(LinkMovementMethod
                            .getInstance());
                }
                // si le cadeau cible est ateint
                else {
                    LinearLayout nbPointsManquantLayout = (LinearLayout) findViewById(R.id.nbPointsManquantLayout);
                    TextView linkToWebSite = (TextView) findViewById(R.id.linkToWebSite);
                    // mCircleDisplayLayout.setVisibility(View.GONE);
                    nbPointsManquantLayout.setVisibility(View.GONE);
                    linkToWebSite.setVisibility(View.VISIBLE);
                    linkToWebSite.setMovementMethod(LinkMovementMethod.getInstance());
                    // gift_name.setText("Liste des cadeaux");
                    mCircleDisplay.showValue(100, 100f, true);
                }
            } else {
                LinearLayout nbPointsManquantLayout = (LinearLayout) findViewById(R.id.nbPointsManquantLayout);
                mCircleDisplayLayout.setVisibility(View.GONE);
                nbPointsManquantLayout.setVisibility(View.GONE);
                gift_name.setText(getString(R.string.liste_cadeaux));
            }
        }

//        gift_name.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                goToListGift();
//
//            }
//        });

    }

    public void logOut(View v) {
        session.logoutUser();
        Intent intent = new Intent(activity, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle b = new Bundle();
        b.putSerializable("produit", liste_produits);
        intent.putExtras(b);
        activity.startActivity(intent);
        finish();
    }

    public void goToListMagasin(View v) {

        //if (checkPick2shopInstalled()) {
        Intent intent = new Intent(activity, ListMagasinActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", user);
        b.putSerializable("produit", liste_produits);
        intent.putExtras(b);
        activity.startActivity(intent);
        //}
    }

    public void goToBonus(View v) {

        //if (checkPick2shopInstalled()) {
        Intent intent = new Intent(activity, ListBonusActivity.class);
        intent.putExtra("type_history", 1);
        activity.startActivity(intent);
        //}
    }

    public void goToHistorique(View v) {

        //if (checkPick2shopInstalled()) {
        Intent intent = new Intent(activity, HistoryActivity.class);
        Bundle b = new Bundle();
        if (type_profil == 1) {
            b.putInt("type_history", HistoryActivity.MAGASINHISTORY);
            b.putInt("magasin_id", Integer.parseInt(magInfo.getId()));
        } else
            b.putInt("type_history", HistoryActivity.COMMERCIALHISTORY);

        intent.putExtras(b);
        activity.startActivity(intent);
        //}
    }

    public void goToConfig(View v) {

        Intent intent = new Intent(activity, ConfigActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", user);
        b.putSerializable("produit", liste_produits);
        intent.putExtras(b);
        activity.startActivity(intent);
    }


    public void goToListGift(View v) {
        Log.e("****", "i'm into gotogift");
        Intent intent = new Intent(activity, ListGiftActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", user);
        b.putSerializable("produit", liste_produits);
        b.putInt("type_profil", ListBonusActivity.COMMERCIALBONUS);
        intent.putExtras(b);
        activity.startActivity(intent);
    }

    public void goToListPromos(View v) {

        Intent intent = new Intent(activity, ListPromosActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", user);
        b.putSerializable("produit", liste_produits);
        intent.putExtras(b);
        activity.startActivity(intent);
    }

    public void goToAddMagasin(View v) {

        //if (checkPick2shopInstalled()) {
        Intent intent = new Intent(activity, AddMagasinActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", user);
        b.putSerializable("produit", liste_produits);
        intent.putExtras(b);
        activity.startActivity(intent);
        //	}

    }

    private boolean checkPick2shopInstalled() {
        Intent test = new Intent(Constants.Pro.ACTION);
        if (ctx.getPackageManager().resolveActivity(test, 0) == null) {
            AlertMessage dialogDeleteCommand = new AlertMessage(this) {

                @Override
                public void onOk() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onCancel() {
                    // TODO Auto-generated method stub

                }
            };
            dialogDeleteCommand.show1(getString(R.string.installer_pic2shop), getString(R.string.quartz_club), true, "Ok");
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onSelectionUpdate(float val, float maxval) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onValueSelected(float val, float maxval) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onResume() {
        super.onResume(); // Always call the superclass method first
        MenuActivity.user = session.getUserDetails();
        if (!this_activity_created) {
            initViews();
        }
        this_activity_created = false;
    }

    @Override
    public void onRefresh() {
        final Handler uiThreadCallback = new Handler();
        new Thread() {
            boolean noConnection = false;

            @Override
            public void run() {
                Looper.prepare();

                if (new ChecKConnection(HomeActivity.this).verifierconnexion()) {

                    // Session Manager
                    session = new SessionManager(getApplicationContext());

                    MenuActivity.user = session.getUserDetails();
                    new UserDataParser();
                    UserDataParser.updateUser(Constants.UPDATE_USER,
                            user.getId(), getApplicationContext());

                }

                uiThreadCallback.post(new Runnable() {
                    @Override
                    public void run() {
                        MenuActivity.user = session.getUserDetails();
                        initViews();
                        swipeLayout.setRefreshing(false);
                    }

                });
            }
        }.start();

    }


    public void goToChooseGift(View view) {
        Intent intent = new Intent(activity, ListGiftActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", user);
        b.putSerializable("produit", liste_produits);
        b.putInt("type_profil", ListBonusActivity.COMMERCIALBONUS);
        intent.putExtra("cible",true);
        intent.putExtras(b);
        activity.startActivity(intent);
    }
}
