package mobi.app4mob.quartzclub.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.adapter.BonusPagerAdapter;
import mobi.app4mob.quartzclub.adapter.ExpandableBonusListAdapter;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.gcm.GCMRegistrationIntentService;
import mobi.app4mob.quartzclub.model.Message;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.BonusDataParser;
import mobi.app4mob.quartzclub.parser.ProductDataParser;
import mobi.app4mob.quartzclub.parser.RegisterTokenParser;
import mobi.app4mob.quartzclub.parser.UserDataParser;
import mobi.app4mob.quartzclub.utils.ChecKConnection;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;
import mobi.app4mob.quartzclub.utils.SessionManager;
import mobi.app4mob.quartzclub.view.CircleDisplay;
import mobi.app4mob.quartzclub.view.CircleDisplay.SelectionListener;

public class    MenuActivity extends BaseActivity implements SelectionListener {

    private CircleDisplay mCircleDisplay;
    public SessionManager session;
    public static User user = null;
    public static Produit produit;
    public static ArrayList<Produit> liste_produits = null;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private BroadcastReceiver mMessageReceiver;
    TextView nb_message;
    LinearLayout bloc_nb_message;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Session Manager
        session = new SessionManager(getApplicationContext());
//        TextView action_bar_title = (TextView) findViewById(R.id.txt_action_bar_title);
//        action_bar_title.setTextColor(getResources().getColor(R.color.color_blue));
//        action_bar_title.setText(R.string.setting_menu);

        Bundle bn = new Bundle();
        bn = getIntent().getExtras();
//        System.out.println(">>>>>>>MENU>>>>>>" + liste_produits.size());

        initGcm();

        loadData();

        initView();
    }

    private void loadData() {
        ProgressTraitment progress = new ProgressTraitment(this) {

            @Override
            public void onLoadStarted() {

                user = session.getUserDetails();
                new UserDataParser();
                UserDataParser.updateUser(Constants.UPDATE_USER,
                        user.getId(), getApplicationContext());
                liste_produits = new ProductDataParser()
                        .getAllProduit(Constants.GET_ALL_PRODUC_WS_URL+"?pays_id="+user.getPays_id());

                if (liste_produits != null && liste_produits.size() > 0) {
                    Produit.liste_produits = liste_produits;
                }

            }

            @Override
            public void onLoadFinished() {
                updateNbMessage();
            }

        };

        progress.DoTraitemnt("", "");

    }

    public void updateNbMessage(){
        System.out.println("NB MESSAGE NOTTIF= "+user.getNb_message());
        bloc_nb_message = (LinearLayout) findViewById(R.id.bloc_nb_message);
        if(user.getNb_message()>0){
            bloc_nb_message.setVisibility(View.VISIBLE);
            nb_message =  (TextView) findViewById(R.id.nb_message);
            nb_message.setText(user.getNb_message()+"");
        }else
            bloc_nb_message.setVisibility(View.INVISIBLE);

    }

    private void initGcm() {

        mMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                System.out.println("im in OnReveive*********************");

                String message = null;
                message = intent.getStringExtra("message");
                if (message!= null){
                    user.setNb_message(user.getNb_message()+1);
                    updateNbMessage();
                }

                //do other stuff here
            }
        };

        //Initializing our broadcast receiver
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {

            //When the broadcast received
            //We are sending the broadcast from GCMRegistrationIntentService

            @Override
            public void onReceive(Context context, Intent intent) {
                //If the broadcast has received with success
                //that means device is registered successfully

                if(intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_SUCCESS)){
                    //Getting the registration token from the intent
                    String token = intent.getStringExtra("token");

                    //Displaying the token as toast
                     //Toast.makeText(getApplicationContext(), "Registration token:" + token, Toast.LENGTH_LONG).show();

                     sendToken(token);

                    //if the intent is not with success then displaying error messages
                } else if(intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_ERROR)){
                    Toast.makeText(getApplicationContext(), getString(R.string.gcm_registration_error), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.error_occured), Toast.LENGTH_LONG).show();
                }
            }
        };

        //Checking play service is available or not
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());

        //if play service is not available
        if(ConnectionResult.SUCCESS != resultCode) {
            //If play service is supported but not installed
            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                //Displaying message that play service is not installed
                Toast.makeText(getApplicationContext(),getString(R.string.google_play_not_installed), Toast.LENGTH_LONG).show();
                GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());

                //If play service is not supported
                //Displaying an error message
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.google_play_not_supported), Toast.LENGTH_LONG).show();
            }

            //If play service is available
        } else {
            //Starting intent to register device
            Intent itent = new Intent(this, GCMRegistrationIntentService.class);
            startService(itent);
        }

    }

    private void sendToken(final String token) {

        ProgressTraitment progress = new ProgressTraitment(this) {

            @Override
            public void onLoadStarted() {

                user = session.getUserDetails();
                String result = new RegisterTokenParser().register(Constants.NOTIFICATION_WS_URL,
                        user.getId(),token,"a8a8a8" );

                if (result != null && result.equals("success")) {
                    //todo
                }else{
                    new MyToast(MenuActivity.this,
                            getString(R.string.register_notif_error),
                            Toast.LENGTH_SHORT);
                }

            }

            @Override
            public void onLoadFinished() {

            }

        };

        progress.DoTraitemnt("", "");

    }

    protected int getLayoutResourceId() {
        return R.layout.activity_menu;
    }

    private void initView() {
        super.mTopToolbar.setBackgroundColor(getResources().getColor(R.color.toolbar));
        mCircleDisplay = (CircleDisplay) findViewById(R.id.circleDisplay);
        RelativeLayout mCircleDisplayLayout = (RelativeLayout) findViewById(R.id.circleDisplayLayout);

        mCircleDisplay.setAnimDuration(2000);
        mCircleDisplay.setValueWidthPercent(15f);
        mCircleDisplay.setFormatDigits(1);
        mCircleDisplay.setDimAlpha(0);
        mCircleDisplay.setSelectionListener(this);
        mCircleDisplay.setTouchEnabled(false);
        mCircleDisplay.setUnit("%");
        mCircleDisplay.setStepSize(0.5f);
        mCircleDisplay.setCustomText(new String[]{""});
        mCircleDisplay.showValue(100f, 100f, true);
    }

    public void goToListGift(View v) {
        Log.e("****", "i'm into gotogift");
        Intent intent = new Intent(this, ListGiftActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", HomeActivity.user);
        b.putSerializable("produit", HomeActivity.liste_produits);
        b.putInt("type_profil", 1); // bonus commercial
        intent.putExtra("consultation",true);
        intent.putExtras(b);
        this.startActivity(intent);
    }

    public void MesPoints(View v) {

        Bundle bn = new Bundle();
//        bn = getIntent().getExtras();
        HomeActivity.user = MenuActivity.user;
        HomeActivity.liste_produits = MenuActivity.liste_produits;

        Intent intent;

        intent = new Intent(this, HomeActivity.class);
        bn.putInt("type_profil", 0);
        intent.putExtras(bn);
        startActivity(intent);
    }

    public void goToMesBonus(View v) {

        Intent intent;

        intent = new Intent(this, ListBonusActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", MenuActivity.user);
        b.putSerializable("produit", MenuActivity.liste_produits);
        b.putInt("type_profil", 1); // bonus commercial
        intent.putExtras(b);
        startActivity(intent);
    }

    public void goToMesNotifs(View v) {

        Intent intent;

        intent = new Intent(this, MessageActivity.class);
        startActivity(intent);
    }

    public void goToMesBonusMagasin(View v) {

        Intent intent;

        intent = new Intent(this, ListBonusActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", MenuActivity.user);
        b.putSerializable("produit", MenuActivity.liste_produits);
        b.putInt("type_profil", 2); // bonus commercial
        intent.putExtras(b);
        startActivity(intent);
    }

    public void goToListGiftMagasin(View v) {
        Log.e("****", "i'm into gotogift");
        Intent intent = new Intent(this, ListGiftActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", HomeActivity.user);
        b.putSerializable("produit", HomeActivity.liste_produits);
        b.putInt("type_profil", 2); // bonus commercial
        intent.putExtras(b);
        this.startActivity(intent);
    }

    public void goToAddMagasin(View v) {

        //if (checkPick2shopInstalled()) {
        Intent intent = new Intent(this, AddMagasinActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", user);
        b.putSerializable("produit", liste_produits);
        intent.putExtras(b);
        this.startActivity(intent);
        //	}

    }

    public void goToVente(View v) {

        //if (checkPick2shopInstalled()) {
        Intent intent = new Intent(this, ScanMagasinVenteActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", user);
        b.putSerializable("produit", liste_produits);
        intent.putExtras(b);
        this.startActivity(intent);
        //	}

    }

    public void goToPointMagasin(View v){

        Intent intent = new Intent(this, ScanMagToPtsNHisActivity.class);
        this.startActivity(intent);

    }

    @Override
    public void onResume() {
        super.onResume(); // Always call the superclass method first
        HomeActivity.user = session.getUserDetails();
        loadData();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_SUCCESS));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_ERROR));
        getApplicationContext().registerReceiver(mMessageReceiver, new IntentFilter("home_activity"));

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.w("home_activity", "onPause");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        getApplicationContext().unregisterReceiver(mMessageReceiver);
    }


    @Override
    public void onSelectionUpdate(float val, float maxval) {

    }

    @Override
    public void onValueSelected(float val, float maxval) {

    }



}
