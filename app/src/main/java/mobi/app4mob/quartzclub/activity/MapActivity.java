package mobi.app4mob.quartzclub.activity;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.VisibleRegion;

import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

import mobi.app4mob.quartzclub.adapter.ListMagasinsAdapter;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.Magasin;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.utils.GPSTracker;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class MapActivity extends FragmentActivity {
    public static User user;
    public static ArrayList<Magasin> magasins;
    Activity activity;
    private GoogleMap mMap;
    public static FragmentManager fragmentManager;
    private int page = 1;

    private Location myLocation;
    private double latitude = 0;
    private double longitude = 0;

    private double long1 = 0;
    private double lat1 = 0;
    private double long2 = 0;
    private double lat2 = 0;

    private Boolean camera_moved = false;
    private int marker_position;
    private ArrayList<Produit> liste_produits;
    GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.map_layout);
        Bundle bn = new Bundle();
        bn = getIntent().getExtras();
        user = (User) bn.getSerializable("user");
        liste_produits = (ArrayList<Produit>) bn.getSerializable("produit");

        TextView action_bar_title = (TextView) findViewById(R.id.txt_action_bar_title);
        action_bar_title.setText(R.string.map_title);

        ((Button) findViewById(R.id.ab_refersh_map))
                .setVisibility(View.VISIBLE);

        ((Button) findViewById(R.id.ab_list_magasin))
                .setVisibility(View.VISIBLE);

        ((Button) findViewById(R.id.ab_scan_magasin))
                .setVisibility(View.VISIBLE);

        activity = this;
        gps = new GPSTracker(activity);
        gps.checkGps();
        initLatLong();

        fragmentManager = getSupportFragmentManager();
        setUpMap();

    }

    public void refreshMap(View v) {

        loadMagasin();
    }

    public void getToListMagasin(View v) {

        Intent intent = new Intent(MapActivity.this,
                ListMagasinActivity.class);
        //for one instence
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        Bundle b = new Bundle();
        b.putSerializable("user", (Serializable) user);
        b.putSerializable("produit", (Serializable) liste_produits);

        intent.putExtras(b);
        finish();
        startActivity(intent);
    }

    public void getToScanView(View v) {

        Intent intent = new Intent(MapActivity.this,
                ScanMagasinVenteActivity.class);
        //for one instence
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        Bundle b = new Bundle();
        b.putSerializable("user", (Serializable) user);
        b.putSerializable("produit", (Serializable) liste_produits);

        intent.putExtras(b);
        finish();
        startActivity(intent);
    }

    private void setUpMap() {
        // Do a null check to confirm that we have not already instantiated the
        // map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) MapActivity.fragmentManager
                    .findFragmentById(R.id.map_add)).getMap();
            // mMap = ((NiceSupportMapFragment)
            // getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            // ((NiceSupportMapFragment)
            // getSupportFragmentManager().findFragmentById(R.id.map))
            // .setPreventParentScrolling(true);
            // Check if we were successful in obtaining the map.
            if (mMap != null) {

                mMap.getUiSettings().setScrollGesturesEnabled(true);
                mMap.getUiSettings().setZoomControlsEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                        latitude, longitude), 14));
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mMap.setMyLocationEnabled(true);
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latitude, longitude)).draggable(false).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA)));

                // Setting a custom info window adapter for the google map
                mMap.setInfoWindowAdapter(new InfoWindowAdapter() {

                    // Use default InfoWindow frame
                    @Override
                    public View getInfoContents(Marker arg0) {
                        return null;
                    }

                    // Defines the contents of the InfoWindow
                    @Override
                    public View getInfoWindow(Marker marker) {

                        // Getting view from the layout file info_window_layout
                        View v = getLayoutInflater().inflate(
                                R.layout.cell_info_window, null);

                        Magasin mag = null;
                        for (Magasin m : magasins) {
                            if (m.getMarker() != null)
                                if (m.getMarker().equals(marker))
                                    mag = m;
                        }

                        // Getting reference to the TextView to set latitude
                        TextView name = (TextView) v.findViewById(R.id.name);

                        // Getting reference to the TextView to set longitude
                        TextView address = (TextView) v
                                .findViewById(R.id.adresse);
                        if (mag == null) {
                            View view = null;
                            RefreshPositionActivity refreshPositionActivity = new RefreshPositionActivity();
                            refreshPositionActivity.refreshPos(view);
                        }
                        if (mag != null) {
                            // Setting the latitude
                            name.setText(mag.getMagasin_name());

                            // Setting the longitude
                            address.setText(mag.getAddress());
                        }


                        // Returning the view containing InfoWindow contents
                        return v;

                    }
                });

                mMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        Magasin mag = null;
                        for (Magasin m : magasins) {
                            if (m.getMarker() != null)
                                if (m.getMarker().equals(marker))
                                    mag = m.set_marker(null);
                        }
                        Intent intent = new Intent(MapActivity.this,
                                DetailMagasinActivity.class);
                        Bundle b = new Bundle();
                        b.putSerializable("magasin", (Serializable) mag);
                        b.putSerializable("user", (Serializable) user);
                        b.putSerializable("produit",
                                (Serializable) liste_produits);

                        intent.putExtras(b);
                        intent.putExtra("carte", true);

                        startActivity(intent);

                    }

                });

                mMap.setOnCameraChangeListener(new OnCameraChangeListener() {
                    @Override
                    public void onCameraChange(CameraPosition position) {
                        if (!camera_moved)
                            loadMagasin();
                        camera_moved = true;
                    }
                });

            }
        }
    }

    private void loadMagasin() {
        mMap.clear();
        Boolean f  = true;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
           f = false;
            return;
        }
        if (f)
        mMap.setMyLocationEnabled(true);

        VisibleRegion visible_region = mMap.getProjection().getVisibleRegion();

        long1 = visible_region.latLngBounds.northeast.longitude;
        lat1 = visible_region.latLngBounds.northeast.latitude;

        long2 = visible_region.latLngBounds.southwest.longitude;
        lat2 = visible_region.latLngBounds.southwest.latitude;

        ProgressTraitment progress = new ProgressTraitment(this) {

            ArrayList<Magasin> tempMagasins;

            @Override
            public void onLoadStarted() {

                tempMagasins = new Magasin().getMagasinNearMe(
                        Constants.MAGASIN_WS_URL, user.getId(), long1, lat1,
                        long2, lat2, latitude, longitude, "", 1);

            }

            @Override
            public void onLoadFinished() {

                if (tempMagasins != null && tempMagasins.size() > 0) {
                    magasins = tempMagasins;
                    for (int i = 0; i < tempMagasins.size(); i++) {
                        /*Functions.LOGI("***** magasin name *****", tempMagasins
                                .get(i).getMagasin_name());*/
                        magasins.get(i)
                                .setMarker(
                                        mMap.addMarker(new MarkerOptions()
                                                .position(
                                                        new LatLng(
                                                                tempMagasins
                                                                        .get(i)
                                                                        .getLatitude(),
                                                                tempMagasins
                                                                        .get(i)
                                                                        .getLongitude()))
                                                .draggable(true)
                                                .title(tempMagasins.get(i)
                                                        .getMagasin_name())

                                                .icon(BitmapDescriptorFactory
                                                        .fromResource(R.drawable.ic_map_pin_toaster))

                                        ));

                    }

                } else if (tempMagasins != null && tempMagasins.size() == 0) {

                    new MyToast(MapActivity.this,
                            getString(R.string.no_magasin),
                            MyToast.LENGTH_SHORT);

                }

            }

        };

        progress.DoTraitemnt("", "");
    }

    private void initLatLong() {
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        } else {
            longitude = 10.180744;
            latitude = 36.799152;
            Functions.LOGE("ERROR",
                    getString(R.string.pas_pu_localiser_emplacement));
            new MyToast(getApplicationContext(),
                    getString(R.string.pas_pu_localiser_emplacement),
                    MyToast.LENGTH_SHORT);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(activity, MenuActivity.class);
        Bundle b = new Bundle();

        b.putSerializable("user", user);
        b.putSerializable("produit", liste_produits);

        intent.putExtras(b);
        // for one instence
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }



}
