package mobi.app4mob.quartzclub.activity;

import java.io.Serializable;
import java.util.ArrayList;

import mobi.app4mob.quartzclub.adapter.ListGiftsAdapter;
import mobi.app4mob.quartzclub.adapter.ListMagasinsAdapter;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.CommandProduct;
import mobi.app4mob.quartzclub.model.Gift;
import mobi.app4mob.quartzclub.model.Magasin;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.GiftDataParser;
import mobi.app4mob.quartzclub.parser.UserDataParser;
import mobi.app4mob.quartzclub.rest.ApiClient;
import mobi.app4mob.quartzclub.rest.ApiInterface;
import mobi.app4mob.quartzclub.rest.RestClient;
import mobi.app4mob.quartzclub.utils.AlertMessage;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;
import mobi.app4mob.quartzclub.utils.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import com.google.gson.JsonObject;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class ListGiftActivity extends BaseActivity implements
        OnRefreshListener<ListView>, OnItemClickListener {
    Activity activity;
    private PullToRefreshListView mListView;
    private int position_selected;
    private final int NB_PRODUCT_IN_PAGE = 30;
    private ArrayList<Gift> arrayGifts;
    private ListGiftsAdapter mAdapter;
    private int page = 1;
    public static User user;

    private Location myLocation;
    private double latitude = 0;
    private double longitude = 0;
    private String keyword = "";
    private ArrayList<Produit> liste_produits;
    private SessionManager session;
    public Boolean target_is_changed = false;
    private int type_profil;
    Boolean cible = false;
    Boolean consultation = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bn = new Bundle();
        bn = getIntent().getExtras();
        cible = bn.getBoolean("cible",false);
        consultation = bn.getBoolean("consultation",false);
        user = (User) bn.getSerializable("user");
        liste_produits = (ArrayList<Produit>) bn.getSerializable("produit");
        type_profil = bn.getInt("type_profil");
        activity = this;
        initViews();

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.list_gift;
    }

    @SuppressWarnings("deprecation")
    private void initViews() {

        if (type_profil == ListBonusActivity.COMMERCIALBONUS) {
            super.mTopToolbar.setBackgroundColor(getResources().getColor(R.color.color_bleu_marine));
        } else {
            super.mTopToolbar.setBackgroundColor(getResources().getColor(R.color.color_rose));
        }

        mListView = (PullToRefreshListView) findViewById(R.id.my_list_view);
        mListView
                .setPullLabel(getString(R.string.my_pull_to_refresh_pull_label));
        mListView
                .setReleaseLabel(getString(R.string.my_pull_to_refresh_release_label));
        mListView
                .setRefreshingLabel(getString(R.string.my_pull_to_refresh_refreshing_label));
        if (type_profil == ListBonusActivity.COMMERCIALBONUS)
            if (!consultation)
            mListView.setOnItemClickListener(this);

        TextView action_bar_title = (TextView) findViewById(R.id.txt_action_bar_title);
        action_bar_title.setText(R.string.list_gift_title);

        loadGift();

    }

    private void loadGift() {
        ProgressTraitment progress = new ProgressTraitment(this) {

            ArrayList<Gift> tempGifts;

            @Override
            public void onLoadStarted() {

                tempGifts = new GiftDataParser()
                        .getGift(Constants.Gifts_WS_URL+"?pays_id="+user.getPays_id());

            }

            @Override
            public void onLoadFinished() {
                mListView.onRefreshComplete();

                if (tempGifts != null && tempGifts.size() > 0) {


                    if (tempGifts.size() < NB_PRODUCT_IN_PAGE) {
                        mListView
                                .setOnRefreshListener((OnRefreshListener<ListView>) null);
                        mListView.setMode(Mode.DISABLED);
                    } else {
                        mListView.setOnRefreshListener(ListGiftActivity.this);
                        mListView.setMode(Mode.PULL_FROM_END);
                    }

                    if (page == 1) {
                        arrayGifts = tempGifts;

                        mAdapter = new ListGiftsAdapter(ListGiftActivity.this,
                                tempGifts, user, type_profil,cible);
                        mListView.setAdapter(mAdapter);

                    } else {
                        for (int i = 0; i < tempGifts.size(); i++) {
                            arrayGifts.add(tempGifts.get(i));
                        }

                        mAdapter.notifyDataSetChanged();

                    }

                } else if (tempGifts != null && tempGifts.size() == 0) {
                    if (page == 1) {
                        arrayGifts = tempGifts;
                    }

                    mListView
                            .setOnRefreshListener((OnRefreshListener<ListView>) null);
                    mListView.setMode(Mode.DISABLED);

                    if (arrayGifts != null && arrayGifts.size() == 0) {
                        mListView.setAdapter(null);
                        new MyToast(ListGiftActivity.this,
                                getString(R.string.no_magasin),
                                Toast.LENGTH_SHORT);
                    }

                } else {

                    if (page > 1) {
                        page--;
                    }

                    new MyToast(ListGiftActivity.this,
                            getString(R.string.error_loading),
                            Toast.LENGTH_SHORT);

                }

            }

        };

        progress.DoTraitemnt("", "");
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

        int position = arg2 > 0 ? (arg2 - 1) : 0;

        position_selected = position;
        final int commercialPoints = Integer.parseInt(arrayGifts.get(position_selected).getCommercial_points());
        int userPoints = user.getPoint();
        if (cible){
            AlertMessage dialogAddToTargetGift = new AlertMessage(this) {


                @Override
                public void onOk() {
                    Gift target_gift = arrayGifts.get(position_selected);
                    user.setTarget_gift(target_gift);
                    // Update Session User(with new solde point)
                    sendTargetGift(target_gift,
                            user.getId());
                    session = new SessionManager(getApplicationContext());
                    session.updateSession(user);


                }

                @Override
                public void onCancel() {
                    Functions.LOGI("*****", "on cancle");
                }
            };

                dialogAddToTargetGift.show2(
                        getString(R.string.alert_add_to_target_gift_message),
                        getString(R.string.alert_add_to_target_gift_title), false,
                        getString(R.string.valider), getString(R.string.annuler));



        }

        else
        {
            if ( userPoints>=commercialPoints ) {
                AlertMessage dialogAddToTargetGift = new AlertMessage(this) {


                    @Override
                    public void onOk() {
                        Gift target_gift = arrayGifts.get(position_selected);


                        requestGift(target_gift.getId(),MenuActivity.user.getId(),commercialPoints);
                        session = new SessionManager(getApplicationContext());
                        session.updateSession(user);
                    }

                    @Override
                    public void onCancel() {
                        Functions.LOGI("*****", "on cancle");
                    }
                };
                dialogAddToTargetGift.show2(
                        getString(R.string.alert_request_gift),
                        getString(R.string.alert_request_gift), false,
                        getString(R.string.valider), getString(R.string.annuler));
            } else {


                AlertMessage dialogAddToTargetGift = new AlertMessage(this) {


                    @Override
                    public void onOk() {

                    }

                    @Override
                    public void onCancel() {
                        Functions.LOGI("*****", "on cancle");
                    }
                };

                    dialogAddToTargetGift.show2(
                            getString(R.string.alert_no_product_cant_request),
                            getString(R.string.alert_no_product_cant_request), false,
                            getString(R.string.valider), getString(R.string.annuler));

            }
        }

    }

    public Boolean sendTargetGift(final Gift target_gift, final int commercial_id) {

        target_is_changed = false;
        ProgressTraitment prog = new ProgressTraitment(this) {

            @Override
            public void onLoadStarted() {

                target_is_changed = GiftDataParser.addToTargetGift(""
                        + commercial_id, "" + target_gift.getId());

            }

            @Override
            public void onLoadFinished() {
                if (target_is_changed) {

                    user.setTarget_gift(target_gift);

                    Intent intent = new Intent(ListGiftActivity.this,
                            HomeActivity.class);
                    Bundle b = new Bundle();

                    //for one instance
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    b.putSerializable("produit",
                            liste_produits);
                    b.putSerializable("user", user);

                    intent.putExtras(b);

                    startActivity(intent);
                } else {

                    new MyToast(ListGiftActivity.this,
                            getString(R.string.error_loading),
                            Toast.LENGTH_SHORT);

                }
            }
        };
        prog.DoTraitemnt("", "");
        return target_is_changed;

    }

    @Override
    public void onRefresh(PullToRefreshBase<ListView> refreshView) {
        page++;
        loadGift();

    }

    void requestGift(final int gift_id, final int user_id, final int commercialPoints){
        ProgressTraitment prog = new ProgressTraitment(this) {

            @Override
            public void onLoadStarted() {
                ApiInterface service = RestClient.getClient();
                Call<JsonObject> call = service.requestGift(user_id,gift_id);
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if(response.body()!=null){
                            JsonObject body = response.body();
                            String message =  body.get("status").getAsString();
                            if (message.equals("success")){
                                MenuActivity.user.setPoint(MenuActivity.user.getPoint()-commercialPoints);
                                session = new SessionManager(getApplicationContext());
                                session.updateSession(user);
                                AlertMessage dialogAddToTargetGift = new AlertMessage(ListGiftActivity.this) {
                                    @Override
                                    public void onOk() {
                                        finish();
                                    }

                                    @Override
                                    public void onCancel() {
                                        loadGift();
                                        return;
                                    }
                                };
                                dialogAddToTargetGift.show2(
                                        getString(R.string.alert_request_gift_sucess),
                                        getString(R.string.alert_request_gift_sucess), false,
                                        getString(R.string.retour), getString(R.string.annuler));
                            }else{
                            AlertMessage dialogAddToTargetGift = new AlertMessage(ListGiftActivity.this) {
                                @Override
                                public void onOk() {
                                    finish();
                                }

                                @Override
                                public void onCancel() {
                                    return;
                                }
                            };
                            dialogAddToTargetGift.show2(
                                    "    "+getString(R.string.alert_request_gift_error)+"            ",
                                    "    "+getString(R.string.alert_request_gift_error)+"               ", false,
                                    getString(R.string.retour), getString(R.string.annuler));
                        }}

                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        return;
                    }
                });

            }

            @Override
            public void onLoadFinished() {


            }
        };
        prog.DoTraitemnt("", "");

        return;
    }


}
