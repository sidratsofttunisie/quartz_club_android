package mobi.app4mob.quartzclub.activity;

import java.io.Serializable;
import java.util.ArrayList;

import mobi.app4mob.quartzclub.adapter.ListMagasinsAdapter;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.Magasin;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.utils.GPSTracker;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;
import mobi.app4mob.quartzclub.utils.SessionManager;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class ListMagasinActivity extends Activity implements
		OnRefreshListener<ListView>, OnItemClickListener {
	Activity activity;
	private PullToRefreshListView mListView;
	private int position_selected;
	private final int NB_PRODUCT_IN_PAGE = 30;
	private ArrayList<Magasin> arrayMagasins;
	private ListMagasinsAdapter mAdapter;
	private int page = 1;
	public static User user;

	private Location myLocation;
	private double latitude = 0;
	private double longitude = 0;
	private String keyword = "";
	private ArrayList<Produit> liste_produits;
	SessionManager session;

	GPSTracker gps;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.list_magasin);

		Bundle bn = new Bundle();
		bn = getIntent().getExtras();
		user = (User) bn.getSerializable("user");
		liste_produits = (ArrayList<Produit>) bn.getSerializable("produit");
		activity = this;

		gps = new GPSTracker(activity);
		gps.checkGps();
		initLatLong();
		initViews();

	}


	private void initLatLong() {
		if (gps.canGetLocation()) {
			latitude = gps.getLatitude();
			longitude = gps.getLongitude();
		} else {
			Functions.LOGE("ERROR",
					"Nous n'avons pas pu localiser votre emplacement actuel.");
			new MyToast(getApplicationContext(),
					getString(R.string.pas_pu_localiser_emplacement),
					Toast.LENGTH_SHORT);
		}
	}

	@SuppressWarnings("deprecation")
	private void initViews() {
		mListView = (PullToRefreshListView) findViewById(R.id.my_list_view);
		mListView
				.setPullLabel(getString(R.string.my_pull_to_refresh_pull_label));
		mListView
				.setReleaseLabel(getString(R.string.my_pull_to_refresh_release_label));
		mListView
				.setRefreshingLabel(getString(R.string.my_pull_to_refresh_refreshing_label));
		mListView.setOnItemClickListener(this);

		((EditText)findViewById(R.id.edit_keyword)).setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					// Do you job here which you want to done through event
					keyword = ((EditText) findViewById(R.id.edit_keyword)).getText()
							.toString().trim();
					page = 1;
					loadMagasin();
				}
				return false;
			}
		});

		loadMagasin();

	}

	public void searchKeywords(View v) {

		keyword = ((EditText) findViewById(R.id.edit_keyword)).getText()
				.toString().trim();
		page = 1;
		loadMagasin();
	}

	public void goToMapView(View v) {
		Intent intent = new Intent(ListMagasinActivity.this, MapActivity.class);
		Bundle b = new Bundle();

		// for one instance
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		b.putSerializable("user", user);
		b.putSerializable("produit", liste_produits);

		intent.putExtras(b);

		startActivity(intent);

	}

	public void goToScanView(View v) {
		Intent intent = new Intent(ListMagasinActivity.this, ScanMagasinVenteActivity.class);
		Bundle b = new Bundle();

		// for one instance
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		b.putSerializable("user", user);
		b.putSerializable("produit", liste_produits);

		intent.putExtras(b);

		startActivity(intent);

	}

	private void loadMagasin() {
		ProgressTraitment progress = new ProgressTraitment(this) {

			ArrayList<Magasin> tempMagasins;

			@Override
			public void onLoadStarted() {
				if (longitude > 0 && latitude > 0) {
					tempMagasins = new Magasin().getMagasin(
							Constants.MAGASIN_WS_URL, user.getId(), longitude,
							latitude, keyword, page);
				}

			}

			@Override
			public void onLoadFinished() {
				if (longitude > 0 && latitude > 0) {
					mListView.onRefreshComplete();

					if (tempMagasins != null && tempMagasins.size() > 0) {

						if (tempMagasins.size() < NB_PRODUCT_IN_PAGE) {
							mListView
									.setOnRefreshListener((OnRefreshListener<ListView>) null);
							mListView.setMode(Mode.DISABLED);
						} else {
							mListView
									.setOnRefreshListener(ListMagasinActivity.this);
							mListView.setMode(Mode.PULL_FROM_END);
						}

						if (page == 1) {
							arrayMagasins = tempMagasins;

							mAdapter = new ListMagasinsAdapter(
									ListMagasinActivity.this, arrayMagasins);
							mListView.setAdapter(mAdapter);

						} else {
							for (int i = 0; i < tempMagasins.size(); i++) {
								arrayMagasins.add(tempMagasins.get(i));
							}

							mAdapter.notifyDataSetChanged();

						}

					} else if (tempMagasins != null && tempMagasins.size() == 0) {
						if (page == 1) {
							arrayMagasins = tempMagasins;
						}

						mListView
								.setOnRefreshListener((OnRefreshListener<ListView>) null);
						mListView.setMode(Mode.DISABLED);

						if (arrayMagasins != null && arrayMagasins.size() == 0) {
							mListView.setAdapter(null);
							new MyToast(ListMagasinActivity.this,
									getString(R.string.no_magasin),
									Toast.LENGTH_SHORT);
						}

					} else {

						if (page > 1) {
							page--;
						}

						new MyToast(ListMagasinActivity.this,
								getString(R.string.error_loading),
								Toast.LENGTH_SHORT);

					}
				} else {
					new MyToast(
							getApplicationContext(),
							getString(R.string.pas_pu_localiser_emplacement),
							Toast.LENGTH_SHORT);
				}
			}

		};

		progress.DoTraitemnt("", "");
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

		int position = arg2 > 0 ? (arg2 - 1) : 0;

		position_selected = position;

		Intent intent = new Intent(ListMagasinActivity.this,
				DetailMagasinActivity.class);
		Bundle b = new Bundle();
		b.putSerializable("magasin", arrayMagasins.get(position_selected));
		b.putSerializable("produit", liste_produits);

		b.putSerializable("user", user);
		intent.putExtra("list",true);
		intent.putExtras(b);

		startActivity(intent);
	}

	public void refreshList(View v) {
		gps = new GPSTracker(activity);
		gps.checkGps();
		initLatLong();
		keyword = "";
		TextView keyword_ = (TextView) findViewById(R.id.edit_keyword);
		keyword_.setText("");
		page = 1;
		loadMagasin();
	}

	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		page++;
		loadMagasin();

	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(activity, MenuActivity.class);
		Bundle b = new Bundle();

		b.putSerializable("user", user);
		b.putSerializable("produit", liste_produits);

		intent.putExtras(b);
		// for one instence
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		finish();
		super.onBackPressed();
	}



}
