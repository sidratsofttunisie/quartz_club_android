package mobi.app4mob.quartzclub.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dm.zbar.android.scanner.ZBarConstants;
import com.handmark.pulltorefresh.library.PullToRefreshBase;

import java.io.Serializable;
import java.util.ArrayList;

import mobi.app4mob.quartzclub.adapter.ListMagasinsAdapter;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.MagInfo;
import mobi.app4mob.quartzclub.model.Magasin;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.MagasinInfoParser;
import mobi.app4mob.quartzclub.utils.GPSTracker;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;
import mobi.app4mob.quartzclub.utils.SessionManager;
import mobi.app4mob.quartzclub.zxing.ScalingScannerActivity;

import static mobi.app4mob.quartzclub.activity.AddMagasinActivity.SCAN_RESULT;

public class ScanMagasinVenteActivity extends BaseActivity {

    private User user;
    private Magasin magasin;
    private static final int ZBAR_QR_SCANNER_REQUEST = 1;
    private MagInfo magInfo;
    private Button mapView;
    private Button listView;
    private ArrayList<Produit> liste_produits;
    GPSTracker gps;
    private double latitude = 0;
    private double longitude = 0;
    ArrayList<Magasin> tempMagasins = new ArrayList<>();
    Boolean carte=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mapView = (Button) findViewById(R.id.btn_action_bar);
        listView = (Button) findViewById(R.id.ab_list_magasin);
        listView.setVisibility(View.VISIBLE);
        mapView.setVisibility(View.VISIBLE);
        listView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getToListMagasin();
            }
        });
        Bundle bn = new Bundle();
        bn = getIntent().getExtras();
        carte = bn.getBoolean("carte",false);
        user = (User) bn.getSerializable("user");
        liste_produits = (ArrayList<Produit>) bn.getSerializable("produit");

        gps = new GPSTracker(this);
        gps.checkGps();
        initLatLong();
        loadMagasin();

    }

    public void scanCodeMagasin(View v) {
        goScan();
    }


    public boolean isCameraAvailable() {
        PackageManager pm = getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    private void goScan() {
        if (isCameraAvailable()) {
            Intent intent = new Intent(this, ScalingScannerActivity.class);
            intent.putExtra("type_profil",1);
            startActivityForResult(intent, ZBAR_QR_SCANNER_REQUEST);
        } else {

            Toast.makeText(this, getString(R.string.camera_indisponible),
                    Toast.LENGTH_SHORT).show();

        }
    }
    String tag="";
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ZBAR_QR_SCANNER_REQUEST:
                if (resultCode == RESULT_OK) {
                    tag = data.getStringExtra(SCAN_RESULT);
                    //((EditText) findViewById(R.id.edit_tag_magasin)).setText(data.getStringExtra(SCAN_RESULT));

                    verifyTag();
                } else if (resultCode == RESULT_CANCELED && data != null) {
                    String error = data.getStringExtra(ZBarConstants.ERROR_INFO);
                    if (!TextUtils.isEmpty(error)) {
                        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    private Magasin findMagasinByRef(String ref) {



        for (Magasin m : tempMagasins
                ) {
            if (m.getTag_magasin().equals(ref)) {
                return m;
            }
        }
            return null;
    }

    public void verifyTag() {

        String tag_mag =tag;
        //String tag_mag = ((EditText) findViewById(R.id.edit_tag_magasin)).getText().toString().trim();

        if (tag_mag.length() > 0) {

            Magasin mag = findMagasinByRef(tag_mag);
            if (mag != null) {
                Intent intent;
                Bundle b = new Bundle();
                if (carte){
                    intent =  new Intent (this,ScanProductActivity.class);
                    intent.putExtra("carte",true);
                }else{
                    intent =  new Intent (this,DetailMagasinActivity.class);
                }
                // si il existe une liste de produit dans la command => go to
                // command
                /*if (AddCommandActivity.list_command_product.size() > 0) {
                    intent = new Intent(this,
                            AddCommandActivity.class);
                    AddCommandActivity.my_first_time = false;
                }
                // si la liste de command est vide ouvrir directement le cam
                else {*/

                    /*intent = new Intent(this,
                            ScanProductActivity.class);
                    ScanProductActivity.my_first_time = true;
                    b.putBoolean("openTheCam", true);*/
                /*}*/

                AddCommandActivity.list_command_product.clear();
                b.putSerializable("magasin", mag);
                b.putSerializable("user", user);
                b.putSerializable("produit", liste_produits);
                b.putString("from_activity", Constants.Extra.SCAN_TAG_MAG);

                intent.putExtras(b);

                startActivity(intent);
            } else {
                /*new MyToast(ScanMagasinVenteActivity.this,
                        getString(R.string.no_exist_magasin),
                        Toast.LENGTH_SHORT);*/
                Toast.makeText(ScanMagasinVenteActivity.this,R.string.no_exist_magasin,Toast.LENGTH_LONG).show();
            }
        } else {

            new MyToast(getApplicationContext(), R.string.invalid_tag_magasin,
                    Toast.LENGTH_SHORT);
        }

    }

    public void getToListMagasin() {

        Intent intent = new Intent(this,
                ListMagasinActivity.class);
        //for one instence
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        Bundle b = new Bundle();
        b.putSerializable("user", (Serializable) user);
        b.putSerializable("produit", (Serializable) liste_produits);

        intent.putExtras(b);
        finish();
        startActivity(intent);
    }

    public void goToMapView(View v) {
        Intent intent = new Intent(this, MapActivity.class);
        Bundle b = new Bundle();

        // for one instance
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        b.putSerializable("user", (Serializable) user);
        b.putSerializable("produit", (Serializable) liste_produits);

        intent.putExtras(b);

        startActivity(intent);

    }

    private void initLatLong() {
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        } else {
            Functions.LOGE("ERROR",
                    getString(R.string.pas_pu_localiser_emplacement));
            new MyToast(getApplicationContext(),
                    getString(R.string.pas_pu_localiser_emplacement),
                    Toast.LENGTH_SHORT);
        }
    }

    private void loadMagasin() {
        ProgressTraitment progress = new ProgressTraitment(this) {

            @Override
            public void onLoadStarted() {
                if (longitude > 0 && latitude > 0) {
                    tempMagasins = new Magasin().getMagasin(
                            Constants.MAGASIN_WS_URL, user.getId(), longitude,
                            latitude, "", -1);
                }

            }

            @Override
            public void onLoadFinished() {
                //System.out.println(">>>>>>>>"+tempMagasins.size()+">>>>>>>");
                if (longitude > 0 && latitude > 0) {
                    if (tempMagasins != null) {
                        if (tempMagasins.size() == 0) {
                            new MyToast(ScanMagasinVenteActivity.this,
                                    getString(R.string.no_magasin),
                                    Toast.LENGTH_SHORT);
                        }
                    } else {

                        new MyToast(ScanMagasinVenteActivity.this,
                                getString(R.string.error_loading),
                                Toast.LENGTH_SHORT);

                    }
                } else {
                    new MyToast(
                            getApplicationContext(),
                            getString(R.string.pas_pu_localiser_emplacement),
                            Toast.LENGTH_SHORT);
                }
            }

        };

        progress.DoTraitemnt("", "");
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_scan_magasin_vente;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MenuActivity.class);
        Bundle b = new Bundle();

        b.putSerializable("user", user);
        b.putSerializable("produit", liste_produits);

        intent.putExtras(b);
        // for one instence
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }
}
