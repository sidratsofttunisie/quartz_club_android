package mobi.app4mob.quartzclub.activity;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import java.util.ArrayList;


import mobi.app4mob.quartzclub.adapter.ExpandableListAdapter;
import mobi.app4mob.quartzclub.adapter.HistoriqueAdapter;
import mobi.app4mob.quartzclub.adapter.ListGiftsAdapter;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.Commande;
import mobi.app4mob.quartzclub.model.Gift;
import mobi.app4mob.quartzclub.model.GroupHistorique;
import mobi.app4mob.quartzclub.model.Historique;
import mobi.app4mob.quartzclub.model.History;
import mobi.app4mob.quartzclub.model.Magasin;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.GiftDataParser;
import mobi.app4mob.quartzclub.parser.HistoriqueDataParser;
import mobi.app4mob.quartzclub.parser.HistoriqueGroupDataParser;
import mobi.app4mob.quartzclub.utils.AlertMessage;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;
import mobi.app4mob.quartzclub.utils.SessionManager;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class HistoryActivity extends BaseActivity implements
        PullToRefreshBase.OnRefreshListener<ListView> {
    Activity activity;
    private ExpandableListView mListView;
    private int position_selected;
    private final int NB_PRODUCT_IN_PAGE = 30;
    private ArrayList<Commande> arrayHistoriques;
    private ExpandableListAdapter mAdapter;
    private int page = 1;
    public User user;
    int type_history;
    int magasin_id = 0;
    private SessionManager session;

    public static final int COMMERCIALHISTORY = 1;
    public static final int MAGASINHISTORY = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.list_history);
        activity = this;

        Bundle bn = new Bundle();
        bn = getIntent().getExtras();

        type_history = bn.getInt("type_history");
        if (bn.containsKey("magasin_id")) {
            magasin_id = bn.getInt("magasin_id");
            super.mTopToolbar.setBackgroundColor(getResources().getColor(R.color.color_rose));
        }

        session = new SessionManager(activity);
        user = session.getUserDetails();
        initViews();

    }
    @Override
    protected int getLayoutResourceId() {
        return R.layout.list_history;
    }

    @SuppressWarnings("deprecation")
    private void initViews() {
        mListView = (ExpandableListView) findViewById(R.id.exp_list);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;
        mListView.setIndicatorBounds(width-GetDipsFromPixel(32), width-GetDipsFromPixel(5));


        TextView action_bar_title = (TextView) findViewById(R.id.txt_action_bar_title);
        action_bar_title.setText(getString(R.string.historique));

        if (type_history == MAGASINHISTORY) {
            super.mTopToolbar.setBackgroundColor(getResources().getColor(R.color.color_rose));
//            ((LinearLayout) findViewById(R.id.header_separator)).setBackgroundColor(Color.parseColor(getString(R.color.color_red_b)));
        }

        loadGift();

    }
    //Convert pixel to dip
    public int GetDipsFromPixel(float pixels)
    {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }
    private void loadGift() {
        ProgressTraitment progress = new ProgressTraitment(this) {

            ArrayList<Commande> tempHistoriques;

            @Override
            public void onLoadStarted() {

                String url = Constants.HISTORIQUE_WS_URL + "?user_id=0";
                if (type_history == MAGASINHISTORY)
                    url = Constants.HISTORIQUE_WS_URL + "?magasin_id="+ magasin_id;
                           // + magasin_id+"&grouped_by=2";
                if (type_history == COMMERCIALHISTORY)
                    url = Constants.HISTORIQUE_WS_URL + "?commercial_id="
                            + user.getId();

                tempHistoriques = new HistoriqueGroupDataParser().getHistorique(url);

            }

            @Override
            public void onLoadFinished() {


                if (tempHistoriques != null && tempHistoriques.size() > 0) {



                    arrayHistoriques = tempHistoriques;

                    mAdapter = new ExpandableListAdapter(HistoryActivity.this,
                            tempHistoriques, type_history);
                    mListView.setAdapter(mAdapter);
                    mListView.expandGroup(0);


                } else if (tempHistoriques != null
                        && tempHistoriques.size() == 0) {




                    if (arrayHistoriques != null
                            && arrayHistoriques.size() == 0) {

                        new MyToast(HistoryActivity.this,
                                getString(R.string.no_history),
                                Toast.LENGTH_SHORT);
                    }

                } else {


                    new MyToast(HistoryActivity.this,
                            getString(R.string.error_loading),
                            Toast.LENGTH_SHORT);

                }

            }

        };

        progress.DoTraitemnt("", "");
    }

    @Override
    public void onRefresh(PullToRefreshBase<ListView> refreshView) {
        page++;
        loadGift();

    }

/*	public void transform(ArrayList<Historique> tempHistoriques){
		ArrayList<Group> groups;
		for (Historique historique : tempHistoriques) {
			Group group;
			group = new Group();
			group.date = historique.getCreated_at();
			group.historique = historique;
		}
	}

	public class Group{
		public String date;
		public Historique historique;
	}*/

}
