package mobi.app4mob.quartzclub.activity;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import mobi.app4mob.quartzclub.adapter.BonusPagerAdapter;
import mobi.app4mob.quartzclub.adapter.ExpandableBonusListAdapter;
import mobi.app4mob.quartzclub.adapter.ExpandableListAdapter;
import mobi.app4mob.quartzclub.adapter.PromosPagerAdapter;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.model.Bonus;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.Promo;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.BonusDataParser;
import mobi.app4mob.quartzclub.parser.PromotionDataParser;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;

public class BonusProdNPackActivity extends BaseActivity {


    private Context context;
    private ArrayList<Bonus> arrayPromos;

    private ViewPager viewPager;
    private Button btnmp;
    private Button btnml;
    private ExpandableListView mListView;
    private LinearLayout ModeListLayout;
    private LinearLayout ModeProduitLayout;
    private PagerAdapter adapter;
    public User user = null;
    public int type_bonus;
    public int type_profil;
    public static ArrayList<Produit> liste_produits;
    private ExpandableBonusListAdapter mAdapter;

    private static final int BONUSPRODUIT = 1;
    private static final int BONUSPACK = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;
        TextView action_bar_title = (TextView) findViewById(R.id.txt_action_bar_title);
        LinearLayout toggleLayout = (LinearLayout) findViewById(R.id.toggle_layout);

        btnmp = (Button) findViewById(R.id.mode_produit);
        btnml = (Button) findViewById(R.id.mode_list);

        Bundle bn = new Bundle();
        bn = getIntent().getExtras();
        user = (User) bn.getSerializable("user");
        liste_produits = (ArrayList<Produit>) bn.getSerializable("produit");
        type_bonus = bn.getInt("type_bonus");
        type_profil = bn.getInt("type_profil");
        System.out.println(">>>>>>>BNP>>>>>>" + liste_produits.size());

        if (bn.getInt("type_profil") == ListBonusActivity.COMMERCIALBONUS) {
            super.mTopToolbar.setBackgroundColor(getResources().getColor(R.color.color_bleu_marine));
            action_bar_title.setTextColor(getResources().getColor(R.color.color_white));
            toggleLayout.setBackgroundColor(getResources().getColor(R.color.color_bleu_marine));
            btnmp.setBackground(getResources().getDrawable(R.drawable.leftsideroundedbtn));
            btnml.setBackground(getResources().getDrawable(R.drawable.rightsideroundedbtn));
            btnmp.setTextColor(getResources().getColor(R.color.color_bleu_marine));
            btnml.setTextColor(getResources().getColor(R.color.color_white));
        }else {
            super.mTopToolbar.setBackgroundColor(getResources().getColor(R.color.color_rose));
            action_bar_title.setTextColor(getResources().getColor(R.color.color_white));
            toggleLayout.setBackgroundColor(getResources().getColor(R.color.color_rose));
            btnmp.setBackground(getResources().getDrawable(R.drawable.leftsideroundedbtn));
            btnml.setBackground(getResources().getDrawable(R.drawable.rightsidenotclicked));
            btnmp.setTextColor(getResources().getColor(R.color.color_rose));
            btnml.setTextColor(getResources().getColor(R.color.color_white));
        }


        if (bn.getInt("type_bonus") == BonusProdNPackActivity.BONUSPRODUIT) {
            if (bn.getInt("type_profil") == ListBonusActivity.COMMERCIALBONUS) {
                action_bar_title.setText(R.string.list_bonus_commercial_title);
            } else {
                action_bar_title.setText(R.string.list_bonus_magasin_title);
            }
        }else{
            if (bn.getInt("type_profil") == ListBonusActivity.COMMERCIALBONUS) {
                action_bar_title.setText(R.string.list_pack_commercial_title);
            } else {
                action_bar_title.setText(R.string.list_pack_magasin_title);
            }
        }

        loadBonus();
        initViews();
    }

    private void initViews() {

        ModeProduitLayout = (LinearLayout) findViewById(R.id.ProduitView);
        ModeListLayout = (LinearLayout) findViewById(R.id.ListProduitView);

        btnmp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (type_profil == ListBonusActivity.COMMERCIALBONUS) {
                    btnmp.setBackground(getResources().getDrawable(R.drawable.leftsideroundedbtn));
                    btnml.setBackground(getResources().getDrawable(R.drawable.rightsideroundedbtn));

                    btnmp.setTextColor(getResources().getColor(R.color.color_blue));
                    btnml.setTextColor(getResources().getColor(R.color.color_white));
                }else{
                    btnmp.setBackground(getResources().getDrawable(R.drawable.leftsideroundedbtn));
                    btnml.setBackground(getResources().getDrawable(R.drawable.rightsidenotclicked));

                    btnmp.setTextColor(getResources().getColor(R.color.color_rose));
                    btnml.setTextColor(getResources().getColor(R.color.color_white));
                }
                ModeListLayout.setVisibility(View.VISIBLE);
                ModeProduitLayout.setVisibility(View.GONE);
            }
        });

        btnml.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type_profil == ListBonusActivity.COMMERCIALBONUS) {
                    btnmp.setBackground(getResources().getDrawable(R.drawable.leftsidenotclicked));
                    btnml.setBackground(getResources().getDrawable(R.drawable.rightsideclicked));

                    btnmp.setTextColor(getResources().getColor(R.color.color_white));
                    btnml.setTextColor(getResources().getColor(R.color.color_blue));
                }else{
                    btnmp.setBackground(getResources().getDrawable(R.drawable.leftsidenotclickedmagasin));
                    btnml.setBackground(getResources().getDrawable(R.drawable.rightsideclicked));

                    btnmp.setTextColor(getResources().getColor(R.color.color_white));
                    btnml.setTextColor(getResources().getColor(R.color.color_rose));
                }

                ModeListLayout.setVisibility(View.GONE);
                ModeProduitLayout.setVisibility(View.VISIBLE);
            }
        });


    }

    public int GetDipsFromPixel(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_bonus_prod_npack;
    }

    private void loadBonus() {
        ProgressTraitment progress = new ProgressTraitment(context) {

            @Override
            public void onLoadStarted() {

                if (type_bonus == BONUSPRODUIT)
                    arrayPromos = new BonusDataParser().getBonus(Constants.BONUSnPACK_WS_URL + "?user_id=" + user.getId());
                else
                    arrayPromos = new BonusDataParser().getBonus(Constants.BONUSnPACK_WS_URL + "?user_id=" + user.getId() + "&is_package=1");

            }

            @Override
            public void onLoadFinished() {
                if (arrayPromos != null) {
                    if (arrayPromos.size() > 0) {


                        //pager view
                        viewPager = (ViewPager) findViewById(R.id.pager);
                        adapter = new BonusPagerAdapter(arrayPromos, context, type_profil);
                        viewPager.setAdapter(adapter);

                        CirclePageIndicator circleIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
                        circleIndicator.setViewPager(viewPager);

                        //expandable list
                        mListView = (ExpandableListView) findViewById(R.id.exp_list);
                        DisplayMetrics metrics = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(metrics);
                        int width = metrics.widthPixels;
                        mListView.setIndicatorBounds(width - GetDipsFromPixel(32), width - GetDipsFromPixel(5));
                        mAdapter = new ExpandableBonusListAdapter(context, arrayPromos, type_profil);
                        mListView.setAdapter(mAdapter);

                    } else {
                        new MyToast(context, " 0 Bonus", Toast.LENGTH_SHORT);
                    }
                } else {
                    new MyToast(context, getString(R.string.error_loading),
                            Toast.LENGTH_SHORT);
                }

            }

        };

        progress.DoTraitemnt("", "");

    }
}
