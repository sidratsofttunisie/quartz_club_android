package mobi.app4mob.quartzclub.activity;

import java.io.Serializable;
import java.util.ArrayList;

import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.constants.Constants.Extra;
import mobi.app4mob.quartzclub.model.CommandProduct;
import mobi.app4mob.quartzclub.model.Magasin;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.utils.AlertMessage;
import ExpandableListAdapter.ExpandableListAdapter;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ExpandableListView;

public class AddCommandActivity extends Activity {

    private User user;
    private Magasin magasin;
    public static ArrayList<CommandProduct> list_command_product = new ArrayList<CommandProduct>();

    public static Shader txtShad;
    Activity activity;

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    private ArrayList<Produit> liste_produits;

    // pour le retoure de l'activty reste une fois
    public static Boolean my_first_time = true;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_command);

        Bundle bn = new Bundle();
        bn = getIntent().getExtras();
        user = (User) bn.getSerializable("user");
        magasin = (Magasin) bn.getSerializable("magasin");
        liste_produits = (ArrayList<Produit>) bn.getSerializable("produit");

        if (bn.getString("from_activity").equals(Extra.SCAN_CODE_PRODUCT)
                || bn.getString("from_activity")
                .equals(Extra.SCAN_CODE_FACTURE)) {
            list_command_product = (ArrayList<CommandProduct>) bn
                    .getSerializable("list_command");
        }

        activity = this;

        initViews();

    }

    public void goTonNumFacture(View v) {
        if (list_command_product != null && list_command_product.size() > 0) {
            Intent intent = new Intent(activity, SetNumFactureActivity.class);
            Bundle b = new Bundle();
            b.putSerializable("magasin", magasin);
            b.putSerializable("user", user);
            b.putSerializable("produit", liste_produits);
            b.putSerializable("list_command", list_command_product);

            intent.putExtras(b);

            startActivity(intent);
        } else {
            showMessageNoProduct();
        }
    }

    public void deleteProduct(View v) {
        // final int position = (int) ((ExpandableListView)
        // v.getParent().getParent().getParent()).getPositionForView(view)();

        final int position = expListView.getPositionForView((View) v
                .getParent().getParent());
        final int size_of_expList = expListView.getCount();
        if (position != AdapterView.INVALID_POSITION
                && size_of_expList > position) {
            ArrayList<String> list_code_to_deleted = listAdapter.getGroup(
                    position).getListe_code();
            list_code_to_deleted.remove(list_code_to_deleted.size() - 1);
            list_command_product.get(position).setListe_code(
                    list_code_to_deleted);
            if (list_command_product.get(position).getListe_code().size() == 0) {
                list_command_product.remove(position);
            }
            listAdapter.notifyDataSetChanged();
        } else {
            Log.e("****", "size of expl = " + size_of_expList + " position = "
                    + position);
        }
    }

    public void deleteCommand(View v) {
        AlertMessage dialogDeleteCommand = new AlertMessage(this) {
            @Override
            public void onOk() {
                list_command_product = new ArrayList<CommandProduct>();
                AddCommandActivity.list_command_product = new ArrayList<CommandProduct>();
                Intent intent = new Intent(activity, HomeActivity.class);
                Bundle b = new Bundle();
                b.putSerializable("user", user);
                b.putSerializable("produit", liste_produits);
                b.putSerializable("list_command", list_command_product);
                intent.putExtras(b);
                finish();
                startActivity(intent);

            }

            @Override
            public void onCancel() {
                Functions.LOGI("*****", "on cancle");
            }
        };
        dialogDeleteCommand.show2(
                getString(R.string.alert_cancel_command_message),
                getString(R.string.alert_cancel_command_title), false,
                getString(R.string.valider), getString(R.string.annuler));
    }

    public void goToScanProduct(View v) {
        Intent intent = new Intent(activity, ScanProductActivity.class);
        Bundle b = new Bundle();

        b.putSerializable("magasin", magasin);
        b.putSerializable("user", user);
        b.putSerializable("produit", liste_produits);
        b.putBoolean("openTheCam", true);
        b.putSerializable("list_command", list_command_product);
        b.putString("from_activity", Extra.LIST_COMMAND_PRODUCT);

        intent.putExtras(b);

		/*
         * if (AddCommandActivity.my_first_time) {
		 * AddCommandActivity.my_first_time = false; } else { finish(); }
		 */

        startActivity(intent);

    }

    private void initViews() {
        ((Button) findViewById(R.id.ab_add_product_to_command))
                .setVisibility(View.VISIBLE);

        ((Button) findViewById(R.id.ab_sup_command))
                .setVisibility(View.VISIBLE);

        // get the listview
        expListView = (ExpandableListView) findViewById(R.id.lvExp);
        // preparing list data

        if (list_command_product != null) {
            listAdapter = new ExpandableListAdapter(this, list_command_product);

            // setting list adapter
            expListView.setAdapter(listAdapter);

            expListView.setGroupIndicator(null);
            expListView.setChildIndicator(null);
            expListView.setChildDivider(getResources().getDrawable(
                    R.color.color_gris_clair));

            txtShad = new LinearGradient(0, 0, 0, 100, new int[]{
                    getResources().getColor(R.color.color_gold_clair),
                    getResources().getColor(R.color.color_gold)}, new float[]{
                    0, 1}, Shader.TileMode.CLAMP);

        }

    }

    private void showMessageNoProduct() {
        AlertMessage connexionFailed = new AlertMessage(activity) {
            @Override
            public void onOk() {

            }

            @Override
            public void onCancel() {

            }
        };

        connexionFailed.show1(getString(R.string.alert_no_product_in_list), getString(R.string.quartz_club),
                false, getString(R.string.valider));

    }


    @Override
    public void onBackPressed() {

        Intent intent = new Intent(activity, DetailMagasinActivity.class);
        Bundle b = new Bundle();

        b.putSerializable("magasin", magasin);
        b.putSerializable("user", user);
        b.putSerializable("produit", liste_produits);
        // b.putString("from_activity", Extra.LIST_COMMAND_PRODUCT);

        intent.putExtras(b);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("openScanner", true);
        editor.commit();

        startActivity(intent);
        finish();
        super.onBackPressed();
    }

}
