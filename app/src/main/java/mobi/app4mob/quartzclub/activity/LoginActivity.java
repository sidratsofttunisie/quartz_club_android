package mobi.app4mob.quartzclub.activity;

import java.io.Serializable;
import java.util.ArrayList;


import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.UserDataParser;
import mobi.app4mob.quartzclub.utils.EmailValidator;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.SessionManager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;


public class LoginActivity extends AppCompatActivity {
    private EditText emailEdit;
    private EditText passwordEdit;
    private ImageView showMeThePassword;
    private static ArrayList<Produit> liste_produits = null;
    public static User user;
    private SharedPreferences prefs;
    String errorMsg;
    Activity activity;
    SessionManager session;
    protected String password;
    boolean firstAsk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.authentification);
        firstAsk = true;
        activity = this;

        // Session Manager
        session = new SessionManager(getApplicationContext());

        Bundle bn = new Bundle();
        bn = getIntent().getExtras();

        if (session.isLoggedIn() && session.withLongSession()) {

            user = session.getUserDetails();
            Intent intent = new Intent(activity, MenuActivity.class);
            Bundle b = new Bundle();
            b.putSerializable("user", user);
            intent.putExtras(b);
            activity.startActivity(intent);
            finish();

        }
        initViews();

    }

    private void initViews() {

        emailEdit = (EditText) findViewById(R.id.editEmail);
        passwordEdit = (EditText) findViewById(R.id.editPassword);


        EditText password = (EditText) findViewById(R.id.editPassword);
        password.setTypeface(Typeface.DEFAULT);
        password.setTransformationMethod(new PasswordTransformationMethod());


        Button btn = (Button) findViewById(R.id.btnConnecte);

        //set espacing between checkbox and label
        CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox);
        final float scale = this.getResources().getDisplayMetrics().density;
        checkBox.setPadding(checkBox.getPaddingLeft() + (int) (10.0f * scale + 0.5f),
                checkBox.getPaddingTop(),
                checkBox.getPaddingRight(),
                checkBox.getPaddingBottom());


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkPermissions();

            }
        });
        showMeThePassword = (ImageView) findViewById(R.id.showMePassword);
        showMeThePassword.setOnTouchListener(mPasswordVisibleTouchListener);
        passwordEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                showMeThePassword.setVisibility(s.length() > 0 ? View.VISIBLE
                        : View.GONE);
            }

        });

        passwordEdit.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                        || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    Log.i("***", "Enter pressed");
                    checkPermissions();
                }
                return false;
            }

        });
    }

    private boolean verifyData() {
        errorMsg = "";
        boolean valide = true;

        String sEmail = emailEdit.getText().toString().trim();
        if (sEmail == null || sEmail.length() == 0) {
            valide = false;

            // emailEdit.setBackgroundDrawable(getResources().getDrawable(R.drawable.textfield_faux_s1));
            errorMsg = errorMsg + getString(R.string.empty_username);

        }

        String sMdp = passwordEdit.getText().toString().trim();
        if (sMdp == null || sMdp.length() == 0) {
            valide = false;
            // passwordEdit.setBackgroundDrawable(getResources().getDrawable(R.drawable.textfield_faux_s1));
            errorMsg = errorMsg + getString(R.string.empty_password_error);

        } else if (sMdp.length() < 4) {
            valide = false;
            // passwordEdit.setBackgroundDrawable(getResources().getDrawable(R.drawable.textfield_faux_s1));
            errorMsg = errorMsg + getString(R.string.password_error);

        } else {
            // passwordEdit.setBackgroundDrawable(getResources().getDrawable(R.drawable.textfield_s1));
        }

        return valide;
    }


    /**
     * Callback received when a permissions request has been completed.
     */


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {
            boolean all_permission_accepted = true;

            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++) {


                    if (permissions[i].equals(android.Manifest.permission.CAMERA)) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            all_permission_accepted = false;

                        }
                    } else if (permissions[i].equals(android.Manifest.permission.ACCESS_WIFI_STATE)) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            all_permission_accepted = false;

                        }
                    } else if (permissions[i].equals(android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            all_permission_accepted = false;

                        }
                    } else if (permissions[i].equals(android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            all_permission_accepted = false;

                        }
                    } else if (permissions[i].equals(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            all_permission_accepted = false;

                        }
                    } else if (permissions[i].equals(android.Manifest.permission.INTERNET)) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            all_permission_accepted = false;

                        }
                    } else if (permissions[i].equals(android.Manifest.permission.ACCESS_NETWORK_STATE)) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            all_permission_accepted = false;

                        }
                    } else if (permissions[i].equals(android.Manifest.permission.READ_PHONE_STATE)) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            all_permission_accepted = false;

                        }
                    }


                }

            }
            if (!all_permission_accepted) {
                new MyToast(LoginActivity.this, getResources().getString(R.string.accept_all_permission), Toast.LENGTH_SHORT);
            } else {
                authentificate();
            }
        }

    }


    private void checkPermissions() {
        // The request code used in ActivityCompat.requestPermissions()
        // and returned in the Activity's onRequestPermissionsResult()
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {android.Manifest.permission.CAMERA, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.ACCESS_NETWORK_STATE, android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.ACCESS_WIFI_STATE, android.Manifest.permission.INTERNET};

        if (!hasPermissions(this, PERMISSIONS)) {
            System.out.println("permission1");
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        } else {
            System.out.println("haspermission1");
            authentificate();
        }

    }

    public static boolean hasPermissions(Context context, String... permissions) {
        System.out.println("inhaspermission1");
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            System.out.println("if1");
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    private void authentificate() {

        if (verifyData()) {
            ProgressTraitment prog = new ProgressTraitment(this) {

                @Override
                public void onLoadStarted() {
                    String email = emailEdit.getText().toString().trim();
                    password = passwordEdit.getText().toString().trim();
                    user = UserDataParser.authentificate(email, password);

                }

                @Override
                public void onLoadFinished() {
                    if (user != null) {
                        user.setPassword(password);
                        // create new sesssion with this user
                        boolean isChecked = ((CheckBox) findViewById(R.id.checkBox)).isChecked();
                        session.createLoginSession(user, isChecked);

                        Intent intent = new Intent(activity, MenuActivity.class);
                        Bundle b = new Bundle();
                        b.putSerializable("user", user);
                        intent.putExtras(b);
                        activity.startActivity(intent);
                        finish();
                    } else

                    {
                        new MyToast(getApplicationContext(),
                                R.string.error_authentification,
                                Toast.LENGTH_SHORT);

                        passwordEdit.setText("");

                        Log.v("main activity",
                                getString(R.string.error_authentification));
                    }

                }
            };
            prog.DoTraitemnt("", "");
        } else {
            // Toast.makeText(this, errorMsg, Toast.LENGTH_SHORT).show();
            // showMessage(Constants.FAILED_DATA);
            new MyToast(this, getString(R.string.failed_data),
                    Toast.LENGTH_SHORT);
            Log.v("main activity", getString(R.string.failed_data));
        }
    }

    public void forgotPassword(View v) {
        Intent intent = new Intent(activity, ForgotPasswordActivity.class);
        Bundle b = new Bundle();

        intent.putExtras(b);
        activity.startActivity(intent);
    }


    private View.OnTouchListener mPasswordVisibleTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            final boolean isOutsideView = event.getX() < 0
                    || event.getX() > v.getWidth() || event.getY() < 0
                    || event.getY() > v.getHeight();

            // change input type will reset cursor position, so we want to save
            // it
            final int cursor = passwordEdit.getSelectionStart();

            if (isOutsideView || MotionEvent.ACTION_UP == event.getAction())
                passwordEdit.setInputType(InputType.TYPE_CLASS_TEXT
                        | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            else
                passwordEdit.setInputType(InputType.TYPE_CLASS_TEXT
                        | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);

            passwordEdit.setSelection(cursor);
            return true;
        }

    };


}
