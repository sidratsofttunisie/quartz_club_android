package mobi.app4mob.quartzclub.activity;

import java.io.Serializable;
import java.util.ArrayList;

import com.dm.zbar.android.scanner.ZBarConstants;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.Magasin;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.ChangeTagParser;
import mobi.app4mob.quartzclub.parser.RegisterTokenParser;
import mobi.app4mob.quartzclub.utils.AlertMessage;
import mobi.app4mob.quartzclub.utils.GPSTracker;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;
import mobi.app4mob.quartzclub.zxing.ScalingScannerActivity;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static mobi.app4mob.quartzclub.activity.AddMagasinActivity.SCAN_RESULT;

public class DetailMagasinActivity extends FragmentActivity {

    public static Magasin magasin;
    public static User user;
    Activity activity;
    private GoogleMap mMap;
    public static FragmentManager fragmentManager;

    private TextView mag_name;
    private TextView address;
    private TextView tel;
    private TextView fax;
    private EditText tag;
    private Button change;
    private double my_latitude = 0;
    private double my_longitude = 0;
    private ArrayList<Produit> liste_produits;
    GPSTracker gps;

    Boolean carte = false;
    Boolean liste = false;

    @Override
    @SuppressWarnings("unchecked")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_magasin);

        Bundle bn = new Bundle();
        bn = getIntent().getExtras();
        magasin = (Magasin) bn.getSerializable("magasin");
        user = (User) bn.getSerializable("user");
        liste_produits = (ArrayList<Produit>) bn.getSerializable("produit");
        carte = bn.getBoolean("carte", false);
        liste = bn.getBoolean("list", false);
        activity = this;
        gps = new GPSTracker(activity);
        gps.checkGps();
        initViews();
        //Functions.LOGI("***", magasin.getMagasin_name());
        //Functions.LOGI("***user*****", user.getFull_name());

        initLatLong();
        fragmentManager = getSupportFragmentManager();
        setUpMap();
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // changer tag magazin
                goScan();
                /*if (tag.getText().toString().trim().length() < 4) {
					new MyToast(DetailMagasinActivity.this,
							getString(R.string.error_magasin_invalide),
							Toast.LENGTH_SHORT);
					tag.setBackgroundResource(R.drawable.invalid_champ);
				} else {
					if(!magasin.getTag_magasin().equals(tag.getText().toString().trim())) {
						tag.setBackgroundResource(R.drawable.bg_edit_text);
						changeTag(tag.getText().toString().trim());
					}

				}*/
            }
        });

    }

    private void initViews() {
        mag_name = (TextView) findViewById(R.id.text_magasin_name);
        address = (TextView) findViewById(R.id.text_address);
        // reg_com = (TextView) findViewById(R.id.text_register_commerce);
        // cin = (TextView) findViewById(R.id.text_cin);
        tel = (TextView) findViewById(R.id.text_tel);
        fax = (TextView) findViewById(R.id.text_fax);
        //tag = (EditText) findViewById(R.id.text_tag);
        change = (Button) findViewById(R.id.change_tag);
        if (magasin != null) {
            mag_name.setText(magasin.getMagasin_name());

            address.setText(magasin.getAddress());

		/*
         * reg_com.setText(magasin.getRegistre_commerce()); if
		 * (Functions.isStringEmpty(magasin.getTag_client())) ref.setText("--");
		 */

		/*
		 * cin.setText(magasin.getCin()); if
		 * (Functions.isStringEmpty(magasin.getCin())) cin.setText("--");
		 */

            tel.setText(magasin.getTel());
            if (Functions.isStringEmpty(magasin.getTel()))
                tel.setText("--");

            fax.setText(magasin.getFax());
            if (Functions.isStringEmpty(magasin.getFax()))
                fax.setText("--");
/*
        tag.setText(magasin.getTag_magasin());*/
            // if (Functions.isStringEmpty(magasin.getTag_magasin()));
            /*tag.setText("--");*/
        }


    }

    private void goScan() {
        if (isCameraAvailable()) {
            Intent intent = new Intent(this, ScalingScannerActivity.class);
            intent.putExtra("type_profil", 1);
            startActivityForResult(intent, ZBAR_QR_SCANNER_REQUEST);
        } else {

            Toast.makeText(this, getString(R.string.camera_indisponible),
                    Toast.LENGTH_SHORT).show();

        }
    }


    private static final int ZBAR_QR_SCANNER_REQUEST = 1;

    String newTag = "";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ZBAR_QR_SCANNER_REQUEST:
                if (resultCode == RESULT_OK) {
                    newTag = data.getStringExtra(SCAN_RESULT);
                    //((EditText) findViewById(R.id.edit_tag_magasin)).setText(data.getStringExtra(SCAN_RESULT));
                    changeTag(newTag);


                } else if (resultCode == RESULT_CANCELED && data != null) {
                    String error = data.getStringExtra(ZBarConstants.ERROR_INFO);
                    if (!TextUtils.isEmpty(error)) {
                        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    public boolean isCameraAvailable() {
        PackageManager pm = getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    private void changeTag(final String tag) {

        ProgressTraitment progress = new ProgressTraitment(this) {
            String result = null;

            @Override
            public void onLoadStarted() {

                result = new ChangeTagParser().changeTag(Constants.UPDATE_TAG_MAG_WS_URL,
                        magasin.getId(), tag);

            }

            @Override
            public void onLoadFinished() {
                if (result != null && result.equals("success")) {
                    new MyToast(DetailMagasinActivity.this,
                            getString(R.string.tag_update_succes),
                            Toast.LENGTH_SHORT);
                } else {
                    new MyToast(DetailMagasinActivity.this,
                            getString(R.string.register_notif_error),
                            Toast.LENGTH_SHORT);
                }
            }

        };
        progress.DoTraitemnt("", "");
    }

    private void setUpMap() {
        // Do a null check to confirm that we have not already instantiated the
        // map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) DetailMagasinActivity.fragmentManager
                    .findFragmentById(R.id.map)).getMap();
            // mMap = ((NiceSupportMapFragment)
            // getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            // ((NiceSupportMapFragment)
            // getSupportFragmentManager().findFragmentById(R.id.map))
            // .setPreventParentScrolling(true);
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                Boolean f  = true;
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    f = false;
                    return;
                }

                if (f)
                    mMap.setMyLocationEnabled(true);
                if (magasin != null) {
                    mMap.addMarker(new MarkerOptions()
                            .position(
                                    new LatLng(magasin.getLatitude(), magasin
                                            .getLongitude()))
                            .title(magasin.getMagasin_name())
                            .icon(BitmapDescriptorFactory
                                    .fromResource(R.drawable.ic_map_pin_toaster)));
                    mMap.getUiSettings().setScrollGesturesEnabled(false);
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                            magasin.getLatitude(), magasin.getLongitude()), 13));
                }
            }
        }
    }

    private void initLatLong() {
        Location myLocation = Functions
                .getMyLocation(DetailMagasinActivity.this);
        if (myLocation != null) {
            my_latitude = myLocation.getLatitude();
            my_longitude = myLocation.getLongitude();

        }
    }

    public void goToScanMag(View v) {
        Location loc1 = new Location("");
        loc1.setLatitude(my_latitude);
        loc1.setLongitude(my_longitude);
        Location loc2 = new Location("");
        if (magasin != null) {
            loc2.setLatitude(magasin.getLatitude());
            loc2.setLongitude(magasin.getLongitude());
        }
        float distanceInMeters = loc1.distanceTo(loc2);
        Functions.LOGI("distance", "Distance = " + distanceInMeters);
        if (distanceInMeters <= 1000) {
            Intent intent;
            Bundle b = new Bundle();
            if (carte || liste) {
                intent = new Intent(DetailMagasinActivity.this,
                        ScanMagasinVenteActivity.class);
                intent.putExtra("carte", true);
            } else {
                // si il existe une liste de produit dans la command => go to
                // command
                if (AddCommandActivity.list_command_product.size() > 0) {
                    intent = new Intent(DetailMagasinActivity.this,
                            AddCommandActivity.class);
                    AddCommandActivity.my_first_time = false;
                }
                // si la liste de command est vide ouvrir directement le cam
                else {
                    intent = new Intent(DetailMagasinActivity.this,
                            ScanProductActivity.class);
                    ScanProductActivity.my_first_time = true;
                    b.putBoolean("openTheCam", true);
                }
            }


            b.putSerializable("magasin", magasin);
            b.putSerializable("user", user);
            b.putSerializable("produit", liste_produits);
            b.putString("from_activity", Constants.Extra.SCAN_TAG_MAG);

            intent.putExtras(b);

            startActivity(intent);/*
            Intent intent = new Intent(DetailMagasinActivity.this,
                    ScanCarteMagasinActivity.class);
            Bundle b = new Bundle();
            b.putSerializable("magasin", magasin);
            b.putSerializable("produit", liste_produits);
            b.putSerializable("user", user);

            intent.putExtras(b);

            startActivity(intent);*/
        } else {
            AlertMessage dialogAuthentificationFailed = new AlertMessage(this) {
                @Override
                public void onOk() {

                }

                @Override
                public void onCancel() {
                }
            };
            dialogAuthentificationFailed.show1(
                    getString(R.string.alert_check_distance_magasin_message2),
                    getString(R.string.alert_check_distance_magasin_title),
                    false, "Ok");
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(activity, ListMagasinActivity.class);
        Bundle b = new Bundle();

        b.putSerializable("user", user);
        b.putSerializable("produit", liste_produits);
        b.putSerializable("magasin",magasin);
        intent.putExtras(b);
        // for one instence
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        initLatLong();
        super.onResume();
    }

}
