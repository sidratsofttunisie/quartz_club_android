package mobi.app4mob.quartzclub.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import mobi.app4mob.quartzclub.adapter.SlidingImage_Adapter;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.model.Photo;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.Slide;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.CatalogueDataParser;
import mobi.app4mob.quartzclub.parser.RegisterTokenParser;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;

public class SlideActivity extends BaseActivity {
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private User user;
    private static final Integer[] IMAGES = {R.drawable.btn_bonus_annif, R.drawable.btn_bonus_mg, R.drawable.btn_bonus_pack, R.drawable.btn_bonus_palier};
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    private ArrayList<Photo> photos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView action_bar_title = (TextView) findViewById(R.id.txt_action_bar_title);

        Bundle bn = new Bundle();
        bn = getIntent().getExtras();

        photos = (ArrayList<Photo>)bn.getSerializable("photos");
        String title = bn.getString("title");
        action_bar_title.setText(title);

        init();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.layout_slide;
    }

    private void init() {

        mPager = (ViewPager) findViewById(R.id.pager);


        mPager.setAdapter(new SlidingImage_Adapter(SlideActivity.this, photos));


        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = IMAGES.length;

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

}