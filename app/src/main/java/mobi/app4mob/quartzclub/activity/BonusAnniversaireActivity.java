package mobi.app4mob.quartzclub.activity;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.adapter.BonusPagerAdapter;
import mobi.app4mob.quartzclub.adapter.ExpandableBonusListAdapter;
import mobi.app4mob.quartzclub.adapter.ListBonusAnnivAdapter;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.model.Bonus;
import mobi.app4mob.quartzclub.model.BonusAnniv;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.BonusAnnivDataParser;
import mobi.app4mob.quartzclub.parser.BonusDataParser;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;

public class BonusAnniversaireActivity extends BaseActivity {

    public static User user = null;
    public static Produit produit;
    public int type_profil;
    public ListView mListView;
    private ArrayList<BonusAnniv> arrayBonus;
    private ListBonusAnnivAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TextView action_bar_title = (TextView) findViewById(R.id.txt_action_bar_title);
        Bundle bn = new Bundle();
        bn = getIntent().getExtras();
        user = (User) bn.getSerializable("user");
        type_profil = bn.getInt("type_profil");

        if (bn.getInt("type_profil") == ListBonusActivity.COMMERCIALBONUS)
            action_bar_title.setText(R.string.list_bonus_anniv_commercial_title);
        else
            action_bar_title.setText(R.string.list_bonus_anniv_magasin_title);

        initView();

    }

    private void initView() {

        if (type_profil == ListBonusActivity.COMMERCIALBONUS) {
            super.mTopToolbar.setBackgroundColor(getResources().getColor(R.color.color_bleu_marine));
        }else{
            super.mTopToolbar.setBackgroundColor(getResources().getColor(R.color.color_rose));
        }

        ProgressTraitment progress = new ProgressTraitment(this) {

            @Override
            public void onLoadStarted() {

                arrayBonus = new BonusAnnivDataParser().getBonusAnniv(Constants.BONUS_ANNIV_WS_URL + "?user_id=" + user.getId());

            }

            @Override
            public void onLoadFinished() {

                if (arrayBonus != null) {
                    if (arrayBonus.size() > 0) {

                        // list
                        mListView = (ListView) findViewById(R.id.list);
                        mAdapter = new ListBonusAnnivAdapter(BonusAnniversaireActivity.this,
                                arrayBonus, type_profil);
                        mListView.setAdapter(mAdapter);

                    } else {
                        new MyToast(BonusAnniversaireActivity.this, " 0 Bonus", Toast.LENGTH_SHORT);
                    }
                } else {
                    new MyToast(BonusAnniversaireActivity.this, getString(R.string.error_loading),
                            Toast.LENGTH_SHORT);
                }

            }


        };
        progress.DoTraitemnt("", "");
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_anniversaire;
    }
}
