package mobi.app4mob.quartzclub.activity;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.adapter.ListBonusAnnivAdapter;
import mobi.app4mob.quartzclub.adapter.ListBonusPalierAdapter;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.model.BonusAnniv;
import mobi.app4mob.quartzclub.model.BonusPalier;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.BonusAnnivDataParser;
import mobi.app4mob.quartzclub.parser.BonusPalierDataParser;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;

public class BonusPalierActivity extends BaseActivity {

    public static User user = null;
    public static Produit produit;
    public int type_profil;
    public ListView mListView;
    private ArrayList<BonusPalier> arrayBonus;
    private ListBonusPalierAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView action_bar_title = (TextView) findViewById(R.id.txt_action_bar_title);
        Bundle bn = new Bundle();
        bn = getIntent().getExtras();
        user = (User) bn.getSerializable("user");
        type_profil = bn.getInt("type_profil");

        if (bn.getInt("type_profil") == ListBonusActivity.COMMERCIALBONUS)
            action_bar_title.setText(R.string.list_bonus_palier_commercial_title);
        else
            action_bar_title.setText(R.string.list_bonus_palier_magasin_title);

        initView();

    }

    private void initView() {

        String strPointsFormat = getResources().getString(R.string.bonusPalier_points_gagner);

        SpannableString str = new SpannableString(strPointsFormat);
        int index = str.toString().indexOf("@");
        Drawable d = getResources().getDrawable(R.drawable.goute_liste);
        d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
        ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BOTTOM);
        str.setSpan(span, index, index + 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ((TextView)findViewById(R.id.points_gagner)).setText(str);

        if (type_profil == ListBonusActivity.COMMERCIALBONUS) {
            super.mTopToolbar.setBackgroundColor(getResources().getColor(R.color.color_bleu_marine));
        }else{
            super.mTopToolbar.setBackgroundColor(getResources().getColor(R.color.color_rose));
        }



        ProgressTraitment progress = new ProgressTraitment(this) {

            @Override
            public void onLoadStarted() {

                arrayBonus = new BonusPalierDataParser().getBonusPalier(Constants.BONUS_PALIER_WS_URL + "?user_id=" + user.getId());

            }

            @Override
            public void onLoadFinished() {

                if (arrayBonus != null) {
                    if (arrayBonus.size() > 0) {

                        // list
                        mListView = (ListView) findViewById(R.id.list);
                        mAdapter = new ListBonusPalierAdapter(BonusPalierActivity.this,
                                arrayBonus, type_profil);
                        mListView.setAdapter(mAdapter);

                    } else {
                        new MyToast(BonusPalierActivity.this, " 0 Bonus", Toast.LENGTH_SHORT);
                    }
                } else {
                    new MyToast(BonusPalierActivity.this, getString(R.string.error_loading),
                            Toast.LENGTH_SHORT);
                }

            }


        };
        progress.DoTraitemnt("", "");
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_bonus_palier;
    }
}
