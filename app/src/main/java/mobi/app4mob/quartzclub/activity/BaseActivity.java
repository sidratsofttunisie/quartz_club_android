package mobi.app4mob.quartzclub.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import android.support.v7.widget.PopupMenu;

import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import mobi.app4mob.quartzclub.utils.SessionManager;

public abstract class BaseActivity extends AppCompatActivity {

    public SessionManager session;
    public Toolbar mTopToolbar;
    private IProfile profile;
    private static IDrawerItem activeItem = null;
    private AccountHeader headerResult = null;
    private Drawer result = null;
    private static final int PROFILE_SETTING = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());
        mTopToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mTopToolbar);
        // Session Manager
        session = new SessionManager(getApplicationContext());
        profile = new ProfileDrawerItem().withName(session.getUserDetails().getFull_name()).withEmail(session.getUserDetails().getEmail());
        System.out.println("ACTIVE ITEM MENU = "+BaseActivity.activeItem);

        buildHeader(false, savedInstanceState);
        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(mTopToolbar)
                .withAccountHeader(headerResult)
                .withSelectedItemByPosition(1)
                .addDrawerItems(
//                        new PrimaryDrawerItem().withName("Home").withIcon(),
                        new PrimaryDrawerItem().withName(R.string.setting_menu).withIcon(R.drawable.ic_home).withEnabled(true).withSelectable(true).withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                            @Override
                            public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                                System.out.println("POSITION HOME= "+position);
                                activeItem = drawerItem;
                                goToMenu();
                                return false;
                            }
                        }),
                        new SecondaryDrawerItem().withName(R.string.setting_catalague).withIcon(R.drawable.ic_catalogue).withEnabled(true).withSelectable(true).withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                            @Override
                            public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                                System.out.println("POSITION CATALOG= "+position);
                                activeItem = drawerItem;
                                goToCatalogue();
                                return false;
                            }
                        }),
                        new SecondaryDrawerItem().withName(R.string.setting_mon_profil).withIcon(R.drawable.ic_profil).withEnabled(true).withSelectable(true).withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                            @Override
                            public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                                System.out.println("POSITION PROFILE= "+position);
                                activeItem = drawerItem;
                                goToProfil();
                                return false;
                            }
                        }),
                        new SecondaryDrawerItem().withName(R.string.setting_deconnexion).withIcon(R.drawable.ic_deconenxion).withEnabled(true).withSelectable(true).withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                            @Override
                            public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                                System.out.println("POSITION LOGOUT= "+position);
                                activeItem = drawerItem;
                                logOut();
                                return false;
                            }
                        })
                ) // add the items we want to use with our Drawer
                .withOnDrawerNavigationListener(new Drawer.OnDrawerNavigationListener() {
                    @Override
                    public boolean onNavigationClickListener(View clickedView) {
                        //this method is only called if the Arrow icon is shown. The hamburger is automatically managed by the MaterialDrawer
                        //if the back arrow is shown. close the activity
                        BaseActivity.this.finish();
                        //return true if we have consumed the event
                        return true;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .build();

                if (activeItem != null) {
                    result.setSelection(activeItem,false);
                    System.out.println("IN SET SELECTION with activeItem= "+activeItem.isSelected());
                }


    }

    /**
     * small helper method to reuse the logic to build the AccountHeader
     * this will be used to replace the header of the drawer with a compact/normal header
     *
     * @param compact
     * @param savedInstanceState
     */
    private void buildHeader(boolean compact, Bundle savedInstanceState) {
        // Create the AccountHeader
        headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.color_gris_clair)
                .withCompactStyle(compact)
                .addProfiles(
                        profile
                )
                .withAccountHeader(R.layout.material_drawar_header)
                .withTextColor(getResources().getColor(R.color.color_blue))
                .withSavedInstance(savedInstanceState)
                .build();
    }


    protected abstract int getLayoutResourceId();

    public void logOut() {
        session.logoutUser();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle b = new Bundle();
        b.putSerializable("produit", HomeActivity.liste_produits);
        intent.putExtras(b);
        this.startActivity(intent);
        finish();
    }

    public void goToCatalogue() {

        Intent intent = new Intent(this, CatalogueActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
        finish();
    }

    public void goToProfil() {

        Intent intent = new Intent(this, EditProfileActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
        finish();
    }

    public void goToMenu() {

        Intent intent = new Intent(this, MenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.action_deconnexion) {
            logOut();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
