package mobi.app4mob.quartzclub.activity;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.constants.Constants;

import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.ProductDataParser;
import mobi.app4mob.quartzclub.parser.UserDataParser;
import mobi.app4mob.quartzclub.utils.AlertMessage;
import mobi.app4mob.quartzclub.utils.ChecKConnection;
import mobi.app4mob.quartzclub.utils.SessionManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

public class SplashScreen extends Activity {

    private Context context;
    Activity activity;
    private static ArrayList<Produit> liste_produits = null;
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        activity = this;

        context = this;

        final Handler uiThreadCallback = new Handler();
        new Thread() {
            boolean noConnection = false;

            @Override
            public void run() {
                Looper.prepare();

                if (new ChecKConnection(SplashScreen.this).verifierconnexion()) {

                    // Session Manager
                    session = new SessionManager(getApplicationContext());
                    if (session.isLoggedIn()) {
                        User user = session.getUserDetails();
                        new UserDataParser();
                        UserDataParser.updateUser(Constants.UPDATE_USER,
                                user.getId(), getApplicationContext());
                    }

                    // sleep(2000);


                } else {
                    noConnection = true;
                    try {
                        sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                uiThreadCallback.post(new Runnable() {
                    @Override
                    public void run() {

                        if (noConnection) {
                            showMessageNoConnexion();
                        } else {

                                Intent intent = new Intent(SplashScreen.this,
                                        LoginActivity.class);
                                Bundle b = new Bundle();
                                intent.putExtras(b);
                                activity.startActivity(intent);
                                finish();



                        }

                    }

                });
            }
        }.start();

    }

    private void showMessageNoConnexion() {
        AlertMessage connexionFailed = new AlertMessage(activity) {
            @Override
            public void onOk() {
                finish();
            }

            @Override
            public void onCancel() {

            }
        };

        connexionFailed.show1(getString(R.string.no_connection), getString(R.string.quartz_club),
                false, getString(R.string.valider));

    }

    private void showMessageError() {
        AlertMessage connexionFailed = new AlertMessage(activity) {
            @Override
            public void onOk() {
                finish();
            }

            @Override
            public void onCancel() {

            }
        };

        connexionFailed.show1(getString(R.string.error_loading), getString(R.string.quartz_club),
                false, getString(R.string.valider));

    }

}
