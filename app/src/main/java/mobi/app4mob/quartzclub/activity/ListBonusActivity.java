package mobi.app4mob.quartzclub.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;

public class ListBonusActivity extends BaseActivity {

    public static User user = null;
    public static Produit produit;
    public int type_bonus;
    public int type_profil;
    public static ArrayList<Produit> liste_produits = null;
    public static final int COMMERCIALBONUS = 1;
    public static final int MAGASINBONUS = 2;
    public Bundle bn = new Bundle();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView action_bar_title = (TextView) findViewById(R.id.txt_action_bar_title);

        action_bar_title.setText(R.string.list_bonus_title);

        if (bn.getInt("type_profil") == ListBonusActivity.COMMERCIALBONUS)
            action_bar_title.setTextColor(getResources().getColor(R.color.color_bleu_marine));
        else
            action_bar_title.setTextColor(getResources().getColor(R.color.color_rose));

        initViews();
    }

    private void initViews() {

        super.mTopToolbar.setBackgroundColor(getResources().getColor(R.color.toolbar));

    }

    @Override
    protected int getLayoutResourceId() {

        bn = getIntent().getExtras();
        user = (User) bn.getSerializable("user");
        liste_produits = (ArrayList<Produit>) bn.getSerializable("produit");
        type_profil = bn.getInt("type_profil");
        System.out.println(">>>>>>>LB>>>>>>" + liste_produits.size());

        if (type_profil == ListBonusActivity.COMMERCIALBONUS)
            return R.layout.activity_list_bonus;
        else
            return R.layout.activity_list_bonus_magasin;
    }

    public void goToBonusProduit(View v) {

        Intent intent = new Intent(ListBonusActivity.this, BonusProdNPackActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", user);
        b.putSerializable("produit", liste_produits);
        b.putInt("type_profil", type_profil); // bonus commercial
        b.putInt("type_bonus", 1); //bonus produit
        intent.putExtras(b);
        startActivity(intent);

    }

    public void goToBonusPack(View v) {

        Intent intent = new Intent(ListBonusActivity.this, BonusProdNPackActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", user);
        b.putSerializable("produit", liste_produits);
        b.putInt("type_profil", type_profil); // bonus commercial
        b.putInt("type_bonus", 2); //bonus produit
        intent.putExtras(b);
        startActivity(intent);

    }

    public void goToBonusAnniv(View v) {

        Intent intent = new Intent(ListBonusActivity.this, BonusAnniversaireActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", user);
        b.putInt("type_profil", type_profil); // bonus commercial
        intent.putExtras(b);
        startActivity(intent);

    }

    public void goToBonusPalier(View v) {

        Intent intent = new Intent(ListBonusActivity.this, BonusPalierActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", user);
        b.putInt("type_profil", type_profil); // bonus commercial
        intent.putExtras(b);
        startActivity(intent);

    }

    public void goToBonusBienvenu(View v) {

        Intent intent = new Intent(ListBonusActivity.this, BonusBienvenuActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", user);
        b.putInt("type_profil", type_profil); // bonus commercial
        intent.putExtras(b);
        startActivity(intent);

    }

    public void goToBonusProduitMagasin(View v) {

        Intent intent = new Intent(ListBonusActivity.this, BonusProdNPackActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", user);
        b.putSerializable("produit", liste_produits);
        b.putInt("type_profil", type_profil); // bonus commercial
        b.putInt("type_bonus", 1); //bonus produit
        intent.putExtras(b);
        startActivity(intent);

    }

    public void goToBonusPackMagasin(View v) {

        Intent intent = new Intent(ListBonusActivity.this, BonusProdNPackActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", user);
        b.putSerializable("produit", liste_produits);
        b.putInt("type_profil", type_profil); // bonus commercial
        b.putInt("type_bonus", 2); //bonus produit
        intent.putExtras(b);
        startActivity(intent);

    }

    public void goToBonusAnnivMagasin(View v) {

        Intent intent = new Intent(ListBonusActivity.this, BonusAnniversaireActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", user);
        b.putInt("type_profil", type_profil); // bonus commercial
        intent.putExtras(b);
        startActivity(intent);

    }

    public void goToBonusPalierMagasin(View v) {

        Intent intent = new Intent(ListBonusActivity.this, BonusPalierActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", user);
        b.putInt("type_profil", type_profil); // bonus commercial
        intent.putExtras(b);
        startActivity(intent);

    }

    public void goToBonusBienvenuMagasin(View v) {

        Intent intent = new Intent(ListBonusActivity.this, BonusBienvenuActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", user);
        b.putInt("type_profil", type_profil); // bonus commercial
        intent.putExtras(b);
        startActivity(intent);

    }
}
