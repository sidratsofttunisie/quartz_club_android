package mobi.app4mob.quartzclub.activity;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.adapter.PromosPagerAdapter;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.Promo;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.PromotionDataParser;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;

import com.viewpagerindicator.CirclePageIndicator;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.TextView;
import android.widget.Toast;

public class ListPromosActivity extends Activity {
    private Context context;
    private ArrayList<Promo> arrayPromos;

    private ViewPager viewPager;
    private PagerAdapter adapter;
    private ArrayList<Produit> liste_produits;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_promos);
        context = this;
        TextView action_bar_title = (TextView) findViewById(R.id.txt_action_bar_title);
        action_bar_title.setText(R.string.list_promo_title);

        Bundle bn = new Bundle();
        bn = getIntent().getExtras();
        liste_produits = (ArrayList<Produit>) bn.getSerializable("produit");
        Produit.liste_produits = liste_produits;
        loadPromos();
    }

    private void loadPromos() {
        ProgressTraitment progress = new ProgressTraitment(context) {

            @Override
            public void onLoadStarted() {

                arrayPromos = new PromotionDataParser()
                        .getPromos(Constants.Promos_WS_URL);

            }

            @Override
            public void onLoadFinished() {
                if (arrayPromos != null) {
                    if (arrayPromos.size() > 0) {

                        viewPager = (ViewPager) findViewById(R.id.pager);
                        adapter = new PromosPagerAdapter(arrayPromos, context);
                        viewPager.setAdapter(adapter);

                        CirclePageIndicator circleIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
                        circleIndicator.setViewPager(viewPager);

                    } else {
                        new MyToast(context, " 0 promos", Toast.LENGTH_SHORT);
                    }
                } else {
                    new MyToast(context, getString(R.string.error_loading),
                            Toast.LENGTH_SHORT);
                }

            }

        };

        progress.DoTraitemnt("", "");

    }

}
