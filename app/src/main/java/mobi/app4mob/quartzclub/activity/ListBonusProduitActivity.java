package mobi.app4mob.quartzclub.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ListBonusProduitActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_list_bonus_produit;
    }
}
