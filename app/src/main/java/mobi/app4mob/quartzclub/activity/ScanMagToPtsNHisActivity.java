package mobi.app4mob.quartzclub.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dm.zbar.android.scanner.ZBarConstants;

import mobi.app4mob.quartzclub.adapter.BonusBienvenuDataParser;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.model.MagInfo;
import mobi.app4mob.quartzclub.model.Magasin;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.MagasinInfoParser;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;
import mobi.app4mob.quartzclub.utils.SessionManager;
import mobi.app4mob.quartzclub.zxing.ScalingScannerActivity;

import static mobi.app4mob.quartzclub.activity.AddMagasinActivity.SCAN_RESULT;

public class ScanMagToPtsNHisActivity extends BaseActivity {

    private User user;
    private Magasin magasin;
    private static final int ZBAR_QR_SCANNER_REQUEST = 1;
    private MagInfo magInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        super.mTopToolbar.setBackgroundColor(getResources().getColor(R.color.color_rose));
        TextView action_bar_title = (TextView) findViewById(R.id.txt_action_bar_title);
        action_bar_title.setText(R.string.scan_code);

    }

    public void scanCodeMagasin(View v) {
        goScan();
    }


    public boolean isCameraAvailable() {
        PackageManager pm = getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    private void goScan() {
        if (isCameraAvailable()) {
            Intent intent = new Intent(ScanMagToPtsNHisActivity.this, ScalingScannerActivity.class);
            intent.putExtra("type_profil", 2);
            startActivityForResult(intent, ZBAR_QR_SCANNER_REQUEST);
        } else {

            Toast.makeText(this, getString(R.string.camera_indisponible),
                    Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ZBAR_QR_SCANNER_REQUEST:
                if (resultCode == RESULT_OK) {

                    ((EditText)findViewById(R.id.edit_tag_magasin)).setText(data.getStringExtra(SCAN_RESULT));


                } else if (resultCode == RESULT_CANCELED && data != null) {
                    String error = data.getStringExtra(ZBarConstants.ERROR_INFO);
                    if (!TextUtils.isEmpty(error)) {
                        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    public void verifyTag(View v) {

        if (((EditText)findViewById(R.id.edit_tag_magasin)).getText().toString().length()>0) {
            ProgressTraitment progress = new ProgressTraitment(this) {


                @Override
                public void onLoadStarted() {

                    magInfo = new MagasinInfoParser().getMagInfo(Constants.MAGINFO_WS_URL + "?ref_magasin=" + ((EditText) findViewById(R.id.edit_tag_magasin)).getText().toString());

                }

                @Override
                public void onLoadFinished() {

                    if (magInfo != null) {

                        if (magInfo.getNext_gift_name().length() > 0) {

                            goToPointsMagasin();

                        } else {
                            new MyToast(ScanMagToPtsNHisActivity.this, getString(R.string.error_loading),
                                    Toast.LENGTH_SHORT);
                        }

                    } else {
                        new MyToast(ScanMagToPtsNHisActivity.this, getString(R.string.error_loading),
                                Toast.LENGTH_SHORT);
                    }

                }


            };
            progress.DoTraitemnt("", "");
        }

    }

    public void goToPointsMagasin() {

        session = new SessionManager(getApplicationContext());
        Intent intent = new Intent(this, HomeActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("user", session.getUserDetails());
        b.putSerializable("maginfo", magInfo);
        b.putInt("type_profil", 1);
        intent.putExtras(b);
        this.startActivity(intent);


    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.scan_magasin;
    }
}
