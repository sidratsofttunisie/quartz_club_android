package mobi.app4mob.quartzclub.activity;

import java.util.ArrayList;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;

import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.Magasin;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.utils.GPSTracker;
import mobi.app4mob.quartzclub.utils.MyToast;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class RefreshPositionActivity extends FragmentActivity {
    public static User user;
    public static ArrayList<Magasin> magasins;
    Activity activity;
    private GoogleMap mMap;
    public static FragmentManager fragmentManager;

    private double latitude = 0;
    private double longitude = 0;




    GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.refresh_position_layout);


      
        TextView action_bar_title = (TextView) findViewById(R.id.txt_action_bar_title);
        action_bar_title.setText(R.string.refresh_position);

        ((Button) findViewById(R.id.ab_refersh_pos))
                .setVisibility(View.VISIBLE);

       
        activity = this;
        gps = new GPSTracker(activity);
        gps.checkGps();
        initLatLong();

        fragmentManager = getSupportFragmentManager();
        setUpMap();

    }

    public void refreshPos(View v) {
		gps = new GPSTracker(activity);
		gps.checkGps();
		initLatLong();
		UpdateMap();
		
	}


    private void setUpMap() {

		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (mMap == null) {
			// Try to obtain the map from the SupportMapFragment.
			mMap = ((SupportMapFragment) RefreshPositionActivity.fragmentManager
					.findFragmentById(R.id.map_add)).getMap();
			// mMap = ((NiceSupportMapFragment)
			// getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
			// ((NiceSupportMapFragment)
			// getSupportFragmentManager().findFragmentById(R.id.map))
			// .setPreventParentScrolling(true);
			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				Log.d("total2", "long=" + longitude);
				Log.d("total2", "lat=" + latitude);
				mMap.addMarker(new MarkerOptions()
						.position(new LatLng(latitude, longitude))
						.draggable(true)
						.title("")
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.ic_map_pin_toaster)));

				mMap.getUiSettings().setScrollGesturesEnabled(true);
				mMap.getUiSettings().setZoomControlsEnabled(true);
				mMap.getUiSettings().setMyLocationButtonEnabled(true);

				mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
						latitude, longitude), 14));

			
			}
		}
	}
	
	private void UpdateMap() {

		// Do a null check to confirm that we have not already instantiated the
		// map.
		

			// Try to obtain the map from the SupportMapFragment.
			mMap = ((SupportMapFragment) RefreshPositionActivity.fragmentManager
					.findFragmentById(R.id.map_add)).getMap();
			// mMap = ((NiceSupportMapFragment)
			// getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
			// ((NiceSupportMapFragment)
			// getSupportFragmentManager().findFragmentById(R.id.map))
			// .setPreventParentScrolling(true);
			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				mMap.clear();
				Log.d("total2", "long=" + longitude);
				Log.d("total2", "lat=" + latitude);
				mMap.addMarker(new MarkerOptions()
						.position(new LatLng(latitude, longitude))
						.draggable(true)
						.title("")
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.ic_map_pin_toaster)));

				mMap.getUiSettings().setScrollGesturesEnabled(true);
				mMap.getUiSettings().setZoomControlsEnabled(true);
				mMap.getUiSettings().setMyLocationButtonEnabled(true);

				mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
						latitude, longitude), 14));

			

			}
		
	}

    
    private void initLatLong() {
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        } else {
            longitude = 10.180744;
            latitude = 36.799152;
            Functions.LOGE("ERROR",
					getString(R.string.pas_pu_localiser_emplacement));
            new MyToast(getApplicationContext(),
					getString(R.string.pas_pu_localiser_emplacement),
                    MyToast.LENGTH_SHORT);
        }
    }
  
    
    public void close(View v){
    	finish();
    }
}
