package mobi.app4mob.quartzclub.activity;

import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.adapter.BonusBienvenuDataParser;
import mobi.app4mob.quartzclub.adapter.ListBonusAnnivAdapter;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.model.BonusAnniv;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.BonusAnnivDataParser;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;

public class BonusBienvenuActivity extends BaseActivity {

    public static User user = null;
    public static Produit produit;
    public int type_profil;
    public ListView mListView;
    private String[] arrayBonus;
    private ListBonusAnnivAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        TextView action_bar_title = (TextView) findViewById(R.id.txt_action_bar_title);
        Bundle bn = new Bundle();
        bn = getIntent().getExtras();
        user = (User) bn.getSerializable("user");
        type_profil = bn.getInt("type_profil");

        if (bn.getInt("type_profil") == ListBonusActivity.COMMERCIALBONUS)
            action_bar_title.setText(R.string.list_bonus_bienvenu_commercial_title);
        else
            action_bar_title.setText(R.string.list_bonus_bienvenu_magasin_title);

        initView();


    }

    private void initView() {

        TextView title = (TextView)findViewById(R.id.title);

        title.getPaint().setShader(
                new LinearGradient(200, 100, 10, title
                        .getHeight(), 0xFFEFDC6F, 0xFFB98827,
                        Shader.TileMode.MIRROR));
        title.setText(getResources().getString(R.string.bonusBienvenu_title));


        if (type_profil == ListBonusActivity.COMMERCIALBONUS) {
            super.mTopToolbar.setBackgroundColor(getResources().getColor(R.color.color_bleu_marine));
        }else{
            super.mTopToolbar.setBackgroundColor(getResources().getColor(R.color.color_rose));
            ((TextView) findViewById(R.id.points)).setTextColor(getResources().getColor(R.color.color_rose));
            ((TextView) findViewById(R.id.pts_title)).setTextColor(getResources().getColor(R.color.color_rose));
        }

        ProgressTraitment progress = new ProgressTraitment(this) {

            @Override
            public void onLoadStarted() {

                arrayBonus = new BonusBienvenuDataParser().getBonusBienvenu(Constants.BONUS_BIENVENU_WS_URL + "?user_id=" + user.getId());

            }

            @Override
            public void onLoadFinished() {

                if (arrayBonus != null) {
                    if (arrayBonus.length > 0) {

                        if (type_profil == ListBonusActivity.COMMERCIALBONUS)
                            ((TextView) findViewById(R.id.points)).setText(arrayBonus[0]+" ");
                        else {
                            ((TextView) findViewById(R.id.points)).setText(arrayBonus[1] + " ");
                        }

                    } else {
                        new MyToast(BonusBienvenuActivity.this, " 0 Bonus", Toast.LENGTH_SHORT);
                    }
                } else {
                    new MyToast(BonusBienvenuActivity.this, getString(R.string.error_loading),
                            Toast.LENGTH_SHORT);
                }

            }


        };
        progress.DoTraitemnt("", "");

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_bonus_bienvenu;
    }
}
