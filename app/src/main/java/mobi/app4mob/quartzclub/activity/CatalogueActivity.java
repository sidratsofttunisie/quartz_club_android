package mobi.app4mob.quartzclub.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import mobi.app4mob.quartzclub.adapter.ExpandableBonusListAdapter;
import mobi.app4mob.quartzclub.adapter.CategsListAdapter;
import mobi.app4mob.quartzclub.adapter.MessageAdapter;
import mobi.app4mob.quartzclub.adapter.SlidingImage_Adapter;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.model.Slide;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.CatalogueDataParser;
import mobi.app4mob.quartzclub.parser.RegisterTokenParser;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;

public class CatalogueActivity extends BaseActivity {
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private User user;
    private static final Integer[] IMAGES = {R.drawable.btn_bonus_annif, R.drawable.btn_bonus_mg, R.drawable.btn_bonus_pack, R.drawable.btn_bonus_palier};
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    private ArrayList<Slide> categs;
    private ListView mListView;
    private CategsListAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView action_bar_title = (TextView) findViewById(R.id.txt_action_bar_title);
        action_bar_title.setText(R.string.catalogue_title);
        loadData();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_catalogue;
    }

    private void loadData() {

        ProgressTraitment progress = new ProgressTraitment(this) {

            ArrayList<Slide> tempCategs;

            @Override
            public void onLoadStarted() {

                user = session.getUserDetails();
                tempCategs = new CatalogueDataParser().getCatalogues(Constants.CATALOGUE_WS_URL + "?commercial_id=" +
                        user.getId());

            }

            @Override
            public void onLoadFinished() {

                if (tempCategs != null) {
                    if (tempCategs.size() > 0) {
                        //todo
                        categs = tempCategs;

                        //expandable list
                        mListView = (ListView) findViewById(R.id.exp_list);
                        DisplayMetrics metrics = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(metrics);
                        int width = metrics.widthPixels;
                        mAdapter = new CategsListAdapter(CatalogueActivity.this,
                                tempCategs);
                        mListView.setAdapter(mAdapter);

                        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                                Intent i = new Intent(CatalogueActivity.this,SlideActivity.class);
                                Bundle b = new Bundle();
                                b.putSerializable("photos",tempCategs.get(pos).getPhotos());
                                b.putString("title",tempCategs.get(pos).getTitre());
                                i.putExtras(b);
                                startActivity(i);
                            }
                        });

                    } else {
                        new MyToast(CatalogueActivity.this,
                                getString(R.string.no_catalogues),
                                Toast.LENGTH_SHORT);
                    }
                }else {
                    new MyToast(CatalogueActivity.this,
                            getString(R.string.error_occured),
                            Toast.LENGTH_SHORT);
                }

            }

        };

        progress.DoTraitemnt("", "");

    }

}