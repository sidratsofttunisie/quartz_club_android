package mobi.app4mob.quartzclub.activity;

import java.io.Serializable;
import java.util.ArrayList;

import org.json.JSONObject;

import net.sourceforge.zbar.Symbol;

import com.dm.zbar.android.scanner.ZBarConstants;
import com.dm.zbar.android.scanner.ZBarScannerActivity;

import mobi.app4mob.quartzclub.constants.Constants.Extra;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.CommandProduct;
import mobi.app4mob.quartzclub.model.Magasin;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.ProductDataParser;
import mobi.app4mob.quartzclub.parser.UserDataParser;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;
import mobi.app4mob.quartzclub.zxing.ScalingScannerActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static mobi.app4mob.quartzclub.activity.AddMagasinActivity.SCAN_RESULT;

public class ScanProductActivity extends Activity {
	private User user;
	private Magasin magasin;
	private ArrayList<CommandProduct> list_command_product = new ArrayList<CommandProduct>();
	private Produit product;
	// pour le retoure de l'activty reste une fois
	public static Boolean my_first_time = true;
	Activity activity;
	private ArrayList<Produit> liste_produits;
	private Boolean openTheCam = true;

	private EditText edit_code_product;
	private String full_code_product = ""; // tout le code (example
	// 12345600001)
	private String code_product = "";// just la partie identifiant du
	// produit
	// (example 123456)
	//private String code_package = "";// just la partie identifiant du
	// package
	// (example 000001)
	private static final int ZBAR_SCANNER_REQUEST = 0;
	private static final int ZBAR_QR_SCANNER_REQUEST = 1;
	MediaPlayer mp;
	MediaPlayer mperror;
	boolean carte = false;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scan_product);
		Bundle bn = new Bundle();
		bn = getIntent().getExtras();
		openTheCam = bn.getBoolean("openTheCam", false);
		user = (User) bn.getSerializable("user");
		magasin = (Magasin) bn.getSerializable("magasin");
		liste_produits = (ArrayList<Produit>) bn.getSerializable("produit");
		Produit.liste_produits = liste_produits;
		carte = bn.getBoolean("carte",false);
		 mp = MediaPlayer.create(this, R.raw.beep);
		 mperror = MediaPlayer.create(this, R.raw.beep_error);
		/*if (bn.getString("from_activity").equals(Extra.SCAN_TAG_MAG)) {
			Functions.LOGI("**** scan product  ****", "from scan tag");
		}*/
		boolean fromActivity = false;
		fromActivity = bn.getString("from_activity").equals(Extra.LIST_COMMAND_PRODUCT);
		if (fromActivity) {
			list_command_product = (ArrayList<CommandProduct>) bn
					.getSerializable("list_command");
		}

		activity = this;

		initViews();

	}

	private void initViews() {
		TextView action_bar_title = (TextView) findViewById(R.id.txt_action_bar_title);
		action_bar_title.setText(R.string.scan_tag_magasin_title);

		edit_code_product = (EditText) findViewById(R.id.edit_code_product);
		if (openTheCam) {
			goScan();

		}
		if (carte){
			goScan();
		}

	}

	public void scanCodeMagasin(View v) {
		// launchScanner();
		goScan();
	}

	private void goScan() {
        if (isCameraAvailable()) {
            Intent intent = new Intent(this, ScalingScannerActivity.class);
            intent.putExtra("type_profil",1);
            int nbProd = 0;
			for (CommandProduct cmd : list_command_product
				 ) {
				nbProd += cmd.getListe_code().size();
			}

            intent.putExtra("nbProduitScanner",nbProd);
            intent.putExtra("scanProduct",true);
            intent.putExtra("productExist",product_is_existe);
            startActivityForResult(intent, ZBAR_QR_SCANNER_REQUEST);

        } else {

            Toast.makeText(this, getString(R.string.camera_indisponible),
                    Toast.LENGTH_SHORT).show();

        }
    }
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
			case ZBAR_SCANNER_REQUEST:
			case ZBAR_QR_SCANNER_REQUEST:
				if (resultCode == RESULT_OK) {

					edit_code_product.setText(data
							.getStringExtra(SCAN_RESULT));

					verifyCode();

				} else if (resultCode == RESULT_CANCELED && data != null) {

					String error = data.getStringExtra(ZBarConstants.ERROR_INFO);
					if (!TextUtils.isEmpty(error)) {
						Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
					}
				}

				break;
			case Constants.PIC2SHOP_REQUEST_CODE_SCAN:
				if (resultCode == RESULT_OK) {

					String barcode = data
							.getStringExtra(Constants.PIC2SHOP_BARCODE);
					String resultText = barcode;
					edit_code_product.setText(resultText);
					verifyCode();

				} else if (resultCode == RESULT_CANCELED && data != null) {

					String error = data.getStringExtra(ZBarConstants.ERROR_INFO);
					if (!TextUtils.isEmpty(error)) {

						Toast.makeText(this, error, Toast.LENGTH_SHORT).show();

					}
				}
				break;
		}


	}


	public Boolean checkIfPackExiste() {
		ArrayList<String> list_code = new ArrayList<String>();
		Boolean package_is_existe = false;
		for (int i = 0; i < list_command_product.size(); i++) {
			list_code = new ArrayList<String>();
			Functions.LOGI("****", "i = " + i + " product code = "
					+ list_command_product.get(i).getProduct().getCode()
					+ "----- code scan: " + code_product);
			if (list_command_product.get(i).getProduct().getCode()
					.equals(code_product)) {
				list_code = list_command_product.get(i).getListe_code();
				// si le packet est existe dans la command
				if (list_code.contains(full_code_product)) {
					ScalingScannerActivity scalingScannerActivity = new ScalingScannerActivity();
					if (!scalingScannerActivity.isActive()){
                        mperror.start();
						new MyToast(getApplicationContext(),
								R.string.package_existe, Toast.LENGTH_SHORT);
					}
					package_is_existe = true;

				}
				//break;
			}
		}
		return package_is_existe;
	}

	public boolean isProductCodeValid(String s) {
		String pattern12 = "^[0-9]{12}$";
		String pattern18 = "^[0-9]{18}$";
		String pattern13 = "^[0-9]{13}$";
		if (s.matches(pattern12) || s.matches(pattern18) || s.matches(pattern13)) {
			return true;
		}
		return false;
	}

	public void addToCommand(View v) {
		verifyCode();
	}

	Boolean product_is_existe = false;

	public void verifyCode() {

		full_code_product = edit_code_product.getText().toString().trim();

		// vérifier si le format de produit scanné est valide ==> example d'un
		// code valid 123456002563
		if (isProductCodeValid(full_code_product)) {


			code_product = full_code_product.substring(0, 6);
			//code_package = full_code_product.substring(6, 12);

			/*Functions.LOGI("**** code ***", "full code : " + full_code_product
					+ " --- code produit : " + code_product
					+ " --- code packet : " + code_package);*/

			if (!checkIfPackExiste()) {
				Produit product = new Produit();
				if (liste_produits != null && liste_produits.size() > 0) {
					product = product.getProduitByCode(code_product);
				}
				if (product != null) {
					ArrayList<String> list_code = new ArrayList<String>();
					 product_is_existe = false;

					for (int i = 0; i < list_command_product.size(); i++) {
						list_code = new ArrayList<String>();
						// si le produit scanné est existe dans la liste
						// des
						// produit scanné on ajout seleument le code a
						// barra
						// du
						// packet ajouté
						if (list_command_product.get(i).getProduct().getCode()
								.equals(product.getCode())) {

							mp.start();
							list_code = list_command_product.get(i)
									.getListe_code();

							list_code.add(full_code_product);
							list_command_product.get(i)
									.setListe_code(list_code);

							product_is_existe = true;
							//break;
						}

					}

					/*
					 * list_command_product.get(i).setProduct(product);
					 * list_code.add(code_product); list_command_product.
					 * get(i).setListe_code(list_code);
					 */
					// si le produit n'existe pas on ajout le produit a
					// la
					// liste
					// des produit scané ainsi la liste des code a bar
					// scanné
					if (!product_is_existe) {
						CommandProduct commandProduct = new CommandProduct();
						list_code.add(full_code_product);
						commandProduct.setProduct(product);
						commandProduct.setListe_code(list_code);
						list_command_product.add(commandProduct);
                        mp.start();

					}else{

						ScalingScannerActivity scalingScannerActivity = new ScalingScannerActivity();
						if (scalingScannerActivity.isActive()){
						 Toast.makeText(ScanProductActivity.this,R.string.package_existe,Toast.LENGTH_LONG).show();
                            mperror.start();
						}
					}

						goScan();



					if (ScanProductActivity.my_first_time) {
						ScanProductActivity.my_first_time = false;
					} else {
						//finish();
					}

				} else

				{
					new MyToast(getApplicationContext(),
							R.string.invalid_product, Toast.LENGTH_SHORT);
                    mperror.start();
					edit_code_product.setText("");

					Log.v("scan product", getString(R.string.invalid_product));
				}

			}
		} else {
            mperror.start();
			new MyToast(getApplicationContext(), R.string.invalid_product,
					Toast.LENGTH_SHORT);
		}
	}

	/*
	 * ************************* FOR PHONE WITH AUTO FOCUS
	 * **************************
	 */
	/*
	 * 
	 * QR code /Barre code scanner methode
	 */

	public void launchScanner() {
		if (isCameraAvailable()) {
			Intent intent = new Intent(this, ZBarScannerActivity.class);
			startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
		} else {
			Toast.makeText(this, getString(R.string.camera_indisponible),
					Toast.LENGTH_SHORT).show();
		}
	}

	public void launchQRScanner() {
		if (isCameraAvailable()) {
			Intent intent = new Intent(this, ZBarScannerActivity.class);
			intent.putExtra(ZBarConstants.SCAN_MODES,
					new int[] { Symbol.QRCODE });
			startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
		} else {
			Toast.makeText(this, getString(R.string.camera_indisponible),
					Toast.LENGTH_SHORT).show();
		}
	}

	public boolean isCameraAvailable() {
		PackageManager pm = getPackageManager();
		return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
	}

	/*public void launchPic2ShopScanner(){
		//Intent intent = new Intent(Constants.PIC2SHOP_ACTION);
		//startActivityForResult(intent, Constants.PIC2SHOP_REQUEST_CODE_SCAN);
		
		Intent intent = new Intent(Constants.Pro.ACTION);
		intent.putExtra(Constants.Pro.FORMATS, "EAN13");
		startActivityForResult(intent, Constants.PIC2SHOP_REQUEST_CODE_SCAN);
	}*/

	@Override
	protected void onResume() {
		super.onResume();
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		boolean goToList = preferences.getBoolean("goToList", false);
		boolean openScanner = preferences.getBoolean("openScanner", false);

		SharedPreferences.Editor editor = preferences.edit();
		if (goToList){
			editor.putBoolean("goToList", false);
			editor.commit();
			goTolistProduct(ScanProductActivity.this);
		}
		if (openScanner){
			editor.putBoolean("openScanner", false);
			editor.commit();
			goScan();
		}
	}

	@Override
	public void onBackPressed() {
		finish();
		/*if (!ScanProductActivity.my_first_time) {
			Intent intent = new Intent(activity, AddCommandActivity.class);
			Bundle b = new Bundle();

			b.putSerializable("user", user);
			b.putSerializable("magasin", magasin);
			b.putSerializable("produit", liste_produits);
			b.putSerializable("from_activity", Extra.SCAN_CODE_PRODUCT);
			b.putSerializable("list_command", list_command_product);
			intent.putExtras(b);
			activity.startActivity(intent);
		}*/
		super.onBackPressed();
	}

	public void goTolistProduct(Context context){
		Intent intent = new Intent(context,
				AddCommandActivity.class);
		Bundle b = new Bundle();

		b.putSerializable("user", user);
		b.putSerializable("magasin", magasin);
		b.putSerializable("produit", liste_produits);
		b.putSerializable("from_activity", Extra.SCAN_CODE_PRODUCT);
		b.putSerializable("list_command", list_command_product);
		intent.putExtras(b);
		activity.startActivity(intent);
	}

}
