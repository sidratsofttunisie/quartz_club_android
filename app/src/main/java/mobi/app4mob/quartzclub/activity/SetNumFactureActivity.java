package mobi.app4mob.quartzclub.activity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.constants.Constants.Extra;
import mobi.app4mob.quartzclub.model.Command;
import mobi.app4mob.quartzclub.model.CommandProduct;
import mobi.app4mob.quartzclub.model.Magasin;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.CommandDataParser;
import mobi.app4mob.quartzclub.utils.AlertMessage;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;
import mobi.app4mob.quartzclub.utils.SessionManager;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SetNumFactureActivity extends Activity {
    public static ArrayList<String> list_product_repeated;
    private User user;
    private Magasin magasin;
    private ArrayList<CommandProduct> list_command_product = new ArrayList<CommandProduct>();
    private static EditText edit_num_facture;
    private static Boolean is_saved; // si la command est envoyé
    private static Command command;
    Activity activity;
    private ArrayList<Produit> liste_produits;
    private int point_gagner;
    
    //new update: show number of point magasin gagné
    public static int point_magasin = 0;
    public static int point_magasin_total = 0;
    
    SessionManager session;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_num_facture);

        Bundle bn = new Bundle();
        bn = getIntent().getExtras();
        user = (User) bn.getSerializable("user");
        magasin = (Magasin) bn.getSerializable("magasin");
        liste_produits = (ArrayList<Produit>) bn.getSerializable("produit");
        list_command_product = (ArrayList<CommandProduct>) bn
                .getSerializable("list_command");

        activity = this;

        initViews();
    }

    private void initViews() {
        TextView action_bar_title = (TextView) findViewById(R.id.txt_action_bar_title);
        action_bar_title.setText(R.string.num_facture_title);

        edit_num_facture = (EditText) findViewById(R.id.edit_num_facture);

    }

    public void SaveCommand(View v) {
        String num_facture = edit_num_facture.getText().toString().trim();
        if (Functions.isStringEmpty(num_facture)) {
            new MyToast(getApplicationContext(), R.string.no_num_fact,
                    Toast.LENGTH_SHORT);
        } else {
            command = new Command(num_facture, list_command_product,
                    magasin.getId(), user.getId());
            sendCommand();
        }
    }

    private void sendCommand() {
        ProgressTraitment prog = new ProgressTraitment(this) {

            @Override
            public void onLoadStarted() {
                point_gagner = CommandDataParser.sendCommand(command);

            }

            @SuppressWarnings("unchecked")
            @Override
            public void onLoadFinished() {
            	
            	Log.i(">>>",">>>>point_gagner"+point_gagner);
                if (point_gagner >= 0) {

                    AlertMessage dialogCommandSuccess = new AlertMessage(
                            activity) {
                        @Override
                        public void onOk() {
                            AddCommandActivity.list_command_product = new ArrayList<CommandProduct>();
                            Intent intent = new Intent(activity,
                                    MenuActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            user.setPoint(point_gagner + user.getPoint());


                            // Update Session User(with new solde point)
                            session = new SessionManager(getApplicationContext());
                            session.updateSession(user);


                            Bundle b = new Bundle();
                            b.putSerializable("user", user);
                            b.putSerializable("produit",
                                    liste_produits);
                            intent.putExtras(b);
                            activity.startActivity(intent);
                            finishAffinity();
                        }

                        @Override
                        public void onCancel() {

                        }
                    };
                    String message_to_show="";
                    if (point_gagner > 0) {
                    	int new_commercial_point = user.getPoint()+point_gagner;
                    	 message_to_show=  getString(R.string.vous_avez_gagne)+" " + point_gagner + " "+getString(R.string.points)+" \n "+getString(R.string.solde_des_points) +new_commercial_point+
                    	 			"\n \n "+getString(R.string.magasin_vous_avez_gagne)+ SetNumFactureActivity.point_magasin + " "+getString(R.string.points)+" \n "+getString(R.string.solde_des_points)+" "+point_magasin_total;
                    	 SetNumFactureActivity.point_magasin = 0;
                    	 SetNumFactureActivity.point_magasin_total = 0;
                    }
                    if (point_gagner == 0) {
                    	message_to_show= getString(R.string.commande_envoye);
                    }
                   
                    dialogCommandSuccess
                    .show1(message_to_show,
                            getString(R.string.alert_send_command_title),
                            false, getString(R.string.valider));

                } else {
                    if (point_gagner == -1) {

                        // supprimer les produits répété
                        for (int j = 0; j < list_product_repeated.size(); j++) {
                            String ref_to_delete = list_product_repeated.get(j);
                            for (int i = 0; i < list_command_product.size(); i++) {
                                // si le produit ne contient pas des refs
                                if (list_command_product.get(i).deleteRef(
                                        ref_to_delete)) {
                                    list_command_product.remove(i);
                                }
                            }
                        }
                        AlertMessage dialogCommandRepeated = new AlertMessage(
                                activity) {
                            @Override
                            public void onOk() {

                                Intent intent = new Intent(activity,
                                        AddCommandActivity.class);
                                Bundle b = new Bundle();
                                b.putSerializable("user", user);
                                b.putSerializable("produit",
                                        liste_produits);
                                b.putSerializable("from_activity",
                                        Extra.SCAN_CODE_FACTURE);
                                b.putSerializable("list_command",
                                        list_command_product);
                                b.putSerializable("magasin",
                                        magasin);
                                b.putSerializable("from_activity",
                                        Extra.SCAN_CODE_FACTURE);
                                intent.putExtras(b);
                                activity.startActivity(intent);
                                finish();
                            }

                            @Override
                            public void onCancel() {

                            }
                        };

                        String list_ref_deleted = "";
                        for (String ref : list_product_repeated) {
                            list_ref_deleted += " - " + ref + "\n";
                        }
                        dialogCommandRepeated
                                .show1(getString(R.string.deja_commander)
                                                + list_ref_deleted,
                                        getString(R.string.alert_fail_send_command_title),
                                        false, getString(R.string.valider));

                    }
                    // problème connexion
                    if (point_gagner == -2) {
                        AlertMessage dialogCommandFailed = new AlertMessage(
                                activity) {
                            @Override
                            public void onOk() {
                                Intent intent = new Intent(activity,
                                        AddCommandActivity.class);
                                Bundle b = new Bundle();
                                b.putSerializable("user", user);
                                b.putSerializable("produit",
                                        liste_produits);
                                b.putSerializable("list_command",
                                        list_command_product);
                                b.putSerializable("magasin",
                                        magasin);
                                b.putSerializable("from_activity",
                                        Extra.SCAN_CODE_FACTURE);
                                intent.putExtras(b);
                                activity.startActivity(intent);
                                finish();
                            }

                            @Override
                            public void onCancel() {

                            }
                        };
                        dialogCommandFailed
                                .show1(getString(R.string.error_occured),
                                        getString(R.string.alert_fail_send_command_title),
                                        false, getString(R.string.valider));

                    }

                }

            }

        };
        prog.DoTraitemnt("", "");

    }

}
