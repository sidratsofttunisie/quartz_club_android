package mobi.app4mob.quartzclub.activity;

import java.io.Serializable;
import java.util.ArrayList;


import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.parser.UserDataParser;
import mobi.app4mob.quartzclub.utils.EmailValidator;
import mobi.app4mob.quartzclub.utils.ProgressTraitment;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.SessionManager;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

public class ForgotPasswordActivity extends Activity {
    private EditText emailEdit;
    private static ArrayList<Produit> liste_produits = null;
    public static User user;
    private SharedPreferences prefs;
    String errorMsg;
    Activity activity;
    SessionManager session;
    protected String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password);

        activity = this;


        initViews();

    }

    private void initViews() {

        emailEdit = (EditText) findViewById(R.id.editEmail);


    }


    public void forgotPassword(View v) {
        String email_patern =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        EmailValidator emailValidator = new EmailValidator();

        if (!emailValidator.validate(emailEdit.getText().toString().trim())) {
            new MyToast(this, getString(R.string.msg_email_invalide),
                    Toast.LENGTH_SHORT);
        } else {
            sendForgotPassword(emailEdit.getText().toString().trim());
        }
    }

    private void sendForgotPassword(final String sMail) {
        ProgressTraitment prog = new ProgressTraitment(activity) {
            int responseWsClient;

            @Override
            public void onLoadStarted() {
                responseWsClient = new UserDataParser().forgotPassword(sMail);
            }

            @Override
            public void onLoadFinished() {
                if (responseWsClient == 1) {
                    new MyToast(activity, getString(R.string.consulter_courrier), 7500);
                }

                if (responseWsClient == -1) {
                    new MyToast(activity, getString(R.string.email_pas_associe_compte), Toast.LENGTH_SHORT);
                }
                if (responseWsClient == -2) {
                    new MyToast(activity, getString(R.string.erreur_produite), Toast.LENGTH_SHORT);
                }

            }

        };
        prog.DoTraitemnt("", "");

    }


}
