package mobi.app4mob.quartzclub.parser;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.model.Message;
import mobi.app4mob.quartzclub.utils.WebClient;

public class MessageDataParser {


   
   

    public ArrayList<Message> getHistorique(String url) {
        ArrayList<Message> data = null;
        String response = null;

        ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();
        WebClient webClient = new WebClient(url, parameters);
        webClient.execute(0);
        response = webClient.getStringResponse();
        Log.i("data ", response);
        data = parseData(response);

        return data;
    }

    public static ArrayList<Message> parseData(String wsResponse) {
        ArrayList<Message> data = null;
        if (wsResponse != null && !wsResponse.equals(null) && wsResponse.length() > 0) {
            try {
                JSONArray obj = new JSONArray(wsResponse);
                data = new ArrayList<Message>();
                for (int i = 0; i < obj.length(); i++) {
                    JSONObject objI = obj.getJSONObject(i);
                    data.add(new Message(objI));
                }
                 Log.i("data ", data.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return data;
    }


}
