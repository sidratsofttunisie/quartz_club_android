package mobi.app4mob.quartzclub.parser;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.utils.SessionManager;
import mobi.app4mob.quartzclub.utils.WebClient;

public class UserDataParser {

    public static void  updateUser(String url, int id, Context context) {
        User user = null;
        String response = null;
        ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();

        parameters.add(new BasicNameValuePair("user_id", "" + id));
        WebClient webClient = new WebClient(url, parameters);
        webClient.execute(Constants.HTTP_METHOD_GET);
        response = webClient.getStringResponse();
       // Log.i("Response UPDATE USER",response);
        parseUpdatedUser(response, context);


    }


    private static void parseUpdatedUser(String wsResponse, Context context) {
        if (wsResponse != null && !wsResponse.equals(null)
                && wsResponse.length() > 0) {
            try {
                SessionManager session;
                JSONObject json = new JSONObject(wsResponse);
                String status = Functions.setAttribute("status", json, "error");

                if (status.equals("success")) {

                    JSONObject data = json.optJSONObject("data");

                    session = new SessionManager(context);
                    User user = session.getUserDetails();
                    int point = Functions.setIntAttribute("point", data, 0);
                    String pays_id = Functions.setAttribute("pays_id", json, "");
                    int gift_point = Functions.setIntAttribute("gift_point", data, 0);
                    String gift_name = Functions.setAttribute("gift_name", data, "");
                    String gift_image = Functions.setAttribute("gift_image", data, "");
                    int nb_message = Functions.setIntAttribute("nb_message", data, 0);
                    int point_jour = Functions.setIntAttribute("point_jour", data, 0);
                    String last_message = Functions.setAttribute("last_message", data, "");
                    user.setPoint(point);
                    user.setPays_id(pays_id);
                    user.setLast_message(last_message);
                    user.setPoint_jour(point_jour);
                    user.setNb_message(nb_message);
                    if (gift_name != "") {
                        user.getTarget_gift().setName(gift_name);
                        user.getTarget_gift().setImage(gift_image);
                        user.getTarget_gift().setCommercial_points(String.valueOf(gift_point));
                    } else {
                        user.setTarget_gift(null);
                    }
                    session.updateSession(user);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }


    public static User authentificate(String email, String password) {
        User user = null;
        String response = null;
        ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();

        parameters.add(new BasicNameValuePair("username", email));
        parameters.add(new BasicNameValuePair("password", password));
        WebClient webClient = new WebClient(Constants.LOGIN_WS_URL, parameters);
        webClient.execute(Constants.HTTP_METHOD_POST);
        response = webClient.getStringResponse();
       // Log.i("response     ", response.toString());
        user = parse(response);
        return user;

    }


    public static User parse(String wsResponse) {
        User data = null;
        if (wsResponse != null && !wsResponse.equals(null)
                && wsResponse.length() > 0) {
            try {

                JSONObject json = new JSONObject(wsResponse);
                String status = Functions.setAttribute("status", json, "error");

                if (status.equals("success")) {

                    data = new User(json.optJSONObject("data"));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return data;
    }


    public int forgotPassword(String sMail) {
        String response = null;
        ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();

        parameters.add(new BasicNameValuePair("email", sMail));
        WebClient webClient = new WebClient(Constants.FORGOT_PASSWORD_WS_URL, parameters);
        webClient.execute(Constants.HTTP_METHOD_POST);
        response = webClient.getStringResponse();
        Log.i("response     ", response.toString());
        return parseForgotPassword(response);
    }

    public static int parseForgotPassword(String wsResponse) {
        int data = -2;
        if (wsResponse != null && !wsResponse.equals(null)
                && wsResponse.length() > 0) {
            try {

                JSONObject json = new JSONObject(wsResponse);
                String status = Functions.setAttribute("status", json, "error");

                if (status.equals("success")) {

                    data = 1;
                } else {
                    Functions.setIntAttribute("flag", json, -2);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return data;
    }

}
