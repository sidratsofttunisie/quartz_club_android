package mobi.app4mob.quartzclub.parser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.model.Bonus;
import mobi.app4mob.quartzclub.model.MagInfo;
import mobi.app4mob.quartzclub.utils.WebClient;

/**
 * Created by Yosri Mekni <dev4.yosri@gmail.com> on 27/02/2018.
 */

public class RegisterTokenParser {

    public String register(String url,int com_id,String token,String device_id) {
        String data = null;
        String response = null;

        ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();
        parameters.add(new BasicNameValuePair("commercial_id", "" + com_id));
        parameters.add(new BasicNameValuePair("token", token));
        parameters.add(new BasicNameValuePair("device_id", device_id));
        WebClient webClient = new WebClient(url, parameters);
        webClient.execute(1);
        response = webClient.getStringResponse();

        data = parseData(response);

        return data;
    }

    public static String parseData(String wsResponse) {
        String data = null;
        if (wsResponse != null && !wsResponse.equals(null) && wsResponse.length() > 0) {
            try {
                JSONObject obj = new JSONObject(wsResponse);
                data = obj.getString("result");
                // Log.i("data ", data.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return data;
    }

}
