package mobi.app4mob.quartzclub.parser;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import mobi.app4mob.quartzclub.model.Promo;
import mobi.app4mob.quartzclub.utils.WebClient;

public class PromotionDataParser {


    public ArrayList<Promo> getPromos(String url) {
        ArrayList<Promo> data = null;
        String response = null;

        ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();
        WebClient webClient = new WebClient(url, parameters);
        webClient.execute(0);
        response = webClient.getStringResponse();

        data = parseData(response);

        return data;
    }

    public static ArrayList<Promo> parseData(String wsResponse) {
        ArrayList<Promo> data = null;
        if (wsResponse != null && !wsResponse.equals(null) && wsResponse.length() > 0) {
            try {
                JSONArray obj = new JSONArray(wsResponse);
                data = new ArrayList<Promo>();
                for (int i = 0; i < obj.length(); i++) {
                    JSONObject objI = obj.getJSONObject(i);
                    data.add(new Promo(objI));
                }
                // Log.i("data ", data.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return data;
    }


}
