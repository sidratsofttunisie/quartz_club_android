package mobi.app4mob.quartzclub.parser;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.util.Log;

import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.Gift;
import mobi.app4mob.quartzclub.utils.WebClient;

public class GiftDataParser {


        public static Boolean addToTargetGift(String commercial_id, String gift_id) {
        Boolean result = false;
        String response = null;
        ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();

        parameters.add(new BasicNameValuePair("commercial_id", commercial_id));
        parameters.add(new BasicNameValuePair("gift_id", gift_id));
        WebClient webClient = new WebClient(Constants.ADD_TARGET_GIFT_WS_URL, parameters);
        webClient.execute(Constants.HTTP_METHOD_POST);
        response = webClient.getStringResponse();
        Log.i("response     ", response.toString());
        result = parse(response);
        return result;

    }

    public static Boolean parse(String wsResponse) {
        Boolean data = false;
        if (wsResponse != null && !wsResponse.equals(null)
                && wsResponse.length() > 0) {
            try {

                JSONObject json = new JSONObject(wsResponse);
                String status = Functions.setAttribute("status", json, "error");

                if (status.equals("success")) {

                    data = true;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return data;
    }


    public ArrayList<Gift> getGift(String url) {
        ArrayList<Gift> data = null;
        String response = null;

        ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();
        WebClient webClient = new WebClient(url, parameters);
        webClient.execute(0);
        response = webClient.getStringResponse();

        data = parseData(response);

        return data;
    }

    public static ArrayList<Gift> parseData(String wsResponse) {
        ArrayList<Gift> data = null;
        if (wsResponse != null && !wsResponse.equals(null) && wsResponse.length() > 0) {
            try {
                JSONArray obj = new JSONArray(wsResponse);
                data = new ArrayList<Gift>();
                for (int i = 0; i < obj.length(); i++) {
                    JSONObject objI = obj.getJSONObject(i);
                    data.add(new Gift(objI));
                }
                // Log.i("data ", data.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return data;
    }


}
