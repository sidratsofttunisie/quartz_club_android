package mobi.app4mob.quartzclub.parser;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.util.Log;

import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.Command;
import mobi.app4mob.quartzclub.model.Commande;
import mobi.app4mob.quartzclub.model.Gift;
import mobi.app4mob.quartzclub.model.GroupHistorique;
import mobi.app4mob.quartzclub.model.Historique;
import mobi.app4mob.quartzclub.model.History;
import mobi.app4mob.quartzclub.utils.WebClient;

public class HistoriqueGroupDataParser {





    public ArrayList<Commande> getHistorique(String url) {
        ArrayList<Commande> data = null;
        String response = null;

        ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();
        WebClient webClient = new WebClient(url, parameters);
        webClient.execute(0);
        response = webClient.getStringResponse();

        data = parseData(response);

        return data;
    }

    public static ArrayList<Commande> parseData(String wsResponse) {
        ArrayList<Commande> data = null;
        if (wsResponse != null && !wsResponse.equals(null) && wsResponse.length() > 0) {
            try {
                JSONArray obj = new JSONArray(wsResponse);
                data = new ArrayList<Commande>();
                for (int i = 0; i < obj.length(); i++) {
                    JSONObject objI = obj.getJSONObject(i);
                    data.add(new Commande(objI));
                }
                 Log.i("data ", data.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return data;
    }


}
