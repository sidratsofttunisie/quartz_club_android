package mobi.app4mob.quartzclub.parser;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import mobi.app4mob.quartzclub.activity.SetNumFactureActivity;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.Command;
import mobi.app4mob.quartzclub.model.CommandProduct;
import mobi.app4mob.quartzclub.utils.WebClient;

public class CommandDataParser {

    public static int sendCommand(Command command) {
        ArrayList<CommandProduct> list_command_product = command
                .getListProduct();
        ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();
        String response = null;

        JSONArray listProduct = new JSONArray();
        try {

            for (int i = 0; i < list_command_product.size(); i++) {
                JSONObject product = new JSONObject();
                product.put("product_id", ""
                        + list_command_product.get(i).getProduct().getId());
                JSONArray list_code = new JSONArray(list_command_product.get(i)
                        .getListe_code());
                product.put("list_code", list_code);
                listProduct.put(product);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        Functions.LOGI("pp", listProduct.toString());
        parameters.add(new BasicNameValuePair(Command.COMMERCIAL_ID, ""
                + command.getCommercial_id()));
        parameters.add(new BasicNameValuePair(Command.MAGASIN_ID, ""
                + command.getMagasin_id()));
        parameters.add(new BasicNameValuePair(Command.NUM_FACTURE, ""
                + command.getNum_facture()));
        parameters.add(new BasicNameValuePair("products", ""
                + listProduct.toString()));

        WebClient webClient = new WebClient(Constants.NEW_COMMAND_WS_URL,
                parameters);
        webClient.execute(Constants.HTTP_METHOD_POST);
        response = webClient.getStringResponse();

        Log.i("response     ", response.toString());
        return parse(response);
    }

    public static int parse(String wsResponse) {
        int result = -2;
        if (wsResponse != null && !wsResponse.equals(null)
                && wsResponse.length() > 0) {
            try {

                JSONObject json = new JSONObject(wsResponse);
                String status = Functions.setAttribute("status", json, "error");

                if (status.equals("success")) {

                    int point = Functions.setIntAttribute("data", json, 0);
                    result = point;
                    SetNumFactureActivity.point_magasin = Functions.setIntAttribute("magasin_points", json, 0);
                    SetNumFactureActivity.point_magasin_total = Functions.setIntAttribute("magasin_points_total", json, 0);
                } else {
                    result = Functions.setIntAttribute("flag", json, -2);
                    if (result == -1) {
                        ArrayList<String> refs = new ArrayList<String>();

                        JSONArray jsonArray = new JSONArray(
                                Functions.setAttribute("data", json, "[]"));
                        if (jsonArray != null) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                refs.add(jsonArray.get(i).toString());
                            }
                        }
                        SetNumFactureActivity.list_product_repeated = refs;
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

}
