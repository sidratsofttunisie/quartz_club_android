package mobi.app4mob.quartzclub.parser;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.model.Bonus;
import mobi.app4mob.quartzclub.model.MagInfo;
import mobi.app4mob.quartzclub.utils.WebClient;

/**
 * Created by Yosri Mekni <dev4.yosri@gmail.com> on 05/03/2018.
 */

public class ChangeTagParser {

    public String changeTag(String url,int mag_id,String tag) {
        String data = null;
        String response = null;

        ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();
        parameters.add(new BasicNameValuePair("magasin_id", "" + mag_id));
        parameters.add(new BasicNameValuePair("new_tag", tag));
        WebClient webClient = new WebClient(url, parameters);
        webClient.execute(1);
        response = webClient.getStringResponse();
        Log.i("data ", response);
        data = parseData(response);
        Log.i("data ", data);
        return data;
    }

    public static String parseData(String wsResponse) {
        String data = null;
        if (wsResponse != null && !wsResponse.equals(null) && wsResponse.length() > 0) {
            try {
                JSONObject obj = new JSONObject(wsResponse);
                data = obj.getString("status");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return data;
    }

}
