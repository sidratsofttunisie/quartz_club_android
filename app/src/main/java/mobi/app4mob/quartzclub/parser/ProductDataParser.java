package mobi.app4mob.quartzclub.parser;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.utils.WebClient;

public class ProductDataParser {


    public ArrayList<Produit> getAllProduit(String url) {
        ArrayList<Produit> liste_produits;
        String response = null;

        ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();


        WebClient webClient = new WebClient(url, parameters);
        webClient.execute(0);
        response = webClient.getStringResponse();

        liste_produits = parseAllProduct(response);

        return liste_produits;

    }

    public static ArrayList<Produit> parseAllProduct(String wsResponse) {
        ArrayList<Produit> liste_produits = null;
        if (wsResponse != null && !wsResponse.equals(null) && wsResponse.length() > 0) {
            try {
                JSONArray obj = new JSONArray(wsResponse);
                liste_produits = new ArrayList<Produit>();
                for (int i = 0; i < obj.length(); i++) {
                    JSONObject objI = obj.getJSONObject(i);
                    liste_produits.add(new Produit(objI));
                }
                // Log.i("data ", data.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return liste_produits;
    }


    public static Produit getProductByCode(String url, String code_product) {
        Produit data = null;
        String response = null;

        ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();

        parameters.add(new BasicNameValuePair(Produit.CODE, code_product));

        WebClient webClient = new WebClient(url, parameters);
        webClient.execute(0);
        response = webClient.getStringResponse();

        data = parseData(response);

        return data;

    }


    // parse One Product
    public static Produit parseData(String wsResponse) {
        Produit data = null;
        if (wsResponse != null && !wsResponse.equals(null)
                && wsResponse.length() > 0) {
            try {
                JSONArray obj = new JSONArray(wsResponse);

                JSONObject objI = obj.getJSONObject(0);
                data = new Produit(objI);

                // Log.i("data ", data.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return data;
    }


}
