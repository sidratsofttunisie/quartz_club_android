package mobi.app4mob.quartzclub.parser;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mobi.app4mob.quartzclub.model.Bonus;
import mobi.app4mob.quartzclub.model.BonusAnniv;
import mobi.app4mob.quartzclub.utils.WebClient;

/**
 * Created by Yosri Mekni <dev4.yosri@gmail.com> on 08/02/2018.
 */

public class BonusAnnivDataParser {


    public ArrayList<BonusAnniv> getBonusAnniv(String url) {
        ArrayList<BonusAnniv> data = null;
        String response = null;

        ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();
        WebClient webClient = new WebClient(url, parameters);
        webClient.execute(0);
        response = webClient.getStringResponse();

        data = parseData(response);

        return data;
    }

    public static ArrayList<BonusAnniv> parseData(String wsResponse) {
        ArrayList<BonusAnniv> data = null;
        if (wsResponse != null && !wsResponse.equals(null) && wsResponse.length() > 0) {
            try {
                JSONObject obj = new JSONObject(wsResponse);
                data = new ArrayList<BonusAnniv>();
                for (int i = 0; i < obj.getJSONArray("data").length(); i++) {
                    JSONObject objI = obj.getJSONArray("data").getJSONObject(i);
                    data.add(new BonusAnniv(objI));
                }
                 Log.i("data ", data.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return data;
    }

}
