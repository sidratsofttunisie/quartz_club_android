package mobi.app4mob.quartzclub.parser;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import mobi.app4mob.quartzclub.activity.AddMagasinActivity;
import mobi.app4mob.quartzclub.activity.ListMagasinActivity;
import mobi.app4mob.quartzclub.activity.R;
import mobi.app4mob.quartzclub.constants.Constants;
import mobi.app4mob.quartzclub.constants.Functions;
import mobi.app4mob.quartzclub.model.Produit;
import mobi.app4mob.quartzclub.model.User;
import mobi.app4mob.quartzclub.utils.AlertMessage;
import mobi.app4mob.quartzclub.utils.MultipartUtility;
import mobi.app4mob.quartzclub.utils.MyToast;
import mobi.app4mob.quartzclub.utils.SessionManager;
import mobi.app4mob.quartzclub.utils.WebClient;
import nl.changer.polypicker.model.Image;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MagasinDataParser {

    public int flag;

    //    public static int addMagasin(final Context context , final int new_sold, final User user, final ArrayList<Produit> liste_produits, int commercial_id, String rais_soc, String register, String nom_,
//                                  String prenom_, String address_, String email_, String tel_,
//                                  String cin_, String tag_client_, String tag_mag_, double longitude, double latitude, HashSet<Image> mMediaImages) {
    public static int addMagasin(int commercial_id, String rais_soc, String register, String nom_,
                                 String prenom_, String address_, String email_, String tel_,
                                 String cin_, String tag_client_, String tag_mag_, double longitude, double latitude, HashSet<Image> mMediaImages) {


//        final ProgressDialog prgDialog = new ProgressDialog(context);
////        prgDialog.setMessage(context.getResources().getString(R.string.chargement_en_cours));
//        prgDialog.setCancelable(false);
//        prgDialog.show();
//
//        final String[] result = {null};
//        final SessionManager session;
//
//        RequestParams requestParams = new RequestParams();
//        requestParams.put("magasin_name", rais_soc);
//        requestParams.put("registre_commerce", register);
//        requestParams.put("first_name", nom_);
//        requestParams.put("last_name", prenom_);
//        requestParams.put("address", address_);
//        requestParams.put("email", email_);
//        requestParams.put("tel", tel_);
//        requestParams.put("cin", cin_);
////          requestParams.put("ref_client",tag_client_);
//        requestParams.put("ref_magasin", tag_mag_);
//        requestParams.put("commercial_id", "" + commercial_id);
//        requestParams.put("longitude", "" + longitude);
//        requestParams.put("latitude", "" + latitude);
//
//        try {
//            List<File> mFiles = new ArrayList<File>();
//            for (Image img : mMediaImages) {
//
//                File ff = new File(img.mUri.toString());
//                mFiles.add(ff);
//                System.out.println("FILE PATH ===> photos[] " + ff.getPath() + " / " + img.mUri.toString().substring(img.mUri.toString().lastIndexOf("/") + 1, img.mUri.toString().length()));
//            }
//            File[] files = mFiles.toArray(new File[mFiles.size()]);
//            requestParams.put("photos[]", files);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        AsyncHttpClient client = new AsyncHttpClient();
//        Log.i("LOOPJ", "send SubmitResponseRequest : SUBMIT_RESPONSE_REQUEST_URL = "
//                + Constants.ADD_MAGASIN_WS_URL + " - requestParams = " + requestParams.toString());
//        client.post(Constants.ADD_MAGASIN_WS_URL, requestParams, new AsyncHttpResponseHandler() {
//
//
//            @Override
//            public void onStart() {
//                Log.i("UPLOAD FILE START", "SubmitResponseRequest => onSuccess : statusCode = "  + " - responseBody ");
//            }
//
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
//                // called when response HTTP status is "200 OK"
//                prgDialog.hide();
//                try {
//                    result[0] = new String(response, "UTF-8");
//                    int flag = parse(result[0]);
//                    if (flag == 1) {
//                        // new MyToast(getApplicationContext(), "success",
//                        // MyToast.LENGTH_SHORT);
//                        /*
//                         * SharedPreferences.Editor ed = prefs.edit();
//						 * ed.putString(User.EMAIL, user.getEmail());
//						 * ed.commit();
//						 */
//
//                        AlertMessage dialogAuthentificationFailed = new AlertMessage(
//                                context) {
//                            @Override
//                            public void onOk() {
//                                if (new_sold > 0)
//                                    user.setPoint(new_sold);
//
//                                // Update Session User(with new solde point)
//                                new SessionManager(
//                                        context).updateSession(user);
//
//                                Intent intent = new Intent(context,
//                                        ListMagasinActivity.class);
//                                Bundle b = new Bundle();
//                                b.putSerializable("user", user);
//                                b.putSerializable("produit", liste_produits);
//                                intent.putExtras(b);
//                                context.startActivity(intent);
//
//                            }
//
//                            @Override
//                            public void onCancel() {
//                                Functions.LOGI("*****", "on cancle");
//                            }
//                        };
//                        dialogAuthentificationFailed.show1(
//                                context.getResources().getString(R.string.alert_add_magasin_message),
//                                context.getResources().getString(R.string.alert_add_magasin_title),
//                                false, context.getResources().getString(R.string.valider));
//
//                    }
//                    // cin existe
//                    if (flag == -1) {
//                        AlertMessage dialogAuthentificationFailed = new AlertMessage(
//                                context) {
//                            @Override
//                            public void onOk() {
//                                // TODO: convert cin to red border
//
//                            }
//
//                            @Override
//                            public void onCancel() {
//                                Functions.LOGI("*****", "on cancle");
//                            }
//                        };
//                        dialogAuthentificationFailed.show1(
//                                context.getResources().getString(R.string.error_cin_existe),
//                                context.getResources().getString(R.string.erreur), false, "OK");
//                    }
//                    // ref client existe
//                    if (flag == -2) {
//                        AlertMessage dialogAuthentificationFailed = new AlertMessage(
//                                context) {
//                            @Override
//                            public void onOk() {
//                                // TODO: convert cin to red border
//
//                            }
//
//                            @Override
//                            public void onCancel() {
//                                Functions.LOGI("*****", "on cancle");
//                            }
//                        };
//                        dialogAuthentificationFailed.show1(
//                                context.getResources().getString(R.string.error_client_existe),
//                                context.getResources().getString(R.string.erreur), false, "OK");
//                    }
//                    // ref magasin existe
//                    if (flag == -3) {
//                        AlertMessage dialogAuthentificationFailed = new AlertMessage(
//                                context) {
//                            @Override
//                            public void onOk() {
//                                // TODO: convert cin to red border
//
//                            }
//
//                            @Override
//                            public void onCancel() {
//                                Functions.LOGI("*****", "on cancle");
//                            }
//                        };
//                        dialogAuthentificationFailed.show1(
//                                context.getResources().getString(R.string.error_magasin_existe),
//                                context.getResources().getString(R.string.erreur), false, "OK");
//                    }
//                    // ref registre du commerce existe
//                    if (flag == -4) {
//                        AlertMessage dialogAuthentificationFailed = new AlertMessage(
//                                context) {
//                            @Override
//                            public void onOk() {
//                                // TODO: convert cin to red border
//
//                            }
//
//                            @Override
//                            public void onCancel() {
//                                Functions.LOGI("*****", "on cancle");
//                            }
//                        };
//                        dialogAuthentificationFailed
//                                .show1(context.getResources().getString(R.string.error_registre_du_commerce_existe),
//                                        context.getResources().getString(R.string.erreur), false, "OK");
//                    }
//                    // nom magasin existe
//                    if (flag == -5) {
//                        AlertMessage dialogAuthentificationFailed = new AlertMessage(
//                                context) {
//                            @Override
//                            public void onOk() {
//                                // TODO: convert cin to red border
//
//                            }
//
//                            @Override
//                            public void onCancel() {
//                                Functions.LOGI("*****", "on cancle");
//                            }
//                        };
//                        dialogAuthentificationFailed
//                                .show1(context.getResources().getString(R.string.error_nom_pv_existe),
//                                        context.getResources().getString(R.string.erreur), false, "OK");
//                    }
//                    // Autre
//                    if (flag == 0) {
//                        new MyToast(context,
//                                R.string.error_add_magasin, Toast.LENGTH_SHORT);
//
//                        Log.v("main context",
//                                context.getResources().getString(R.string.error_add_magasin));
//                    }
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                }
//                Log.i("UPLOAD FILE", "SubmitResponseRequest => onSuccess : statusCode = " + statusCode + " - responseBody "
//                        + result[0]);
//
//            }
//
//            @Override
//            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
//                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
//
//                try {
//                    result[0] = new String(errorResponse, "UTF-8");
//                } catch (UnsupportedEncodingException ee) {
//                    ee.printStackTrace();
//                }
//                Log.i("UPLOAD FILE", "SubmitResponseRequest => onSuccess : statusCode = " + statusCode + " - responseBody "
//                        + result[0]);
//            }
//
//            @Override
//            public void onRetry(int retryNo) {
//                Log.i("UPLOAD FILE RETRY", "SubmitResponseRequest => onSuccess : statusCode = "  + " - responseBody ");
//            }
//
//
//        });


        String result = null;
        try {
            HttpClient httpClient = new DefaultHttpClient();

            System.out.println("URL Path is " + Constants.ADD_MAGASIN_WS_URL);
            int i = 0;
            HttpPost postRequest = new HttpPost(Constants.ADD_MAGASIN_WS_URL);
            MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            reqEntity.addPart("magasin_name", new StringBody(rais_soc));
            reqEntity.addPart("registre_commerce", new StringBody(register));
            reqEntity.addPart("first_name", new StringBody(nom_));
            reqEntity.addPart("last_name", new StringBody(prenom_));
            reqEntity.addPart("address", new StringBody(address_));
            reqEntity.addPart("email", new StringBody(email_));
            reqEntity.addPart("tel", new StringBody(tel_));
            reqEntity.addPart("cin", new StringBody(cin_));
//          reqEntity.addPart("ref_client",new StringBody(tag_client_));
            reqEntity.addPart("ref_magasin", new StringBody(tag_mag_));
            reqEntity.addPart("commercial_id", new StringBody("" + commercial_id));
            reqEntity.addPart("longitude", new StringBody("" + longitude));
            reqEntity.addPart("latitude", new StringBody("" + latitude));
            for (Image img : mMediaImages) {
                File ff = new File(img.mUri.toString());
                FileBody fileBody = new FileBody(ff, img.mUri.toString().substring(img.mUri.toString().lastIndexOf("/") + 1, img.mUri.toString().length()), "image/jpeg", "UTF-8");
                System.out.println("FILE PATH ===> photos[" + i + "] " + ff.getPath() + " / " + img.mUri.toString().substring(img.mUri.toString().lastIndexOf("/") + 1, img.mUri.toString().length()));
                reqEntity.addPart("photos[" + i + "]", fileBody);
                i++;
            }
            postRequest.setEntity(reqEntity);
            HttpResponse response = httpClient.execute(postRequest);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            String sResponse;
            StringBuilder s = new StringBuilder();
            while ((sResponse = reader.readLine()) != null) {

                s = s.append(sResponse);

            }

            System.out.println("Response is: " + s);

            result = s.toString();

        } catch (Exception e) {
            e.printStackTrace();


        }


//        int result = 0;
//        List<String> response = null;
//        ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();
//
//        parameters.add(new BasicNameValuePair("magasin_name", rais_soc));
//        parameters.add(new BasicNameValuePair("registre_commerce", register));
//        parameters.add(new BasicNameValuePair("first_name", nom_));
//        parameters.add(new BasicNameValuePair("last_name", prenom_));
//        parameters.add(new BasicNameValuePair("address", address_));
//        parameters.add(new BasicNameValuePair("email", email_));
//        parameters.add(new BasicNameValuePair("tel", tel_));
//        parameters.add(new BasicNameValuePair("cin", cin_));
////        parameters.add(new BasicNameValuePair("ref_client", tag_client_));
//        parameters.add(new BasicNameValuePair("ref_magasin", tag_mag_));
//        parameters.add(new BasicNameValuePair("commercial_id", "" + commercial_id));
//        parameters.add(new BasicNameValuePair("longitude", "" + longitude));
//        parameters.add(new BasicNameValuePair("latitude", "" + latitude));
//        WebClient webClient = new WebClient(Constants.ADD_MAGASIN_WS_URL, parameters,mMediaImages);
//        webClient.execute(Constants.HTTP_METHOD_POST);
//        response = webClient.getStringResponse();
//        try {
//            String charset = "UTF-8";
//            MultipartUtility multipart = new MultipartUtility(Constants.ADD_MAGASIN_WS_URL, charset);
//
////            multipart.addHeaderField("User-Agent", "CodeJava");
////            multipart.addHeaderField("Test-Header", "Header-Value");
//
//            multipart.addFormField("friend_id", "Cool Pictures");
//            multipart.addFormField("userid", "Java,upload,Spring");
//            multipart.addFormField("magasin_name", rais_soc);
//            multipart.addFormField("registre_commerce", register);
//            multipart.addFormField("first_name", nom_);
//            multipart.addFormField("last_name", prenom_);
//            multipart.addFormField("address", address_);
//            multipart.addFormField("email", email_);
//            multipart.addFormField("tel", tel_);
//            multipart.addFormField("cin", cin_);
//            multipart.addFormField("ref_client", tag_client_);
//            multipart.addFormField("ref_magasin", tag_mag_);
//            multipart.addFormField("commercial_id", "" + commercial_id);
//            multipart.addFormField("longitude", "" + longitude);
//            multipart.addFormField("latitude", "" + latitude);
//            int i =0;
//            for (Image img : mMediaImages) {
//                File ff = new File(img.mUri.toString());
//                System.out.println("FILE PATH"+" ===> photos[] "+ff.getPath()+" / "+img.mUri.toString().substring(img.mUri.toString().lastIndexOf("/") + 1,img.mUri.toString().length()) );
//                multipart.addFilePart("photos[]", ff);
//                i++;
//            }
//
//            response = multipart.finish();
//
//            Log.v("rht", "SERVER REPLIED:");
//
//            for (String line : response) {
//                Log.v("rht", "Line : " + line);
//
//            }
//            Log.i("response     ", response.toString());
//            result = parse(response.toString());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return result[0];

        return parse(result);
    }

    public static int parse(String wsResponse) {
        Boolean data = false;
        int flag = 0;
        int point = 0;
        if (wsResponse != null && !wsResponse.equals(null)
                && wsResponse.length() > 0) {
            try {

                JSONObject json = new JSONObject(wsResponse);
                String status = Functions.setAttribute("status", json, "error");

                if (status.equals("success")) {
                    data = true;
                    flag = 1;
                    //nouveau solde des points
                    point = Functions.setIntAttribute("data", json, 0);
                    AddMagasinActivity.new_sold = point;
                }
                if (status.equals("fail")) {
                    flag = Functions.setIntAttribute("flag", json, 0);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return flag;
    }

}
